#include <Wire.h>
#include <LiquidCrystal.h>
#include <SoftwareSerial.h>
#include "DHT.h"

#define DHTPIN A1
#define DHTTYPE DHT11
#define trigPin 12
#define echoPin 11
#define DST_IP "192.168.1.12"

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
DHT dht(DHTPIN, DHTTYPE);
SoftwareSerial mySerial(2, 3);

float humidity;
float t;
float thermos;
unsigned int measTempCount;
unsigned int backLightCount;
unsigned int sendStatusCount;

void setup(){
  lcd.begin(16, 2);
  dht.begin();
  mySerial.begin(9600);
  Serial.begin(9600);
  
  pinMode(10,OUTPUT);
  pinMode(trigPin,OUTPUT);
  pinMode(echoPin,INPUT);
  pinMode(A1,INPUT);
  pinMode(A5,OUTPUT);
  
  measTempCount=0;
  sendStatusCount=0;
  backLightCount=300;
  thermos=17.0;
  digitalWrite(10,HIGH);
  digitalWrite(A5,LOW);
  
  lcd.print("boot");
  delay(250);
  measTemp();
  displayValues();
  lcd.setCursor(0,1);
  lcd.print("net start");
  sendStatus(0);
  
}



void loop(){
  delay(200);
  
  handleButtons();
  
  if(checkSonar()){
    backLightCount = 300;  
    digitalWrite(10,HIGH);
    
  }
  
  //every minute
  if(measTempCount == 300){
    measTemp();
    setThermos();
    
    displayValues();
    measTempCount=0;
  }
  
  //every 12 minutes
  if(sendStatusCount == 3600){
    sendStatus(1);
    sendStatusCount=0;
  }
  
  if(backLightCount == 2){
    digitalWrite(10,LOW);
    lcd.setCursor(0,1);
    lcd.print("      .");
  }
  
  if(backLightCount > 0){
    backLightCount=backLightCount-1;
  }
  sendStatusCount++;
  measTempCount++;
}

void handleButtons(){
  int buttonVal = analogRead(A0);
  if(buttonVal < 1010){
    lcd.setCursor(9,1);
    lcd.print("but");
    lcd.print(buttonVal);
  }
  //select button pressed
  if(buttonVal >600 && buttonVal <700){
    sendStatus(2);
  }
  
  //up or down button, set thermos
  if(buttonVal >90 && buttonVal < 120){
    thermos = thermos+0.5; 
    lcd.setCursor(8,1);
    lcd.print(" thr");
    lcd.print(thermos); 
  }
  if(buttonVal >230 && buttonVal < 290){
    thermos = thermos-0.5;
    lcd.setCursor(9,1);
    lcd.print("thr");
    lcd.print(thermos);  
  }
  
}

boolean checkSonar(){
  int timetaken, dist;
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  timetaken = pulseIn(echoPin, HIGH);
  dist = (timetaken/2) * 0.034049 ;
  if (dist >= 40 || dist <= 10){
    return false;
  }else{
    delay(250);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(trigPin, LOW);
    timetaken = pulseIn(echoPin, HIGH);
    dist = (timetaken/2) * 0.034049 ;
    if (dist >= 40 || dist <= 10){
      //lcd.setCursor(10,1);
      //lcd.print("sen");
      //lcd.print(dist);
      return true;
    }
    //lcd.setCursor(10,1);
    //lcd.print("seo");
    //lcd.print(dist);
    return false;
  } 
}

void displayValues(){
  char buffer[20];
  lcd.setCursor(0,0);
  //sprintf(buffer,"temp %f hum %f ",t,h );
  //lcd.print(buffer);
  lcd.print("temp ");
  lcd.print(t);
  lcd.print("h ");
  lcd.print(humidity);
}


void measTemp(){
  humidity = dht.readHumidity();
  // Read temperature as Celsius
  t = dht.readTemperature(); 
}

void setThermos(){
   if((t + 1.0) < thermos){
       digitalWrite(A5,HIGH);
   }
   if( t > thermos){
     digitalWrite(A5,LOW);
   }
  
}

void sendStatus(int istatus){
  String cmd;
  char temp[10];
  String tempAsString;
  String mosAsString;
  
  mySerial.println("AT+RST");
  mySerial.println("AT+CWMODE=1");
  relayData();//1
  delay(2500);
  relayData();//2
  mySerial.println("AT+CWJAP=\"edland\",\"nathalie09\"");
  relayData();//3
  delay(4000);
  relayData();//4
  cmd = "AT+CIPSTART=\"TCP\",\""; //make this command: AT+CPISTART="TCP","192.168.88.35",80
    cmd += DST_IP;
    cmd += "\",80";
    mySerial.println(cmd);
    // check for linked
    if(linkCheck()){
    
      relayData();//5
      delay(1000);
      //relayData();//6
      dtostrf(t,1,2,temp);
      tempAsString = String(temp);
      
      dtostrf(thermos,1,2,temp);
      mosAsString = String(temp);
      
      cmd = "GET /projects/senswi/status.php?s=";
      cmd.concat(istatus);
      cmd.concat("&t=" + tempAsString);
      cmd.concat("&m=" + mosAsString);
      cmd.concat(" HTTP/1.0\r\n");
      //if(istatus==0){
      //  cmd = "GET /status.php?s=0&t= HTTP/1.0\r\n";
      //}
      cmd += "Host: 192.168.1.12\r\n\r\n";
      
      //The ESP8266 needs to know the size of the GET request
      mySerial.print("AT+CIPSEND=");
      mySerial.println(cmd.length());
      delay(200);
      mySerial.print("GET /projects/senswi/status.php?s=");
      mySerial.print(istatus);
      mySerial.print("&t=" + tempAsString);
      mySerial.print("&m=" + mosAsString);
      mySerial.println(" HTTP/1.0\r\n");
      delay(20);
      //  mySerial.println("GET /status.php HTTP/1.0");
      mySerial.println("Host: 192.168.1.12");
      mySerial.println("");
      
      handleResponse();
      
      delay(250);
      Serial.println("link done");
    }
}

boolean linkCheck(){
  char inData[255]; // Allocate some space for the string
  char inChar=-1; // Where to store the character read
  byte index = 0;
  delay(50);
  Serial.println('l');
  while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            
    }
    inData[index] = '\0';
  String res = String(inData);
  
  if(res.indexOf("Linked") > 0){
      return true;
  }else{
     lcd.setCursor(9,1);
     lcd.print("no link   ");
     Serial.println("rese");
     Serial.println(res);
  }
  return false;  
}

void handleResponse(){
  char inData[255]; // Allocate some space for the string
  char inChar=-1; // Where to store the character read
  byte index = 0;
  delay(50);
  Serial.println('a');
  delay(50);
  while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            
    }
    inData[index] = '\0';
  String res = String(inData);
  Serial.print("res:");
    Serial.println(res);
  delay(100);
  
  int positio=res.indexOf("MP");
  delay(50);
  Serial.print("po");
  Serial.println(positio);
  
  int positmos=res.indexOf("OS");
  delay(50);
  Serial.print("tmpv:");
  Serial.println(positmos);
  
  if(positmos > 0){
    res = res.substring(positmos+2,positmos+6);
    delay(50);
    Serial.print("mos:");
    Serial.println(res);
    //float mos = res.toFloat(); //tofloat not yet available in 1.04
    char buffer[5];
    res.toCharArray(buffer,5);
    float mos = atof(buffer);
    
    if(mos > 10 && mos < 25){
       thermos = mos; 
    }else{
      Serial.print("out of range:");
      Serial.println(mos);
    }
    
  }
  
  if(positio > 0){
    res = res.substring(positio+2,positio+6);
    delay(50);
    Serial.println("res:");
    Serial.println(res);
    
    
    lcd.setCursor(0,1);
    lcd.print("out:       ");
    lcd.setCursor(5,1);
    lcd.print(res);
   
  }else{
    delay(50);
    Serial.println("res not found");
    Serial.println(res);
    
    lcd.setCursor(9,1);
    lcd.print("no data   ");
  }
  
}

void relayData(){
  char inData[255]; // Allocate some space for the string
  char inChar=-1; // Where to store the character read
  byte index = 0;
  delay(50);
  Serial.println('w');
  while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            
    }
    inData[index] = '\0';
   Serial.println(inData);
  
}
