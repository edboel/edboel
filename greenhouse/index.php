<html>
<head><title>An automated greenhouse</title></head>
<br>
<center>
<h2>An automated greenhouse<h2>
</center><br>
Edsard Boelen, 20-mar-2016
<hr><i>
After I saw a ted talk about indoor growing I thought it would be fun to build an indoor greenhouse... Special plant grow leds aren't expensive and with a raspberry pi a nice online monitor and control system can be build.
<hr></i>
<img src="greenhousel.jpg" /><img src="greenhouser.jpg" /><br>
<br>
<br>
<h2> Overview </h2>
<br>
The greenhouse will have the following capabilities:<ul>
<li>dimmable high efficiency lights     </li>
<li>temp measurement every 15 min       </li>
<li>humidity measurement every 15 min   </li>
<li>webcam snapshot every hour          </li>
<li>airflow fan                         </li>
<li>light intensity sensor              </li>
<li>multiple soil humidity sensors      </li>
<li>water pump + irrigation             </li>
<li>water supply tank sensor            </li>
</ul>

<br>

<h2> Casing </h2>
<br>
For the conveniece I bought 2 'socker' greenhouse casings at the local IKEA for 12 euro's it has a 40x20cm surface and has a height of 35 cm.
<br>
<img src="socker.jpg" />

<h2> Lights </h2>
<br>
The lights are the most important part, for the first casing I used 6 strips of 40cm with 5630 leds drawing 17 watts of power.<br>
An IRF3205 mosfet will drive the leds so they can be turned on and off by the raspberry pi. Unfortunately the gate threshold voltage is to high for the raspberry pi so i will have an extra transistor pulling up the voltage.<br>
The other controller will use 3 FQP30N06 mosfets which don't need an extra transistor.<br>
<img src="mosfet1.jpg" /> . <img src="mosfet2.jpg" /> . <img src="mosfet3.jpg" /> .<img src="leds1.jpg" /><br>
<br>
To controll it from a raspberry pi, connect the gate to GPIO 27 (pin 13), then in a bash shell in raspbian running on the pi it can be controlled by the following line:
<pre>
  
  gpio -g mode 27 out
  gpio -g write 27 1

</pre>
If the gpio utillity is not available, install it from wiringpi.com<br>

To make them work in a web interface I installed apache on the raspberry pi and used the following code:


<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #557799">&lt;?php</span>
   <span style="color: #008800; font-weight: bold">if</span>(<span style="color: #007020">isset</span>(<span style="color: #996633">$_REQUEST</span>[<span style="background-color: #fff0f0">&#39;l&#39;</span>])){
        <span style="color: #008800; font-weight: bold">if</span>(<span style="color: #996633">$_REQUEST</span>[<span style="background-color: #fff0f0">&#39;l&#39;</span>] <span style="color: #333333">==</span> <span style="background-color: #fff0f0">&#39;on&#39;</span>){
          <span style="color: #007020">system</span>(<span style="background-color: #fff0f0">&#39;gpio write 2 0&#39;</span>);
        }
        <span style="color: #008800; font-weight: bold">if</span>(<span style="color: #996633">$_REQUEST</span>[<span style="background-color: #fff0f0">&#39;l&#39;</span>] <span style="color: #333333">==</span> <span style="background-color: #fff0f0">&#39;off&#39;</span>){
          <span style="color: #007020">system</span>(<span style="background-color: #fff0f0">&#39;gpio write 2 1&#39;</span>);
        }
   }
<span style="color: #557799">?&gt;</span>
<span style="color: #007700">&lt;script </span><span style="color: #0000CC">type=</span><span style="background-color: #fff0f0">&quot;text/javascript&quot;</span><span style="color: #007700">&gt;</span>
$(<span style="color: #007020">document</span>).ready(<span style="color: #008800; font-weight: bold">function</span>(){
        $(<span style="background-color: #fff0f0">&#39;.lighton&#39;</span>).click(<span style="color: #008800; font-weight: bold">function</span>(e){
                <span style="color: #008800; font-weight: bold">var</span> xhttp <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> XMLHttpRequest();
                xhttp.open(<span style="background-color: #fff0f0">&quot;GET&quot;</span>,<span style="background-color: #fff0f0">&quot;buttons.php?l=on&quot;</span>,<span style="color: #008800; font-weight: bold">true</span>);
                xhttp.send();
        });
        $(<span style="background-color: #fff0f0">&#39;.lightoff&#39;</span>).click(<span style="color: #008800; font-weight: bold">function</span>(e){
                <span style="color: #008800; font-weight: bold">var</span> xhttp <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> XMLHttpRequest();
                xhttp.open(<span style="background-color: #fff0f0">&quot;GET&quot;</span>,<span style="background-color: #fff0f0">&quot;buttons.php?l=off&quot;</span>,<span style="color: #008800; font-weight: bold">true</span>);
                xhttp.send();
        });
});
<span style="color: #007700">&lt;/script&gt;</span>
<span style="color: #007700">&lt;hr/&gt;</span>
<span style="color: #007700">&lt;table</span> <span style="color: #0000CC">class=</span><span style="background-color: #fff0f0">&quot;buttontable&quot;</span> <span style="color: #0000CC">width=</span><span style="background-color: #fff0f0">&quot;100%&quot;</span><span style="color: #007700">&gt;</span>
<span style="color: #007700">&lt;tr&gt;</span>
<span style="color: #007700">&lt;td&gt;</span> <span style="color: #880000; font-weight: bold">&amp;nbsp;</span> <span style="color: #007700">&lt;/td&gt;&lt;td&gt;&lt;button</span> <span style="color: #0000CC">type=</span><span style="background-color: #fff0f0">&quot;button&quot;</span> <span style="color: #0000CC">class=</span><span style="background-color: #fff0f0">&quot;lighton  btn btn-primary btn-block&quot;</span><span style="color: #007700">&gt;</span>light on<span style="color: #007700">&lt;/button&gt;&lt;/td&gt;</span>
<span style="color: #007700">&lt;td&gt;</span> <span style="color: #880000; font-weight: bold">&amp;nbsp;</span> <span style="color: #007700">&lt;/td&gt;&lt;td&gt;&lt;button</span> <span style="color: #0000CC">type=</span><span style="background-color: #fff0f0">&quot;button&quot;</span> <span style="color: #0000CC">class=</span><span style="background-color: #fff0f0">&quot;lightoff btn btn-primary btn-block&quot;</span><span style="color: #007700">&gt;</span>light off<span style="color: #007700">&lt;/button&gt;&lt;/td&gt;</span>
<span style="color: #007700">&lt;td&gt;</span> <span style="color: #880000; font-weight: bold">&amp;nbsp;</span> <span style="color: #007700">&lt;/td&gt;</span>
<span style="color: #007700">&lt;/tr&gt;</span>
<span style="color: #007700">&lt;tr&gt;&lt;td</span> <span style="color: #0000CC">colspan=</span><span style="background-color: #fff0f0">&quot;4&quot;</span><span style="color: #007700">&gt;</span> <span style="color: #880000; font-weight: bold">&amp;nbsp;</span> <span style="color: #007700">&lt;br&gt;&lt;/td&gt;&lt;/tr&gt;</span>
<span style="color: #007700">&lt;/table&gt;</span>
<span style="color: #007700">&lt;hr/&gt;</span>
</pre></div>



<h2> DHT22 Temp and humidity sens </h2>
<br>
<img src="dht22.jpg" /> . &nbsp; <img src="dht22b.jpg" /><br>
The DHT22 is my allround temperature and humidity sensor, for the raspberry pi, it is not fully reliable because it uses the 1-wire interface which needs precision timing of 50us. A multitasking OS cannot guarantee that all the time. Anyway, if the crc fails I will just retry a couple of times. I connected the data pin to and used the adafruit python library for reading. download the version I used <a href="dht22.tar.gz">here</a><br>
<pre>

import sys

import Adafruit_DHT
import time
humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.AM2302, 4)

if temperature < 10 or humidity > 110:
        time.sleep(3)
        humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.AM2302, 4)
if humidity is not None and temperature is not None:
        print time.strftime("%Y-%m-%d %H:%M:%S")+'_0_{0:0.1f}_{1:0.1f}'.format(temperature, humidity)
else:
        print 'Failed to get reading. Try again!'
        sys.exit(1)

</pre>
the following crontab will log the temperatures every 15 minutes.<br>
<pre>
   0,15,30,45 * * * * /local/readtemp.py >> /data/temps.txt
</pre>
Below you will see the first results on how they are stored in the text files.
<pre>
2016-04-01 23:30:03_0_20.2_58.4
2016-04-01 23:45:05_0_20.1_56.6
2016-04-02 00:00:03_0_20.1_56.9
2016-04-02 00:15:03_0_19.8_60.0
2016-04-02 00:30:02_0_19.5_61.5
</pre>

<h2> Camera </h2>
<br>
I had a raspberry pi camera laying around so i used that one. It is placed 40cm from the case and will make an image every hour. the following script will also create the appropiate file name.
<br>
<br>
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #996633">now</span><span style="color: #333333">=</span><span style="color: #008800; font-weight: bold">$(</span>date +<span style="background-color: #fff0f0">&quot;%y-%m-%d_%H&quot;</span><span style="color: #008800; font-weight: bold">)</span>; sudo raspistill -o /data/img/img_<span style="color: #996633">$now</span>.jpg
</pre></div>
<br>
<img src="camera1.jpg" /><br>
and camera2 at exactly 40 cm from the case<br>
<img src="camera2.jpg" /><br>

<h2> Webinterface </h2>
<br>
The webinterface will show some nice charts and a good current overview by using bar graphs. The charts are being generated by hicharts. the bars are some css tricks and looks like as following:<br>
<br>
<img src="bars.jpg" /><br>
The code for the vertical bars are as follows It will read the last line from the temp logs and place them in bootstrap bars:

<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #333333">&lt;</span><span style="color: #007700">style</span><span style="color: #333333">&gt;</span>
<span style="color: #007700">center</span>{
 <span style="color: #008800; font-weight: bold">vertical-align</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">text</span><span style="color: #333333">-</span><span style="color: #008800; font-weight: bold">top</span>;
}

<span style="color: #BB0066; font-weight: bold">.col-sm-4</span>{
	<span style="color: #008800; font-weight: bold">width</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">100</span><span style="color: #333333">%</span>;
	<span style="color: #008800; font-weight: bold">text-align</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">center</span>;
}

<span style="color: #BB0066; font-weight: bold">.vertical-bars</span> {
    <span style="color: #008800; font-weight: bold">margin-top</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">20px</span>;
    <span style="color: #008800; font-weight: bold">text-align</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">center</span>; }
    <span style="color: #BB0066; font-weight: bold">.vertical-bars</span> <span style="color: #007700">li</span> {
      <span style="color: #008800; font-weight: bold">display</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">inline</span><span style="color: #333333">-</span><span style="color: #008800; font-weight: bold">block</span>;
      <span style="color: #008800; font-weight: bold">position</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">relative</span>;
      <span style="color: #008800; font-weight: bold">width</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">45px</span>;
      <span style="color: #008800; font-weight: bold">height</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">200px</span>;
      <span style="color: #008800; font-weight: bold">background</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">#f0f0f0</span>;
      <span style="color: #008800; font-weight: bold">margin</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0</span> <span style="color: #6600EE; font-weight: bold">10px</span>; }
      <span style="color: #008800; font-weight: bold">@media</span> <span style="color: #333333">(</span><span style="color: #007700">max-width</span><span style="color: #333333">:</span> <span style="color: #007700">300px</span><span style="color: #333333">)</span> {
        <span style="color: #BB0066; font-weight: bold">.vertical-bars</span> <span style="color: #007700">li</span> {
          <span style="color: #008800; font-weight: bold">width</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">25px</span>; } }
      <span style="color: #BB0066; font-weight: bold">.vertical-bars</span> <span style="color: #007700">li</span> <span style="color: #007700">span</span><span style="color: #BB0066; font-weight: bold">.vbar</span> {
        <span style="color: #008800; font-weight: bold">position</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">absolute</span>;
        animation<span style="color: #333333">-</span>duration<span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">3s</span>;
       animation<span style="color: #333333">-</span>name<span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">height</span>;
        <span style="color: #008800; font-weight: bold">left</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0</span>;
        <span style="color: #008800; font-weight: bold">right</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0</span>;
        <span style="color: #008800; font-weight: bold">bottom</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0</span>; }
      <span style="color: #BB0066; font-weight: bold">.vertical-bar</span> <span style="color: #007700">li</span> <span style="color: #007700">span</span><span style="color: #BB0066; font-weight: bold">.title</span> {
        <span style="color: #008800; font-weight: bold">position</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">absolute</span>;
        <span style="color: #008800; font-weight: bold">left</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0</span>;
        <span style="color: #008800; font-weight: bold">right</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0</span>;
        <span style="color: #008800; font-weight: bold">text-align</span><span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">center</span>;
        <span style="color: #008800; font-weight: bold">bottom</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">-20px</span>; }
        
        <span style="color: #008800; font-weight: bold">@keyframes</span> <span style="color: #007700">height</span> {
        <span style="color: #007700">0</span><span style="color: #333333">%,</span> <span style="color: #007700">100</span><span style="color: #333333">%</span> {
            transition<span style="color: #333333">-</span>timing<span style="color: #333333">-</span>function<span style="color: #333333">:</span> cubic<span style="color: #333333">-</span>bezier(<span style="color: #6600EE; font-weight: bold">1</span><span style="color: #333333">,</span> <span style="color: #6600EE; font-weight: bold">0</span><span style="color: #333333">,</span> <span style="color: #6600EE; font-weight: bold">0</span><span style="color: #333333">.</span><span style="color: #6600EE; font-weight: bold">65</span><span style="color: #333333">,</span> <span style="color: #6600EE; font-weight: bold">0</span><span style="color: #333333">.</span><span style="color: #6600EE; font-weight: bold">85</span>);
        }
        <span style="color: #007700">0</span><span style="color: #333333">%</span> {
            <span style="color: #008800; font-weight: bold">height</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0</span>;
        }
        <span style="color: #007700">100</span><span style="color: #333333">%</span> {
            <span style="color: #008800; font-weight: bold">max-height</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">100</span><span style="color: #333333">%</span>;
        }

}
<span style="color: #333333">&lt;/</span><span style="color: #007700">style</span><span style="color: #333333">&gt;</span>

<span style="color: #557799">&lt;?php</span>

    <span style="color: #008800; font-weight: bold">function</span> <span style="color: #0066BB; font-weight: bold">last_line</span>(<span style="color: #996633">$filename</span>){
        <span style="color: #996633">$line</span> <span style="color: #333333">=</span> <span style="background-color: #fff0f0">&#39;&#39;</span>;

        <span style="color: #996633">$f</span> <span style="color: #333333">=</span> <span style="color: #007020">fopen</span>(<span style="color: #996633">$filename</span>, <span style="background-color: #fff0f0">&#39;r&#39;</span>);
        <span style="color: #996633">$cursor</span> <span style="color: #333333">=</span> <span style="color: #333333">-</span><span style="color: #0000DD; font-weight: bold">1</span>;

        <span style="color: #007020">fseek</span>(<span style="color: #996633">$f</span>, <span style="color: #996633">$cursor</span>, SEEK_END);
        <span style="color: #996633">$char</span> <span style="color: #333333">=</span> <span style="color: #007020">fgetc</span>(<span style="color: #996633">$f</span>);

        <span style="color: #DD4422">/**</span>
<span style="color: #DD4422">        * Trim trailing newline chars of the file</span>
<span style="color: #DD4422">        */</span>
        <span style="color: #008800; font-weight: bold">while</span> (<span style="color: #996633">$char</span> <span style="color: #333333">===</span> <span style="background-color: #fff0f0">&quot;</span><span style="color: #666666; font-weight: bold; background-color: #fff0f0">\n</span><span style="background-color: #fff0f0">&quot;</span> <span style="color: #333333">||</span> <span style="color: #996633">$char</span> <span style="color: #333333">===</span> <span style="background-color: #fff0f0">&quot;</span><span style="color: #666666; font-weight: bold; background-color: #fff0f0">\r</span><span style="background-color: #fff0f0">&quot;</span>) {
            <span style="color: #007020">fseek</span>(<span style="color: #996633">$f</span>, <span style="color: #996633">$cursor</span><span style="color: #333333">--</span>, SEEK_END);
            <span style="color: #996633">$char</span> <span style="color: #333333">=</span> <span style="color: #007020">fgetc</span>(<span style="color: #996633">$f</span>);
        }

        <span style="color: #DD4422">/**</span>
<span style="color: #DD4422">        * Read until the start of file or first newline char</span>
<span style="color: #DD4422">        */</span>
        <span style="color: #008800; font-weight: bold">while</span> (<span style="color: #996633">$char</span> <span style="color: #333333">!==</span> <span style="color: #008800; font-weight: bold">false</span> <span style="color: #333333">&amp;&amp;</span> <span style="color: #996633">$char</span> <span style="color: #333333">!==</span> <span style="background-color: #fff0f0">&quot;</span><span style="color: #666666; font-weight: bold; background-color: #fff0f0">\n</span><span style="background-color: #fff0f0">&quot;</span> <span style="color: #333333">&amp;&amp;</span> <span style="color: #996633">$char</span> <span style="color: #333333">!==</span> <span style="background-color: #fff0f0">&quot;</span><span style="color: #666666; font-weight: bold; background-color: #fff0f0">\r</span><span style="background-color: #fff0f0">&quot;</span>) {
            <span style="color: #DD4422">/**</span>
<span style="color: #DD4422">            * Prepend the new char</span>
<span style="color: #DD4422">            */</span>
            <span style="color: #996633">$line</span> <span style="color: #333333">=</span> <span style="color: #996633">$char</span> <span style="color: #333333">.</span> <span style="color: #996633">$line</span>;
            <span style="color: #007020">fseek</span>(<span style="color: #996633">$f</span>, <span style="color: #996633">$cursor</span><span style="color: #333333">--</span>, SEEK_END);
            <span style="color: #996633">$char</span> <span style="color: #333333">=</span> <span style="color: #007020">fgetc</span>(<span style="color: #996633">$f</span>);
        }
        <span style="color: #008800; font-weight: bold">return</span> <span style="color: #996633">$line</span>;
    }

    <span style="color: #996633">$living</span> <span style="color: #333333">=</span> last_line(<span style="background-color: #fff0f0">&#39;temps.txt&#39;</span>);
    <span style="color: #996633">$alivin</span> <span style="color: #333333">=</span> <span style="color: #007020">explode</span>(<span style="background-color: #fff0f0">&quot;_&quot;</span>, <span style="color: #996633">$living</span>);

    <span style="color: #008800; font-weight: bold">if</span>(<span style="color: #996633">$alivin</span>[<span style="color: #0000DD; font-weight: bold">2</span>] <span style="color: #333333">&lt;</span> <span style="color: #0000DD; font-weight: bold">110</span> <span style="color: #333333">&amp;&amp;</span> <span style="color: #996633">$alivin</span>[<span style="color: #0000DD; font-weight: bold">3</span>] <span style="color: #333333">&lt;</span> <span style="color: #0000DD; font-weight: bold">110</span>){
        <span style="color: #996633">$tempp</span> <span style="color: #333333">=</span>  ( <span style="color: #996633">$alivin</span>[<span style="color: #0000DD; font-weight: bold">2</span>] <span style="color: #333333">/</span> <span style="color: #0000DD; font-weight: bold">35</span> ) <span style="color: #333333">*</span> <span style="color: #0000DD; font-weight: bold">100</span>;
    }<span style="color: #008800; font-weight: bold">else</span>{
            <span style="color: #996633">$alivin</span>[<span style="color: #0000DD; font-weight: bold">3</span>] <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">0</span>;
    }
<span style="color: #557799">?&gt;</span>

<span style="color: #333333">&lt;</span><span style="color: #007700">center</span><span style="color: #333333">&gt;</span><span style="color: #007700">Current</span><span style="color: #333333">&lt;/</span><span style="color: #007700">center</span><span style="color: #333333">&gt;&lt;</span><span style="color: #007700">br</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;</span><span style="color: #007700">div</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;container-fluid&quot;</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;</span><span style="color: #007700">div</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;row&quot;</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;</span><span style="color: #007700">div</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;col-sm-4&quot;</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;</span><span style="color: #007700">div</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;vertical-bars xs-center&quot;</span><span style="color: #333333">&gt;</span>
    <span style="color: #333333">&lt;</span><span style="color: #007700">ul</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;list-inline&quot;</span><span style="color: #333333">&gt;</span>
        <span style="color: #333333">&lt;</span><span style="color: #007700">li</span><span style="color: #333333">&gt;&lt;</span><span style="color: #007700">span</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;vbar&quot;</span> <span style="color: #007700">style</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;height:</span><span style="color: #557799">&lt;?</span><span style="color: #333333">=</span><span style="color: #996633">$tempp</span><span style="color: #557799">?&gt;</span><span style="background-color: #fff0f0">%; background:#009900;&quot;</span><span style="color: #333333">&gt;&lt;/</span><span style="color: #007700">span</span><span style="color: #333333">&gt;&lt;</span><span style="color: #007700">span</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;title&quot;</span><span style="color: #333333">&gt;</span><span style="color: #557799">&lt;?</span><span style="color: #333333">=</span><span style="color: #996633">$alivin</span>[<span style="color: #0000DD; font-weight: bold">2</span>]<span style="color: #557799">?&gt;</span><span style="color: #007700">C</span><span style="color: #333333">&lt;/</span><span style="color: #007700">span</span><span style="color: #333333">&gt;&lt;/</span><span style="color: #007700">li</span><span style="color: #333333">&gt;</span>
        <span style="color: #333333">&lt;</span><span style="color: #007700">li</span><span style="color: #333333">&gt;&lt;</span><span style="color: #007700">span</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;vbar&quot;</span> <span style="color: #007700">style</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;height:</span><span style="color: #557799">&lt;?</span><span style="color: #333333">=</span><span style="color: #996633">$alivin</span>[<span style="color: #0000DD; font-weight: bold">3</span>]<span style="color: #557799">?&gt;</span><span style="background-color: #fff0f0">%; background:#00bbff;&quot;</span><span style="color: #333333">&gt;&lt;/</span><span style="color: #007700">span</span><span style="color: #333333">&gt;&lt;</span><span style="color: #007700">span</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;title&quot;</span><span style="color: #333333">&gt;</span><span style="color: #557799">&lt;?</span><span style="color: #333333">=</span><span style="color: #996633">$alivin</span>[<span style="color: #0000DD; font-weight: bold">3</span>]<span style="color: #557799">?&gt;</span><span style="color: #333333">%&lt;/</span><span style="color: #007700">span</span><span style="color: #333333">&gt;&lt;/</span><span style="color: #007700">li</span><span style="color: #333333">&gt;</span>
        <span style="color: #333333">&lt;</span><span style="color: #007700">li</span><span style="color: #333333">&gt;&lt;</span><span style="color: #007700">span</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;vbar&quot;</span> <span style="color: #007700">style</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;height:75%; background:#ffaa00;&quot;</span><span style="color: #333333">&gt;&lt;/</span><span style="color: #007700">span</span><span style="color: #333333">&gt;&lt;</span><span style="color: #007700">span</span> <span style="color: #007700">class</span><span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;title&quot;</span><span style="color: #333333">&gt;</span><span style="color: #007700">100</span><span style="color: #333333">&lt;/</span><span style="color: #007700">span</span><span style="color: #333333">&gt;&lt;/</span><span style="color: #007700">li</span><span style="color: #333333">&gt;</span>
    <span style="color: #333333">&lt;/</span><span style="color: #007700">ul</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;/</span><span style="color: #007700">div</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;/</span><span style="color: #007700">div</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;/</span><span style="color: #007700">div</span><span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;/</span><span style="color: #007700">div</span><span style="color: #333333">&gt;</span>
</pre></div>


<br>
<br>
The charts are build using <a href="www.highcharts.com">hicharts</a> it receives the data as JSON arrays. <br>
<img src="hichart.jpg" /> <br>
<br>
The following php code will read the generated text files as in the exampole above and outputs all the data as a big json array:<br>
<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #557799">&lt;?php</span>
<span style="color: #996633">$dfield</span> <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">2</span>;
<span style="color: #008800; font-weight: bold">if</span>(<span style="color: #007020">isset</span>(<span style="color: #996633">$_GET</span>[<span style="background-color: #fff0f0">&#39;typenr&#39;</span>])){

	<span style="color: #996633">$dfield</span> <span style="color: #333333">=</span> <span style="color: #996633">$_GET</span>[<span style="background-color: #fff0f0">&#39;typenr&#39;</span>];
}

<span style="color: #008800; font-weight: bold">if</span>(<span style="color: #007020">isset</span>(<span style="color: #996633">$_GET</span>[<span style="background-color: #fff0f0">&#39;name&#39;</span>]) <span style="color: #333333">&amp;&amp;</span> <span style="color: #007020">strlen</span>(<span style="color: #996633">$_GET</span>[<span style="background-color: #fff0f0">&#39;name&#39;</span>]) <span style="color: #333333">&lt;</span> <span style="color: #0000DD; font-weight: bold">6</span>){
    <span style="color: #996633">$filename</span> <span style="color: #333333">=</span> <span style="color: #996633">$_GET</span>[<span style="background-color: #fff0f0">&#39;name&#39;</span>]<span style="color: #333333">.</span><span style="background-color: #fff0f0">&quot;.txt&quot;</span>;

    <span style="color: #996633">$notFirst</span> <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">false</span>;
    <span style="color: #008800; font-weight: bold">echo</span> <span style="background-color: #fff0f0">&quot;[&quot;</span>;
    <span style="color: #996633">$handle</span> <span style="color: #333333">=</span> <span style="color: #007020">fopen</span>(<span style="color: #996633">$filename</span>, <span style="background-color: #fff0f0">&quot;r&quot;</span>);
    <span style="color: #008800; font-weight: bold">if</span> (<span style="color: #996633">$handle</span>) {
        <span style="color: #008800; font-weight: bold">while</span> ((<span style="color: #996633">$line</span> <span style="color: #333333">=</span> <span style="color: #007020">fgets</span>(<span style="color: #996633">$handle</span>)) <span style="color: #333333">!==</span> <span style="color: #008800; font-weight: bold">false</span>) {
            <span style="color: #888888">// process the line read.</span>
            <span style="color: #996633">$aData</span> <span style="color: #333333">=</span> <span style="color: #007020">explode</span>(<span style="background-color: #fff0f0">&quot;_&quot;</span>,<span style="color: #996633">$line</span>);
            <span style="color: #008800; font-weight: bold">if</span>(<span style="color: #996633">$aData</span> <span style="color: #333333">!==</span> <span style="color: #008800; font-weight: bold">false</span> <span style="color: #333333">&amp;&amp;</span> <span style="color: #996633">$aData</span>[<span style="color: #996633">$dfield</span>] <span style="color: #333333">&gt;</span> <span style="color: #0000DD; font-weight: bold">10</span> <span style="color: #333333">&amp;&amp;</span> <span style="color: #996633">$aData</span>[<span style="color: #996633">$dfield</span>] <span style="color: #333333">&lt;</span> <span style="color: #0000DD; font-weight: bold">110</span>){
            <span style="color: #008800; font-weight: bold">if</span>(<span style="color: #996633">$notFirst</span>){
                <span style="color: #008800; font-weight: bold">echo</span> <span style="background-color: #fff0f0">&quot;,&quot;</span>;
            }
            <span style="color: #008800; font-weight: bold">echo</span><span style="background-color: #fff0f0">&quot;[&quot;</span><span style="color: #333333">.</span><span style="color: #007020">strtotime</span>(<span style="color: #996633">$aData</span>[<span style="color: #0000DD; font-weight: bold">0</span>])<span style="color: #333333">.</span><span style="background-color: #fff0f0">&quot;,&quot;</span><span style="color: #333333">.</span><span style="color: #996633">$aData</span>[<span style="color: #996633">$dfield</span>]<span style="color: #333333">.</span><span style="background-color: #fff0f0">&quot;]&quot;</span>;
            <span style="color: #008800; font-weight: bold">echo</span> <span style="background-color: #fff0f0">&quot;</span><span style="color: #666666; font-weight: bold; background-color: #fff0f0">\n</span><span style="background-color: #fff0f0">&quot;</span>;
            <span style="color: #996633">$notFirst</span><span style="color: #333333">=</span><span style="color: #008800; font-weight: bold">true</span>;
            }
        }

        <span style="color: #007020">fclose</span>(<span style="color: #996633">$handle</span>);
        <span style="color: #008800; font-weight: bold">echo</span> <span style="background-color: #fff0f0">&quot;]&quot;</span>;
    } <span style="color: #008800; font-weight: bold">else</span> {
        <span style="color: #888888">// error opening the file.</span>
    } 
}
<span style="color: #557799">?&gt;</span>
</pre></div>
<br>
<br>
Then the following code will generate 2 graphs, one for the temperature and one for humidity. The data is received using ajax.<br>
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #007700">&lt;script </span><span style="color: #0000CC">src=</span><span style="background-color: #fff0f0">&quot;https://code.highcharts.com/highcharts.js&quot;</span><span style="color: #007700">&gt;&lt;/script&gt;</span>
<span style="color: #007700">&lt;script </span><span style="color: #0000CC">src=</span><span style="background-color: #fff0f0">&quot;https://code.highcharts.com/modules/exporting.js&quot;</span><span style="color: #007700">&gt;&lt;/script&gt;</span>
<span style="color: #007700">&lt;script </span><span style="color: #0000CC">type=</span><span style="background-color: #fff0f0">&quot;text/javascript&quot;</span><span style="color: #007700">&gt;</span>

$(<span style="color: #007020">document</span>).ready(<span style="color: #008800; font-weight: bold">function</span> () {
   $.getJSON(<span style="background-color: #fff0f0">&#39;getdata.php?name=temp&#39;</span>, <span style="color: #008800; font-weight: bold">function</span> (data) {

        $(<span style="background-color: #fff0f0">&#39;#container_temp&#39;</span>).highcharts({
            chart<span style="color: #333333">:</span> {
                zoomType<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;x&#39;</span>
            },
            title<span style="color: #333333">:</span> {
                text<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;temps&#39;</span>
            },
            subtitle<span style="color: #333333">:</span> {
                text<span style="color: #333333">:</span> <span style="color: #007020">document</span>.ontouchstart <span style="color: #333333">===</span> <span style="color: #008800; font-weight: bold">undefined</span> <span style="color: #333333">?</span>
                        <span style="background-color: #fff0f0">&#39;Click and drag in the plot area to zoom in&#39;</span> <span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;Pinch the chart to zoom in&#39;</span>
            },
            xAxis<span style="color: #333333">:</span> {
                type<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;datetime&#39;</span>
            },
            yAxis<span style="color: #333333">:</span> {
                title<span style="color: #333333">:</span> {
                    text<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;Temps&#39;</span>
                },
		plotLines<span style="color: #333333">:</span> [{
                    value<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">20</span>,
                    color<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;green&#39;</span>,
                    dashStyle<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;shortdash&#39;</span>,
                    width<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">2</span>,
                    label<span style="color: #333333">:</span> {
                        text<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;minimum&#39;</span>
                    }
                }, {
                    value<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">30</span>,
                    color<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;red&#39;</span>,
                    dashStyle<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;shortdash&#39;</span>,
                    width<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">2</span>,
                    label<span style="color: #333333">:</span> {
                        text<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;maximum&#39;</span>
                    }
                }]
            },
            legend<span style="color: #333333">:</span> {
                enabled<span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">false</span>
            },
            plotOptions<span style="color: #333333">:</span> {
                area<span style="color: #333333">:</span> {
                    fillColor<span style="color: #333333">:</span> {
                        linearGradient<span style="color: #333333">:</span> {
                            x1<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">0</span>,
                            y1<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">0</span>,
                            x2<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">0</span>,
                            y2<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">1</span>
                        },
                        stops<span style="color: #333333">:</span> [
                            [<span style="color: #0000DD; font-weight: bold">0</span>, Highcharts.getOptions().colors[<span style="color: #0000DD; font-weight: bold">2</span>]],
                            [<span style="color: #0000DD; font-weight: bold">1</span>, Highcharts.Color(Highcharts.getOptions().colors[<span style="color: #0000DD; font-weight: bold">2</span>]).setOpacity(<span style="color: #0000DD; font-weight: bold">0</span>).get(<span style="background-color: #fff0f0">&#39;rgba&#39;</span>)]
                        ]
                    },
                    marker<span style="color: #333333">:</span> {
                        radius<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">2</span>
                    },
                    lineWidth<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">1</span>,
                    states<span style="color: #333333">:</span> {
                        hover<span style="color: #333333">:</span> {
                            lineWidth<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">1</span>
                        }
                    },
                    threshold<span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">null</span>
                }
            },

            series<span style="color: #333333">:</span> [{
                type<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;area&#39;</span>,
		color<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;#009900&#39;</span>,
                name<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;temp1&#39;</span>,
                data<span style="color: #333333">:</span> data
            }]
        });
      });
   <span style="color: #888888">// });</span>
   
     $.getJSON(<span style="background-color: #fff0f0">&#39;getdata.php?name=temp&amp;typenr=3&#39;</span>, <span style="color: #008800; font-weight: bold">function</span> (data) {

        $(<span style="background-color: #fff0f0">&#39;#container_hum&#39;</span>).highcharts({
            chart<span style="color: #333333">:</span> {
                zoomType<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;x&#39;</span>
            },
            title<span style="color: #333333">:</span> {
                text<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;humidity&#39;</span>
            },
            subtitle<span style="color: #333333">:</span> {
                text<span style="color: #333333">:</span> <span style="color: #007020">document</span>.ontouchstart <span style="color: #333333">===</span> <span style="color: #008800; font-weight: bold">undefined</span> <span style="color: #333333">?</span>
                        <span style="background-color: #fff0f0">&#39;Click and drag in the plot area to zoom in&#39;</span> <span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;Pinch the chart to zoom in&#39;</span>
            },
            xAxis<span style="color: #333333">:</span> {
                type<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;datetime&#39;</span>
            },
            yAxis<span style="color: #333333">:</span> {
                title<span style="color: #333333">:</span> {
                    text<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;Hum&#39;</span>
                },
		plotLines<span style="color: #333333">:</span> [{
                    value<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">50</span>,
                    color<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;blue&#39;</span>,
                    dashStyle<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;shortdash&#39;</span>,
                    width<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">2</span>,
                    label<span style="color: #333333">:</span> {
                        text<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;minimum&#39;</span>
                    }
                }]
            },
            legend<span style="color: #333333">:</span> {
                enabled<span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">false</span>
            },
            plotOptions<span style="color: #333333">:</span> {
                area<span style="color: #333333">:</span> {
                    fillColor<span style="color: #333333">:</span> {
                        linearGradient<span style="color: #333333">:</span> {
                            x1<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">0</span>,
                            y1<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">0</span>,
                            x2<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">0</span>,
                            y2<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">1</span>
                        },
                        stops<span style="color: #333333">:</span> [
                            [<span style="color: #0000DD; font-weight: bold">0</span>, Highcharts.getOptions().colors[<span style="color: #0000DD; font-weight: bold">0</span>]],
                            [<span style="color: #0000DD; font-weight: bold">1</span>, Highcharts.Color(Highcharts.getOptions().colors[<span style="color: #0000DD; font-weight: bold">0</span>]).setOpacity(<span style="color: #0000DD; font-weight: bold">0</span>).get(<span style="background-color: #fff0f0">&#39;rgba&#39;</span>)]
                        ]
                    },
                    marker<span style="color: #333333">:</span> {
                        radius<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">2</span>
                    },
                    lineWidth<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">1</span>,
                    states<span style="color: #333333">:</span> {
                        hover<span style="color: #333333">:</span> {
                            lineWidth<span style="color: #333333">:</span> <span style="color: #0000DD; font-weight: bold">1</span>
                        }
                    },
                    threshold<span style="color: #333333">:</span> <span style="color: #008800; font-weight: bold">null</span>
                }
            },

            series<span style="color: #333333">:</span> [{
                type<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;area&#39;</span>,
                name<span style="color: #333333">:</span> <span style="background-color: #fff0f0">&#39;hum&#39;</span>,
                data<span style="color: #333333">:</span> data
            }]
        });
      });

  });
<span style="color: #007700">&lt;/script&gt;</span>

<span style="color: #007700">&lt;div</span> <span style="color: #0000CC">id=</span><span style="background-color: #fff0f0">&quot;container_temp&quot;</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;min-width: 310px; height: 300px; margin: 0 auto&quot;</span><span style="color: #007700">&gt;&lt;/div&gt;</span>
<span style="color: #007700">&lt;div</span> <span style="color: #0000CC">id=</span><span style="background-color: #fff0f0">&quot;container_hum&quot;</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;min-width: 310px; height: 300px; margin: 0 auto&quot;</span><span style="color: #007700">&gt;&lt;/div&gt;</span>


 
</pre></div>

<hr/>
<br><br>

<h2> Version 2 Lights </h2>
<br>
The second greenhouse will have 4x40cm of special grow light strip. That will be a total of 96 type 5050 leds drawing 12 Watts of power.<br>
<br>
I made 2 bars with 2 strips each for an even distribution but less mounting work and more space to access the plants.<br>
<img src="strips1.jpg" /><br>
The led color will be in a ratio of 4 red one blue led. As you can see below.
<br>
<img src="leds2.jpg" /><br>
The image below shows why the green part of the light spectrum is not needed.
<br>
<img src="spectrum.jpg" /><br>


<h2> Fans </h2>
<br>
<br>
It won't happen often, but a small fan would help a lot if the temperature gets too high, every 15 minutes the temperature will be checked, if it is above 30 degrees, then the fan will run until the next measurement. It will also be driven by a mosfet, but the speed will be brought down by a LM317 and a variable resistor.<br>
<img src="fan.jpg" /><br>


<h2> Light intensity sensor</h2>
<br>
<br>
The light intensity can easily be measured by using an LDR. But unfortunately, the raspberry pi has no analog inputs!. It is advised to use an mcp3008. That is a 10bit ADC with an SPI interface with 8 analog inputs. I will use the first one for light inside the case, one for outside light. The other six can be used for soil measurement. <br>
The LDR will simply be connected as a voltage divider with a resistor of 1k.  
<br>
<img src="mcp3008.jpg" /> . <img src="ldr.jpg" /><br>
<br>
The reading will be done with a python script as shown below. <br>
<br>
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">time</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">os</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">RPi.GPIO</span> <span style="color: #008800; font-weight: bold">as</span> <span style="color: #0e84b5; font-weight: bold">GPIO</span>

GPIO<span style="color: #333333">.</span>setwarnings(<span style="color: #007020">False</span>)
GPIO<span style="color: #333333">.</span>setmode(GPIO<span style="color: #333333">.</span>BCM)
DEBUG <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">1</span>

<span style="color: #888888"># read SPI data from MCP3008 chip, 8 possible adc&#39;s (0 thru 7)</span>
<span style="color: #008800; font-weight: bold">def</span> <span style="color: #0066BB; font-weight: bold">readadc</span>(adcnum, clockpin, mosipin, misopin, cspin):
        <span style="color: #008800; font-weight: bold">if</span> ((adcnum <span style="color: #333333">&gt;</span> <span style="color: #0000DD; font-weight: bold">7</span>) <span style="color: #000000; font-weight: bold">or</span> (adcnum <span style="color: #333333">&lt;</span> <span style="color: #0000DD; font-weight: bold">0</span>)):
                <span style="color: #008800; font-weight: bold">return</span> <span style="color: #333333">-</span><span style="color: #0000DD; font-weight: bold">1</span>
        GPIO<span style="color: #333333">.</span>output(cspin, <span style="color: #007020">True</span>)

        GPIO<span style="color: #333333">.</span>output(clockpin, <span style="color: #007020">False</span>)  <span style="color: #888888"># start clock low</span>
        GPIO<span style="color: #333333">.</span>output(cspin, <span style="color: #007020">False</span>)     <span style="color: #888888"># bring CS low</span>

        commandout <span style="color: #333333">=</span> adcnum
        commandout <span style="color: #333333">|=</span> <span style="color: #005588; font-weight: bold">0x18</span>  <span style="color: #888888"># start bit + single-ended bit</span>
        commandout <span style="color: #333333">&lt;&lt;=</span> <span style="color: #0000DD; font-weight: bold">3</span>    <span style="color: #888888"># we only need to send 5 bits here</span>
        <span style="color: #008800; font-weight: bold">for</span> i <span style="color: #000000; font-weight: bold">in</span> <span style="color: #007020">range</span>(<span style="color: #0000DD; font-weight: bold">5</span>):
                <span style="color: #008800; font-weight: bold">if</span> (commandout <span style="color: #333333">&amp;</span> <span style="color: #005588; font-weight: bold">0x80</span>):
                        GPIO<span style="color: #333333">.</span>output(mosipin, <span style="color: #007020">True</span>)
                <span style="color: #008800; font-weight: bold">else</span>:
                        GPIO<span style="color: #333333">.</span>output(mosipin, <span style="color: #007020">False</span>)
                commandout <span style="color: #333333">&lt;&lt;=</span> <span style="color: #0000DD; font-weight: bold">1</span>
                GPIO<span style="color: #333333">.</span>output(clockpin, <span style="color: #007020">True</span>)
                time<span style="color: #333333">.</span>sleep(<span style="color: #6600EE; font-weight: bold">0.01</span>)
                GPIO<span style="color: #333333">.</span>output(clockpin, <span style="color: #007020">False</span>)
                time<span style="color: #333333">.</span>sleep(<span style="color: #6600EE; font-weight: bold">0.01</span>)
        adcout <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">0</span>
        <span style="color: #888888"># read in one empty bit, one null bit and 10 ADC bits</span>
        <span style="color: #008800; font-weight: bold">for</span> i <span style="color: #000000; font-weight: bold">in</span> <span style="color: #007020">range</span>(<span style="color: #0000DD; font-weight: bold">12</span>):
                GPIO<span style="color: #333333">.</span>output(clockpin, <span style="color: #007020">True</span>)
                time<span style="color: #333333">.</span>sleep(<span style="color: #6600EE; font-weight: bold">0.01</span>)
                GPIO<span style="color: #333333">.</span>output(clockpin, <span style="color: #007020">False</span>)
                time<span style="color: #333333">.</span>sleep(<span style="color: #6600EE; font-weight: bold">0.01</span>)
                adcout <span style="color: #333333">&lt;&lt;=</span> <span style="color: #0000DD; font-weight: bold">1</span>
                <span style="color: #008800; font-weight: bold">if</span> (GPIO<span style="color: #333333">.</span>input(misopin)):
                        adcout <span style="color: #333333">|=</span> <span style="color: #005588; font-weight: bold">0x1</span>

        GPIO<span style="color: #333333">.</span>output(cspin, <span style="color: #007020">True</span>)
        
        adcout <span style="color: #333333">&gt;&gt;=</span> <span style="color: #0000DD; font-weight: bold">1</span>       <span style="color: #888888"># first bit is &#39;null&#39; so drop it</span>
        <span style="color: #008800; font-weight: bold">return</span> adcout

<span style="color: #888888"># change these as desired - they&#39;re the pins connected from the</span>
<span style="color: #888888"># SPI port on the ADC to the Cobbler</span>
SPICLK <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">17</span>
SPIMISO <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">23</span>
SPIMOSI <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">24</span>
SPICS <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">25</span>

<span style="color: #888888"># set up the SPI interface pins</span>
GPIO<span style="color: #333333">.</span>setup(SPIMOSI, GPIO<span style="color: #333333">.</span>OUT)
GPIO<span style="color: #333333">.</span>setup(SPIMISO, GPIO<span style="color: #333333">.</span>IN)
GPIO<span style="color: #333333">.</span>setup(SPICLK, GPIO<span style="color: #333333">.</span>OUT)
GPIO<span style="color: #333333">.</span>setup(SPICS, GPIO<span style="color: #333333">.</span>OUT)

last_read <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">0</span>       <span style="color: #888888"># this keeps track of the last potentiometer value</span>
tolerance <span style="color: #333333">=</span> <span style="color: #0000DD; font-weight: bold">5</span>       <span style="color: #888888"># to keep from being jittery we&#39;ll only change</span>
                    <span style="color: #888888"># volume when the pot has moved more than 5 &#39;counts&#39;</span>

<span style="color: #888888"># we&#39;ll assume that the pot didn&#39;t move</span>

<span style="color: #888888"># read the analog pin</span>
ldr1 <span style="color: #333333">=</span> readadc(<span style="color: #0000DD; font-weight: bold">2</span>, SPICLK, SPIMOSI, SPIMISO, SPICS)
ldr2 <span style="color: #333333">=</span> readadc(<span style="color: #0000DD; font-weight: bold">3</span>, SPICLK, SPIMOSI, SPIMISO, SPICS)
mois1 <span style="color: #333333">=</span> readadc(<span style="color: #0000DD; font-weight: bold">5</span>, SPICLK, SPIMOSI, SPIMISO, SPICS)
mois2 <span style="color: #333333">=</span> readadc(<span style="color: #0000DD; font-weight: bold">7</span>, SPICLK, SPIMOSI, SPIMISO, SPICS)
        
<span style="color: #008800; font-weight: bold">print</span> time<span style="color: #333333">.</span>strftime(<span style="background-color: #fff0f0">&quot;%Y-%m-</span><span style="background-color: #eeeeee">%d</span><span style="background-color: #fff0f0"> %H:%M:%S&quot;</span>)<span style="color: #333333">+</span><span style="background-color: #fff0f0">&#39;_0_&#39;</span><span style="color: #333333">+</span><span style="color: #007020">str</span>(ldr1)<span style="color: #333333">+</span><span style="background-color: #fff0f0">&#39;_&#39;</span><span style="color: #333333">+</span><span style="color: #007020">str</span>(ldr2)<span style="color: #333333">+</span><span style="background-color: #fff0f0">&#39;_&#39;</span><span style="color: #333333">+</span><span style="color: #007020">str</span>(mois1)<span style="color: #333333">+</span><span style="background-color: #fff0f0">&#39;_&#39;</span><span style="color: #333333">+</span><span style="color: #007020">str</span>(mois2)
</pre></div>


<h2> Soil moisture </h2>
<br>
<br>
Soil has different resistances in relation to its dryness. I got a couple of probes which are just coated copper plates. I put one in a dry part and one in a wet part of the ground and measured the resistance. one was 10k ohm and the dry one about 80k. So for soil moisture, I used the same chip as for the light meter, only a 50K resistor instead of 1k. <br>
<img src="soilprobe.jpg" /><img src="soil2.jpg" /><br>




<h2> Water pump </h2>
<br>
<br>
The water pump has a flow of 40ml per second when it runs on 12V. at 5V the flow is 20ml per second, the pump will not function properly on a lower voltage. A 2 liter milk can will be the water store. A water level indicator will check if there is enough water in the can and can alert if not.<br>
<pre>
  gpio -g mode 11 input up
  gpio -g read 11
</pre>
<img src="pump1.jpg" /> . <img src="pump2.jpg" /> <br>
The water tube will go to a splitter, each small tube will feed a plant.<br>
<br>
<img src="splitter1.jpg" /> . <img src="splitter2.jpg" /> <br>



<br>
<br>
<h2> Experiment ! </h2>
<br>

<br>
<video width="800" height="600" controls>
  <source src="timelapse1.mp4" type="video/mp4">
  <source src="timelapse1.webm" type="video/webm">
Your browser does not support the video tag.
</video> 

<br><BR>
Below is an image of the main board. On left left is a raspberry pi 2. The top right is a LM2596 DC DC converter. On the right side you see 3 FQP30N06 MOSFETS that drive the lights, fan and water pump. For the fan and water pump there are also 2 LM317 voltage regulators with 2 variable resitors next to it. And finally in the center is the MCP3008 with a ldr connected to one input..<br>
<img src="board.jpg" />

</html>
