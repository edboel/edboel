/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * logtemps.c
 * e.boelen 6 jan 2011
 * arduino temp logger
 */

#include <Time.h>
#include <SdFat.h>
#include <SdFatUtil.h>
#include <SPI.h>
#include <Ethernet.h>
#include <OneWire.h>
#include <DallasTemperature.h>

/************ ETHERNET STUFF ************/
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte ip[] = { 
  192, 168, 10, 100 };
Server server(80);

/************ SDCARD STUFF ************/
Sd2Card card;
SdVolume volume;
SdFile root;
SdFile file;
char filename[] = "temps.log";

/************ TIMER STUFF ************/
unsigned long  time33mins;
unsigned long  time60mins;
//Time time;


/************ TEMPERATURE STUFF ************/
#define ONE_WIRE_BUS 7
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWirea(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensorsa(&oneWirea);

OneWire oneWireb(6);
DallasTemperature sensorsb(&oneWireb);

OneWire oneWirec(5);
DallasTemperature sensorsc(&oneWirec);

OneWire oneWired(5);
DallasTemperature sensorsd(&oneWired);

// store error strings in flash to save RAM
#define error(s) error_P(PSTR(s))

void error_P(const char* str) {
  PgmPrint("error: ");
  SerialPrintln_P(str);
  if (card.errorCode()) {
    PgmPrint("SD error: ");
    //Serial.print(card.errorCode(), HEX);
    //Serial.print(',');
    //Serial.println(card.errorData(), HEX);
  }
  while(1);
}

void setup() {
  //Serial.begin(9600);

  PgmPrint("Free RAM: ");
  //Serial.println(FreeRam());  

  // initialize the SD card at SPI_HALF_SPEED to avoid bus errors with
  // breadboards.  use SPI_FULL_SPEED for better performance.
  pinMode(10, OUTPUT);                       // set the SS pin as an output (necessary!)
  digitalWrite(10, HIGH);                    // but turn off the W5100 chip!

  if (!card.init(SPI_HALF_SPEED, 4)) error("card.init failed!");

  // initialize a FAT volume
  if (!volume.init(&card)) error("vol.init failed!");

  PgmPrint("Volume is FAT");
  //Serial.println(volume.fatType(),DEC);
  //Serial.println();

  if (!root.openRoot(&volume)) error("openRoot failed");

  // list file in root with date and size
  PgmPrintln("Files found in root:");
  root.ls(LS_DATE | LS_SIZE);
  //Serial.println();

  // Recursive list of all directories
  PgmPrintln("Files found in all dirs:");
  root.ls(LS_R);

  //Serial.println();
  PgmPrintln("Done");

  // Debugging complete, we start the server!
  Ethernet.begin(mac, ip);
  server.begin();
  
  // Start up the temperature library
  sensorsa.begin();
  sensorsb.begin();
  sensorsc.begin();
  sensorsd.begin();
  
  setTime(0); // start the clock
  time33mins = time60mins = now();

}

void LogTemps(){
   if (!file.open(root, filename, O_CREAT | O_APPEND | O_WRITE)) {
     //error(“open”);
   }
   
// write values to the file

          sensorsa.requestTemperatures(); 
          sensorsb.requestTemperatures(); 
          sensorsc.requestTemperatures(); 
          sensorsd.requestTemperatures(); 
          

file.print(now() );
file.print("|");
file.print(sensorsa.getTempCByIndex(0));
file.print("|");
file.print(sensorsb.getTempCByIndex(0));
file.print("|");
file.print(sensorsc.getTempCByIndex(0));
file.print("|");
file.print(sensorsd.getTempCByIndex(0));
file.print("\n");

if (!file.close() || file.writeError){
 // error(“close/write”);
}
  
}

void ListFiles(Client client, uint8_t flags) {
  // This code is just copied from SdFile.cpp in the SDFat library
  // and tweaked to print to the client output in html!
  dir_t p;

  root.rewind();
  while (root.readDir(p) > 0) {
    // done if past last used entry
    if (p.name[0] == DIR_NAME_FREE) break;

    // skip deleted entry and entries for . and  ..
    if (p.name[0] == DIR_NAME_DELETED || p.name[0] == '.') continue;

    // only list subdirectories and files
    if (!DIR_IS_FILE_OR_SUBDIR(&p)) continue;


    // print file name with possible blank fill
    //root.printDirName(*p, flags & (LS_DATE | LS_SIZE) ? 14 : 0);


    for (uint8_t i = 0; i < 11; i++) {
      if (p.name[i] == ' ') continue;
      if (i == 8) {
        client.print('.');
      }
      client.print(p.name[i]);
    }
    if (DIR_IS_SUBDIR(&p)) {
      client.print('/');
    }

    // print modify date/time if requested
    if (flags & LS_DATE) {
      root.printFatDate(p.lastWriteDate);
      client.print(' ');
      root.printFatTime(p.lastWriteTime);
    }
    // print size if requested
    if (!DIR_IS_SUBDIR(&p) && (flags & LS_SIZE)) {
      client.print(' ');
      client.print(p.fileSize);
    }
    client.println("<br>");
  }
}


void loop()
{
  Client client = server.available();
  if (client) {
    // an http request ends with a blank line
    boolean current_line_is_blank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        // if we've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so we can send a reply
        if (c == '\n' && current_line_is_blank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println();

          // print all the files, use a helper to keep it clean
          //ListFiles(client, 0);
          client.println("<h2>Temperature:</h2>");
          
          sensorsa.requestTemperatures(); 
          sensorsb.requestTemperatures(); 
          sensorsc.requestTemperatures(); 
          sensorsd.requestTemperatures(); 
          client.println(sensorsa.getTempCByIndex(0));
           client.println("<br>");
           client.println(sensorsb.getTempCByIndex(1));
           client.println("<br>");
           client.println(sensorsc.getTempCByIndex(2));
           client.println("<br>");
           client.println(sensorsd.getTempCByIndex(3));
           client.println("<br><br>..");
          //ListFiles(client, 0);

          break;
        }
        if (c == '\n') {
          // we're starting a new line
          current_line_is_blank = true;
        } 
        else if (c != '\r') {
          // we've gotten a character on the current line
          current_line_is_blank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    client.stop();
  }
  

  if(now() > time33mins + (33 * SECS_PER_MIN) )
  {
    //Serial.println("33 minutes has elapsed");
   
    time33mins = now();    
     LogTemps();
  }
  if(now() > time60mins + (60 * SECS_PER_MIN) )
  {
    //Serial.println("one hour has elapsed");
    time60mins = now(); 
    LogTemps();   
  }

}


