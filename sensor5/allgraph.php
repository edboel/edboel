<?php
  include("phplot.php");
//  $timep = $_GET["timep"];
//  include("read_data.php");

$graph = new PHPlot(800,400);


$stack = array();
$lfile="temps.log";
$mfile="../sensor3/nwtemp.log";


$ldata= file($lfile);
$mdata= file($mfile);

$cend= count($ldata);
$cbegin= $cend-336;
$cdiff= count($mdata)-$cend;

$i=$cbegin;
$labelcount=0;

for($i; $i<$cend;$i++){
	$lline=$ldata[$i];
	$expl= explode("|",$lline);
	$mline=$mdata[$i+$cdiff];
	$mxpl= explode("|",$mline);

	$outpform = '%d %B %H:%M';
	$tstamp = mktime($expl[2],$expl[3],0,1,$expl[1],2010);
	$dde = strftime($outpform, $tstamp);
	if($labelcount < 30){
		array_push($stack, array('',$expl[4],$expl[5],$mxpl[4],$mxpl[5],trim($mxpl[7])));
		$labelcount++;
	}else{
		array_push($stack, array($dde,$expl[4],$expl[5],$mxpl[4],$mxpl[5],trim($mxpl[7])));
		$labelcount=0;
	}

}

//$graph->SetDataValues($example_data);


  $graph->SetXDataLabelAngle(90);
  $graph->SetDataValues($stack);
  $graph->SetYLabel("Temp C");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0),array(166,0,166),""));
  $graph->SetLegend(array("room","outside","outside","room","room"));
  $graph->SetLegendPixels(50,10);
  $graph->DrawGraph();

?>

