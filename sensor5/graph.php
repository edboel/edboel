<?php
  include("phplot.php");
//  $timep = $_GET["timep"];
//  include("read_data.php");

$graph = new PHPlot(800,400);


$stack = array();
$lfile="temps.log";
$mdata= file($lfile);
$cend= count($mdata);
$cbegin= $cend-336;

$i=$cbegin;
$labelcount=0;

for($i; $i<$cend;$i++){
	$mline=$mdata[$i];
	$expl= explode("|",$mline);

	$outpform = '%d %B %H:%M';
	$tstamp = mktime($expl[2],$expl[3],0,1,$expl[1],2010);
	$dde = strftime($outpform, $tstamp);
	if($labelcount < 30){
		array_push($stack, array('',$expl[4],$expl[5],$expl[6]));
		$labelcount++;
	}else{
		array_push($stack, array($dde,$expl[4],$expl[5],$expl[6]));
		$labelcount=0;
	}

}

//$graph->SetDataValues($example_data);


  $graph->SetXDataLabelAngle(90);
  $graph->SetDataValues($stack);
  $graph->SetYLabel("Temp C");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0)),"");
  $graph->SetLegend(array("room","outside","nc1","nc2"));
  $graph->SetLegendPixels(50,10);
  $graph->DrawGraph();

?>

