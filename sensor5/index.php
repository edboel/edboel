<html>
<br>
<h1>arduino sensor webserver</h1>
<br>
edsard boelen, 6 jan 2011<br>
<br>
<hr>
<i>bought some arduino uno kits with some shields, it is shockingly easy to set it up and build all kinds of things with it.</i>
<br>
<hr>
<br>
view current graph <a href="graph.php">here</a><br>
view graph combined with data from sensor3 <a href="allgraph.php">here </a><br>
<br>
<br>
what the code does is it records the temperature every 30 minutes and stores it on a SD card. 
But the board also has an ethernet interface. A small webserver displays the actual temperature. <br>
<br>

the code:<br>
<a href="logtemps.c">logtemps.c</a> <br>
used libraries:<br>
<ul>
<li><a href="">Time library</a>
<li><a href="">DallasTemplerature</a>
<li><a href="">OneWire library</a>
<li><a href="">sdFat library</a>
</ul>
<br>
you can get the files I used <a href="arduinolibs">here</a>
<br>
<br>
here is an image of the current stsem installed in my kitchen<br>
<img src="keuken.jpg"/><br>

</html>

