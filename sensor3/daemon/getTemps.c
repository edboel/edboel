/* Name: getTemps.c
 * Project: USB-DS1820 based on AVR USB driver
 * Author: Christian Starkjohann
 * Creation Date: 2005-01-16
 * Tabsize: 4
 * Copyright: (c) 2005 by OBJECTIVE DEVELOPMENT Software GmbH
 * License: Proprietary, free under certain conditions. See Documentation.
 * This Revision: $Id: WireDs1820.c Do Sep 27 14:14:57 CEST 2007
 * Modified by W. Stoljarski, 
 * Modified by A. Beck
 */

#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* this is libusb, see http://libusb.sourceforge.net/ */
#include <usb.h>   
#include "def.h"

#define USBDEV_SHARED_VENDOR    0x16C0  /* VOTI */
#define USBDEV_SHARED_PRODUCT   0x05DC  /* Obdev's free shared PID */

#define USBDEV_VENDOR_STR	"arnold.beck@web.de"
#define USBDEV_PRODUCT_STR	"USB-DS1820"

#define timeout			3

/* Use obdev's generic shared VID/PID pair and follow the rules outlined
 * in firmware/usbdrv/USBID-License.txt.
 */

#define PSCMD_ECHO  0
#define PSCMD_GET   1
#define PSCMD_ON    2
#define PSCMD_OFF   3
/* These are the vendor specific SETUP commands implemented by our USB device */
//float getTemp(int ,usb_dev_handle * );

static void usage(char *name)
{
    fprintf(stderr, "usage:\n");
    fprintf(stderr, "  %s gettemp [Pin]\n", name);
    fprintf(stderr, "  %s startmeas [Pin]\n", name);
    fprintf(stderr, "  %s meastemp [Pin]\n", name);
    fprintf(stderr, "  %s count [Pin]\n\n", name);
}


static int  usbGetStringAscii(usb_dev_handle *dev, int index, int langid, char *buf, int buflen)
{
char    buffer[256];
int     rval, i;

    if((rval = usb_control_msg(dev, USB_ENDPOINT_IN, USB_REQ_GET_DESCRIPTOR, (USB_DT_STRING << 8) + index, langid, buffer, sizeof(buffer), 1000)) < 0)
        return rval;
    if(buffer[1] != USB_DT_STRING)
        return 0;
    if((unsigned char)buffer[0] < rval)
        rval = (unsigned char)buffer[0];
    rval /= 2;
    /* lossy conversion to ISO Latin1 */
    for(i=1;i<rval;i++){
        if(i > buflen)  /* destination buffer overflow */
            break;
        buf[i-1] = buffer[2 * i];
        if(buffer[2 * i + 1] != 0)  /* outside of ISO Latin1 range */
            buf[i-1] = '?';
    }
    buf[i-1] = 0;
    return i-1;
}

static usb_dev_handle   *findDevice(void)
{
struct usb_bus      *bus;
struct usb_device   *dev;
usb_dev_handle      *handle = 0;

    usb_find_busses();
    usb_find_devices();
    for(bus=usb_busses; bus; bus=bus->next){
        for(dev=bus->devices; dev; dev=dev->next){
            if(dev->descriptor.idVendor == USBDEV_SHARED_VENDOR && dev->descriptor.idProduct == USBDEV_SHARED_PRODUCT){
                char    string[256];
                int     len;
                handle = usb_open(dev); /* we need to open the device in order to query strings */
                if(!handle){
                    fprintf(stderr, "Warning: cannot open USB device: %s\n", usb_strerror());
                    continue;
                }
                /* now find out whether the device actually is obdev's Remote Sensor: */
                len = usbGetStringAscii(handle, dev->descriptor.iManufacturer, 0x0409, string, sizeof(string));
                if(len < 0){
                    fprintf(stderr, "warning: cannot query manufacturer for device: %s\n", usb_strerror());
                    goto skipDevice;
                }
                /* fprintf(stderr, "seen device from vendor ->%s<-\n", string); */
                if(strcmp(string, USBDEV_VENDOR_STR) != 0)
                    goto skipDevice;
                len = usbGetStringAscii(handle, dev->descriptor.iProduct, 0x0409, string, sizeof(string));
                if(len < 0){
                    fprintf(stderr, "warning: cannot query product for device: %s\n", usb_strerror());
                    goto skipDevice;
                }
                /* fprintf(stderr, "seen product ->%s<-\n", string); */
                if(strcmp(string, USBDEV_PRODUCT_STR) == 0)
                    break;
skipDevice:
                usb_close(handle);
                handle = NULL;
            }
        }
        if(handle)
            break;
    }
    if(!handle)
        fprintf(stderr, "Could not find USB device www.obdev.at/USB-DS1820\n");
    return handle;
}




int main(int argc, char **argv)
{
usb_dev_handle      *handle = NULL;
int                 nBytes,tmp0=0,tmp1=0,tmp2=0;
float		t0,t1,t2;

	time_t now;
   struct tm *tm;

    setuid(0);
    usb_init();

  //  if((handle = findDevice()) == NULL){
  //      fprintf(stderr, "Could not find USB device \"USB-DS1820\" with vid=0x%x pid=0x%x\n", USBDEV_SHARED_VENDOR, USBDEV_SHARED_PRODUCT);
  //      exit(1);
  //  }

    tmp0 = getTemp(0,handle);
	t0 = tmp0 / 2.0;
    tmp1 = getTemp(1,handle);
	t1 = tmp1 / 2.0;
    tmp2 = getTemp(5,handle);
	t2 = tmp2 / 2.0;
 //   usb_close(handle);

	now = time(NULL);
      tm = localtime(&now);
//	printf (" %.2f , %.2f , %.2f \n ",t0,t1,t2);
	printf("%4d|%3d|%2d|%2d|%.2f|%.2f|%.2f|%.2f \n",
		tm->tm_year+1900,tm->tm_yday+1,tm->tm_hour,tm->tm_min,t0,t1,0.0,t2);

    return 0;
}


int getTemp(int pin, usb_dev_handle *handl2)
{
	char buffer[503];
	int nBytes,ltemp=0;
	float ntemp =0;
usb_dev_handle      *handle = NULL;

    if((handle = findDevice()) == NULL){
        fprintf(stderr, "Could not find USB device \"USB-DS1820\" with vid=0x%x pid=0x%x\n", USBDEV_SHARED_VENDOR, USBDEV_SHARED_PRODUCT);
        exit(1);
    }

	nBytes = usb_control_msg(handle, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, UMTM_STARTMEAS /* command */, pin, 0, (char *)buffer, sizeof(buffer), 5000);
        if(nBytes < 0){
            fprintf(stderr, "USB error: %s\n", usb_strerror());
            exit(1);
	}
	/* warten */
	sleep(timeout);
   
	nBytes = usb_control_msg(handle, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, UMTM_GETT /* command */, pin, 0, (char *)(buffer), sizeof(buffer), 5000);
        if(nBytes < 0){
            fprintf(stderr, "USB error: %s\n", usb_strerror());
            exit(1);
	}

        if(nBytes>0)
	{
	  ltemp = buffer[1];
	  //float  ntemp = ltemp/2;
	  //printf(" temp %d = %d \n ",pin,ltemp);
  	  //printf(" pin = %d , ltemp = %d fl = %f \n",pin,ltemp,ntemp);	
	  //printf(" buf 0 = %d , buf 2 = %d , buf 11 = %d \n ",buffer[0],buffer[2],buffer[11]);
	}

usb_close(handle);

   return ltemp;
}


