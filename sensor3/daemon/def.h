#ifndef _DEF_H_
#define _DEF_H_

#define SHORT_CIRCUIT		-1		/* Short Circuit ! */
#define NO_SENSOR		-2		/* No Sensor found */
#define BUS_ERROR		-3		/* Bus Error */

#define UMTM_GETT		0
#define UMTM_STARTMEAS		1
#define UMTM_DEV_ENUM		2
#define UMTM_DEV_CNT		3

#define maxCountOfSensors       10

/* 10 byte */
typedef struct tempID{
    short       	Tt;                     // temperatur
    unsigned char       id[8];                  // id of sensor
}tempID_t;

/* 10 * maxCountOfSensors + 1 = 101 bytes */
typedef struct tempWithID{
    char        	count;                  // count of sensors
    tempID_t    	tmp[maxCountOfSensors]; // temperatur & id's
}tempWithID_t;

#endif /* _DEF_H_ */

/*
USB interrupt must be not disabled for more than 20 (25?) cycles
*/

/*
usbPool function must be called at regular intervals from the main loop.
Maximum delay between calls is somewhat less than 50ms.
*/

