<?php
  include("phplot.php");
  $timep = $_GET["timep"];

  if($timep == "24h")
	 $graph = new PHPlot(500,400);
  else
  	$graph = new PHPlot(800,400);
  $mode = "silent";
 
  include("read_data.php");

  $graph->SetXDataLabelAngle(90);
  $graph->SetDataValues($stack);
  $graph->SetYLabel("Temp C");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0)),"");
  $graph->SetLegend(array("room","outside","cpu","room2"));
  $graph->SetLegendPixels(50,10);
  $graph->DrawGraph();

?>



