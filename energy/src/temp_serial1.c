#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>


#define BACKLIGHT_PIN     13

LiquidCrystal_I2C lcd(0x20);  // Set the LCD I2C address
OneWire W1(8);
OneWire W2(9);
OneWire W3(10);
OneWire W4(11);
OneWire W5(12);
DallasTemperature sen1(&W1);
DallasTemperature sen2(&W2);
DallasTemperature sen3(&W3);
DallasTemperature sen4(&W4);
DallasTemperature sen5(&W5);

float tmp1,tmp2,tmp3,tmp4,tmp5;
//LiquidCrystal_I2C lcd(0x38, BACKLIGHT_PIN, POSITIVE);  // Set the LCD I2C address

void setup()
{
  Serial.begin(9600);
  
  // Switch on the backlight
  pinMode ( BACKLIGHT_PIN, OUTPUT );
  digitalWrite ( BACKLIGHT_PIN, HIGH );
  
  lcd.begin(16,2);               // initialize the lcd 
  sen1.begin();
  sen2.begin();
  sen3.begin();
  sen4.begin();
  sen5.begin();

  lcd.home ();                   // go home
  lcd.print(" getting temps ");  
  lcd.setCursor ( 0, 1 );        // go to the next line
  lcd.print (" init serial   ");
  delay ( 1000 );
  digitalWrite ( BACKLIGHT_PIN, LOW);
  
}

void echoSerial(){
 byte index=0;
 char outa[20];
 char outb[20];
 outb[0]='\0';
 boolean foundData=false;
 while (Serial.available() > 0){
   foundData=true;
   if(index < 19){
     outa[index]=Serial.read();
     index++;
     outa[index]= '\0'; 
   }else{ 
     if(index < 39){
       outb[index-20]=Serial.read();
       index++;
       outb[index-20]= '\0';
     }
   }
 }
 if(foundData){
   lcd.clear();
   lcd.home();
   lcd.print(outa);
   lcd.setCursor ( 0, 1 );
   lcd.print(outb);
 }
}

void loop()
{
  sen1.requestTemperatures();
  sen2.requestTemperatures();
  sen3.requestTemperatures();
  sen4.requestTemperatures();
  sen5.requestTemperatures();
  
  echoSerial();
  delay(1000);
  echoSerial();
  delay(1000);
    
  digitalWrite ( BACKLIGHT_PIN, HIGH );
  
  if(sen1.getTempCByIndex(0) < 80){
    tmp1=sen1.getTempCByIndex(0);
  }
  if(sen2.getTempCByIndex(0) < 80){
    tmp2=sen2.getTempCByIndex(0);
  }
  if(sen3.getTempCByIndex(0) < 80){
    tmp3=sen3.getTempCByIndex(0);
  }
  if(sen4.getTempCByIndex(0) < 80){
    tmp4=sen4.getTempCByIndex(0);
  }
  if(sen5.getTempCByIndex(0) < 80){
    tmp5=sen5.getTempCByIndex(0);
  }
  
   lcd.home ();                   // go home
  lcd.print("T1=");  
  lcd.print(tmp1);
  lcd.print(" T2=");
  lcd.print(tmp2);
  lcd.setCursor ( 0, 1 );        // go to the next line
  lcd.print("T3=");
  lcd.print(tmp3);
  lcd.print("  T4=");
  lcd.print(tmp4);
  
  Serial.print(tmp1);
  Serial.print("|");
  Serial.print(tmp2);
  Serial.print("|");
  Serial.print(tmp3);
  Serial.print("|");
  Serial.print(tmp4);
  Serial.print("|");
  Serial.print(tmp5);
  Serial.println(";");
 
  for(int i=0;i<5;i++){
    echoSerial();
    delay(1000);
  }
 
 digitalWrite ( BACKLIGHT_PIN, LOW );
  
}
