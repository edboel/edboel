
#include <OneWire.h>
#include <DallasTemperature.h>

#define BACKLIGHT_PIN     13
#define STATUS_PIN        12
#define DATA_ENABLE_PIN   2
#define ESENS_PIN         3
#define CV_PIN            4
#define TEMP1_PIN         5
#define WATER_PIN         6
#define TEMP2_PIN         7

OneWire W1(TEMP1_PIN);
OneWire W2(TEMP2_PIN);
DallasTemperature sen1(&W1);
DallasTemperature sen2(&W2);
float tmp1,tmp2;

long pulseCount;
//unsigned long pulseTime,lastTime;
//power and energy
//float power, elapsedkWh;
//Number of pulses per wh - found or set on the meter.
//static int kppwh = 1000; //1000 pulses/kwh = 1 pulse per wh

void setup()
{
  sen1.begin();
  sen2.begin();
  tmp1 = tmp2 = 0;
  pulseCount = 1;
  //power = elapsedkWh = 1.0;
  pinMode ( BACKLIGHT_PIN, OUTPUT );
  pinMode ( STATUS_PIN, OUTPUT );
  pinMode ( ESENS_PIN, INPUT );
  digitalWrite ( BACKLIGHT_PIN, LOW ); 
  pinMode ( DATA_ENABLE_PIN, OUTPUT );
  digitalWrite ( DATA_ENABLE_PIN, LOW ); 
  Serial.begin(9600);
  // KWH interrupt attached to IRQ 1  = pin3
  attachInterrupt(1, onPulse, FALLING);
  
  sen1.requestTemperatures();
  sen2.requestTemperatures();
  delay(1000);
  
}

// The interrupt routine
void onPulse()
{

  //used to measure time between pulses.
  //lastTime = pulseTime;
  //pulseTime = micros();
  //if(pulseTime - lastTime > 1000){
    //pulseCounter
    pulseCount++;
    //Calculate power
    //power = (3600000000.0 / (pulseTime - lastTime))/kppwh;
    //Find kwh elapsed
    //elapsedkWh = (1.0*pulseCount/kppwh); //multiply by 1000 to pulses per wh to kwh convert wh to kwh
  //}
}

String readLine(){
  String result;
  for(int i=0;i<10;i++){
    if(Serial.available()) {
      digitalWrite ( BACKLIGHT_PIN, HIGH ); 
      int dcount=0;
      while (Serial.available() && dcount < 200) {
        // get the new byte:
        char inChar = (char)Serial.read(); 
        // add it to the inputString:
        result += inChar;
        dcount++;
      }
    }  
    delay(20);
  }
  digitalWrite ( BACKLIGHT_PIN, LOW );
  return result;
}

void sendLine(String node, String message){
  digitalWrite ( STATUS_PIN, HIGH );
  delay(50);
  digitalWrite ( DATA_ENABLE_PIN, HIGH );
  delay(50);
  Serial.println(node+"_"+message);
  delay(100);
  digitalWrite ( DATA_ENABLE_PIN, LOW );
  delay(50);
  digitalWrite ( STATUS_PIN, LOW ); 
}


void loop()
{
  String message;
  char response[10];
  int tcount=0;
  
  if(sen1.getTempCByIndex(0) < 80){
    tmp1=sen1.getTempCByIndex(0);
  }
  if(sen2.getTempCByIndex(0) < 80){
    tmp2=sen2.getTempCByIndex(0);
  }
  
  sen1.requestTemperatures();
  sen2.requestTemperatures();
  
  while(tcount < 500){
    // wait for incomming message
    message = readLine();
    if(message.startsWith("h_status")){
      sendLine("m","11");
    }
    if(message.startsWith("h_temp1")){
      dtostrf(tmp1,5,2,response);
      sendLine("m",response );
    } 
    if(message.startsWith("h_temp2")){
      dtostrf(tmp2,5,2,response);
      sendLine("m",response);
    } 
    if(message.startsWith("h_pulse")){
      sendLine("m",String(int(pulseCount)));
    }
    /*if(message.startsWith("h_power")){
      dtostrf(power,6,2,response);
      sendLine("m",response);
    }
    if(message.startsWith("h_kwh")){
      dtostrf(elapsedkWh,6,2,response);
      sendLine("m",response);
    }*/
    if(message.startsWith("h_resetpulse")){
      pulseCount=0;
      //elapsedkWh=1.0;
      sendLine("m","ok");
    }
    if(message.startsWith("h_water")){
      sendLine("m",String(digitalRead(WATER_PIN)));
    }
    if(message.startsWith("h_mcvstat")){
      sendLine("m",String(digitalRead(CV_PIN)));
    }
    if(message.startsWith("h_mcvon")){
      digitalWrite ( CV_PIN, HIGH ); 
      sendLine("m",String(digitalRead(CV_PIN)));
    }
    if(message.startsWith("h_mcvoff")){
      digitalWrite ( CV_PIN, LOW ); 
      sendLine("m",String(digitalRead(CV_PIN)));
    }
    if(message.startsWith("h_lcvstat")){
      sendLine("m",String(digitalRead(CV_PIN)));
    }
    if(message.startsWith("h_lcvon")){
      digitalWrite ( CV_PIN, HIGH ); 
      sendLine("m",String(digitalRead(CV_PIN)));
    }
    if(message.startsWith("h_lcvoff")){
      digitalWrite ( CV_PIN, LOW ); 
      sendLine("m",String(digitalRead(CV_PIN)));
    }
    tcount++;
    delay(50);
  }
}
