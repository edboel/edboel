
#define BACKLIGHT_PIN     13
#define STAT_PIN     12
#define DATA_ENABLE_PIN   2

void setup()
{
  pinMode ( BACKLIGHT_PIN, OUTPUT );
  pinMode ( STAT_PIN, OUTPUT );
  digitalWrite ( BACKLIGHT_PIN, HIGH ); 
  pinMode ( DATA_ENABLE_PIN, OUTPUT );
  digitalWrite ( DATA_ENABLE_PIN, HIGH ); 
  digitalWrite ( STAT_PIN, HIGH );
  Serial.begin(9600);
  
}

void loop()
{
 delay(400);
 digitalWrite ( DATA_ENABLE_PIN, HIGH );
 delay(100);
 digitalWrite ( BACKLIGHT_PIN, HIGH );
 Serial.println("hi");
 delay (500);
 digitalWrite ( BACKLIGHT_PIN, LOW );
  Serial.println("there");
 delay(100);
 digitalWrite ( DATA_ENABLE_PIN, LOW );
 
 for(int i=0;i<100;i++){
   String inputString = ""; 
     delay(50);
     if(Serial.available()) {
       while (Serial.available()){
         char inChar = (char)Serial.read(); 
         inputString += inChar;
         if(inChar =='\n'){  
           if(inputString.startsWith("reply")){
             digitalWrite ( STAT_PIN, HIGH );
           }
           inputString='';
         }
       }
     }
 }
 digitalWrite ( STAT_PIN, LOW );
 //digitalWrite ( DATA_ENABLE_PIN, LOW   );
}
