/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SoftwareSerial.h>

#define DATA_ENABLE_PIN   4
#define BACKLIGHT_PIN     13
#define STATUS_PIN        12

LiquidCrystal_I2C lcd(0x20);  // Set the LCD I2C address
OneWire W1(8);
OneWire W2(9);
OneWire W3(10);
DallasTemperature sen1(&W1);
DallasTemperature sen2(&W2);
DallasTemperature sen3(&W3);

float tmp1,tmp2,tmp3;
//LiquidCrystal_I2C lcd(0x38, BACKLIGHT_PIN, POSITIVE);  // Set the LCD I2C address
SoftwareSerial mySerial(2, 3); // RX, TX

void setup()
{
  Serial.begin(9600);
  mySerial.begin(9600);

  // Switch on the backlight
  pinMode ( BACKLIGHT_PIN, OUTPUT );
  pinMode ( DATA_ENABLE_PIN, OUTPUT );
  pinMode ( STATUS_PIN, OUTPUT );
  digitalWrite ( DATA_ENABLE_PIN, LOW );
  digitalWrite ( STATUS_PIN, LOW );
  digitalWrite ( BACKLIGHT_PIN, HIGH );

  lcd.begin(16,2);               // initialize the lcd 
  sen1.begin();
  sen2.begin();
  sen3.begin();

  lcd.home ();                   // go home
  lcd.print(" getting temps ");  
  lcd.setCursor ( 0, 1 );        // go to the next line
  lcd.print (" init serial   ");
  delay ( 1000 );
  digitalWrite ( BACKLIGHT_PIN, LOW);
}

String readLine(){
  String result="";
  delay(50);
  digitalWrite ( STATUS_PIN, HIGH );
  for(int i=0;i<25;i++){
    if(mySerial.available()) {
      while (mySerial.available()) {
        // get the new byte:
        char inChar = (char)mySerial.read(); 
        // add it to the inputString:
        //Serial.print('-');
        //Serial.print(inChar);
        //Serial.print('_');
        //Serial.print(inChar,DEC);
        if(inChar > 45 && inChar < 123){
          result += inChar;
        }
        if (result.length() > 25 || inChar == 13) {
          //stringComplete = true;
          break;
        } 
      }
    }
    delay(100); 
  }
  digitalWrite ( STATUS_PIN, LOW );
  if(result.length() < 3 || !result.startsWith("m_")){
    delay(250);
    result = "__err1"+result+".";
  }

  return result.substring(2);
}

void sendLine(String node, String message){
  digitalWrite ( DATA_ENABLE_PIN, HIGH );
  delay(50);
  mySerial.println(node+"_"+message);
  delay(50);
  digitalWrite ( DATA_ENABLE_PIN, LOW );
  delay(50);
}

void checkHallNode(){
  String response;
  lcd.clear();                   // go home
  Serial.println("hall_: 0");
  sendLine("h","status");
  response = readLine();
  if(!response.startsWith("11")){
    lcd.print("error hall");  
    Serial.print("err_h: ");
    Serial.println(response);
  }
  else{
    delay(50);
    sendLine("h","temp1");
    response = readLine();
    lcd.print("T1="+response);
    Serial.println("htmp1: "+response);
    sendLine("h","temp2");
    response = readLine();
    Serial.println("htmp2: "+response);
    sendLine("h","pulse");
    response = readLine();
    Serial.println("pulse: "+response);
    //sendLine("h","kwh");
    //delay(50);
    //response = readLine();
    //lcd.print("kwh="+response); 
    //Serial.println("kwh__: "+response);
    sendLine("h","water");
    response = readLine();
    Serial.println("water: "+response);
    sendLine("h","mcvstat");
    response = readLine();
    Serial.println("cv___: "+response);
  }
  digitalWrite ( DATA_ENABLE_PIN, LOW );

}

void checkMasterNode(){
  sen1.requestTemperatures();
  sen2.requestTemperatures();
  sen3.requestTemperatures();
  delay(1500);

  digitalWrite ( BACKLIGHT_PIN, HIGH );

  if(sen1.getTempCByIndex(0) < 80){
    tmp1=sen1.getTempCByIndex(0);
  }
  if(sen2.getTempCByIndex(0) < 80){
    tmp2=sen2.getTempCByIndex(0);
  }
  if(sen3.getTempCByIndex(0) < 80){
    tmp3=sen3.getTempCByIndex(0);
  }

  lcd.home ();                   // go home
  lcd.print("T1=");  
  lcd.print(tmp1);
  lcd.print(" T2=");
  lcd.print(tmp2);
  lcd.setCursor ( 0, 1 );        // go to the next line
  lcd.print("T3=");
  lcd.print(tmp3);

  Serial.println("mastr: 0");
  Serial.print("mtmp1: ");
  Serial.println(tmp1);
  Serial.print("mtmp2: ");
  Serial.println(tmp2);
  Serial.print("mtmp3: ");
  Serial.println(tmp3);
  digitalWrite ( BACKLIGHT_PIN, LOW );

}

void loop()
{
  checkMasterNode();
  checkHallNode();
  //checkLivingNode();  

  delay(1000);
}

