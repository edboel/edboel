#include <SoftwareSerial.h>
#define BACKLIGHT_PIN     13
#define STAT_PIN          12
#define DATA_ENABLE_PIN   4

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
SoftwareSerial mySerial(2, 3); // RX, TX

void setup() {
  // initialize serial:
  Serial.begin(9600);
  delay(500);
  Serial.println("init");
  mySerial.begin(9600);
  
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  pinMode ( BACKLIGHT_PIN, OUTPUT );
  pinMode ( STAT_PIN, OUTPUT );
  pinMode ( DATA_ENABLE_PIN, OUTPUT );
  digitalWrite ( DATA_ENABLE_PIN, LOW );
  digitalWrite ( STAT_PIN, HIGH );
    delay(500);
  digitalWrite ( STAT_PIN, LOW );
    
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    if(inputString.startsWith("ther")){
        digitalWrite ( BACKLIGHT_PIN, HIGH );
        delay(100);
        
        // send reply :)
        delay(200);
        digitalWrite ( DATA_ENABLE_PIN, HIGH );
        delay(400);
        mySerial.println("reply");
        delay(200);
        mySerial.println("reply");
        delay(200);
        mySerial.println("reply");
        delay(200);
        mySerial.println("reply");
        delay(200);
        mySerial.println("reply");
        delay(200);
        digitalWrite ( DATA_ENABLE_PIN, LOW );
        digitalWrite ( BACKLIGHT_PIN, LOW );
        
        
    }
    Serial.println(inputString);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  delay(50);
  
  if(mySerial.available()) {
    int dcount=0;
    while (mySerial.available() && dcount < 200) {
      // get the new byte:
      digitalWrite ( STAT_PIN, HIGH );
      char inChar = (char)mySerial.read(); 
      // add it to the inputString:
      inputString += inChar;
      if (inChar == '\n') {
        stringComplete = true;
      } 
      dcount++;
      digitalWrite ( STAT_PIN, LOW );
    }
  }
}


