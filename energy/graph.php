<?php
  include("phplot.php");
//  $timep = $_GET["timep"];
//  include("read_data.php");

$graph = new PHPlot(800,400);


$stack = array();
$lfile="../sensor6/pulse.log";
$mdata= file($lfile);
$cend= count($mdata);
$cbegin= $cend-336;

$i=$cbegin;
$labelcount=$prev=0;

for($i; $i<$cend;$i++){
	$mline=$mdata[$i];
	$expl= explode("_",$mline);
	
	$delta=$expl[1]-$prev;
	if( $delta > 3600){
		$delta=0;
	}
 	if( $delta < -1){
		$delta = $delta * -1;
	}	
	//echo "$prev .. ".$expl[1]." .. $delta <br>";
	$prev=$expl[1];
	if($labelcount==30){
		array_push($stack, array($expl[0],$delta));
		$labelcount=0;
	}else{
		array_push($stack, array('',$delta));
		$labelcount++;
	}

}

//$graph->SetDataValues($example_data);


  $graph->SetXDataLabelAngle(90);
  $graph->SetDataValues($stack);
  $graph->SetYLabel("avg. Power (W)");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue"));
  //$graph->SetLegend(array("room","outside"));
  $graph->SetLegendPixels(50,10);
  $graph->DrawGraph();

?>

