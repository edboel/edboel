<html>
<br>
<h1>home build e-meter and e-thermostat</h1>
<br>
edsard boelen, 24 nov 2012<br>
<br>
<hr>
<i>
As the days are getting colder, it is time to fire up the heating. At this moment we use an easy thermostat in the most basic way. As it should be. But it would be nice to have it logged and to be able to control it remotely. What also would be nice is to have some more info on the display besides the current room temperature.
<br><br>
As a second idea, 
The power companies are now selling so called e-meters for 200,- a piece. 
They monitor the power usage constantly and present it then in an overview.
After a little search I found out that measuring technique is nothing more than a light sensor counting the KWh meter.
I can easily include that in my project.
</i>
<br>
<hr>
<br>
<?php
	$delta = 0; 
	$filename = "../sensor6/pulse.log";
 	$data = file($filename);
	$lastline = $data[count($data)-1];
	$slastline = $data[count($data)-2];
 	$expl = explode('_',$lastline);
	$sexpl = explode('_',$slastline);
	$delta = $expl[1] - $sexpl[1];
	if($delta < -1){
		$delta = $delta * -1;
	}	
?>
Current power usage:  <b> <?php echo $delta ?> W </b>&nbsp <a href="graph.php">view graph</a><br>
<br>
<h3> the idea </h3>
At first I thougt a raspberry pi would be nice to have as a central unit. 
But the wiring and the power consumption made me consider 3 arduino's in the end. 
One in the room, one in the hall and one in the computer room.
They will communicate with eachother with RS485 interface.
<br>
<h3>RS485</h3>
RS485 uses a differential balanced line to transfer data, it has a very low distorsion.
where rs232 has a maximum cable length of 12 meter, rs485 can handle 1200 meters. 
Another advantage is that it can be used as a bus network.<br>
The <a href="">75lbc176</a> is an affordable rs485 driver IC, It can just be connected to the uart outputs of the arduino. 
I will connect it as shown below.
<br>
<img src="rs485_setup.png" /><br> 
The arduino connecting to the computer will have a second uart, one for the rs485 network, and one to connect to the computer.<br>
The softSerial library allows the arduino to have multiple serial ports.<br>

<h3>Communication test </h3>
The first thing is to make the arduinos talk to eachother and one to pass some info to the computer.<br>
I wrote 2 test applications, one for a master and one for a slave. 
The master will send a message, the slave will respond. 
The response is then transfered to the copmuter via the USB serial.<br>
the appliactions can be downloaded as: 
<a href="rs485test_master.c">rs485_test_master</a> and
<a href="rs485test_slave.c">rs485_test_slave</a> <br>
The circuit layout is as follows:<br>
<img src="rs485cir.png" /><br>
<img src="rs485test_im.png" /><br>
<br>
<h3>Network</h3>
the 75lbc176 can be configured as a bus network. But has no collision detection as with the ethernet protocol. 
This is solved as follows: 
The master will call each slave every minute and gives each slave some time to respond.  
This way no collisions can occur.<br>
<center>Network messages:<br>
<table border="1">
<tr><td> &nbsp; name &nbsp; </td><td> &nbsp; to node &nbsp </td><td> &nbsp; response value</td></tr>
<tr><td>status</td><td>hall</td><td>1</td></tr>
<tr><td>temp1</td><td>hall</td><td>11.11</td></tr>
<tr><td>temp2</td><td>hall</td><td>11.11</td></tr>
<tr><td>esens</td><td>hall</td><td>1111</td></tr>
<tr><td>water</td><td>hall</td><td>1</td></tr>
<tr><td>cv</td><td>hall</td><td>1</td></tr>

<tr><td>status</td><td>living</td><td>1</td></tr>
<tr><td>temp1</td><td>living</td><td>11.11</td></tr>
<tr><td>temp2</td><td>living</td><td>11.11</td></tr>
<tr><td>cvset</td><td>living</td><td>11.11</td></tr>


</table></center>
<h3>The master node</h3>
Will have 3 temp sensors, 
usb serial interface to pass all data to the server.
Small lcd screen.
<br>
<img src="rs485mastercir.png" /><br>
<br>
<img src="rs485master.png" /><br>
<br>
<h3>The hall node</h3>
will have 2 temp sensors,
energy counter,
water level meter,
actuator for central heating.
<br>
<img src="rs485hallcir.png" /><br>
<br>
<img src="rs485hall.png" /><br>
<br>
<img src="hallnodebox.png" /><br>
<br>
The code can be found 
<a href="hall_node.c">here</a> and <a href="master_node.c">here</a> <br>
<h3>The living room node</h3>
Will have temp sensor,
256x256 dot display,
menu buttons.
<br>
</html>

