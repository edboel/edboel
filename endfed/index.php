<html>
<br>
<h1> 10-20-40m end fed antenna</h1>
<br>
edsard boelen, 14 feb 2014<br>
<br>
<hr><i>
Last december I decided that I should have besides the yaesu 8900r tranceiver also a HF tranceiver for the 1..50MHz signals.
So I got a used yaesu 450D for a good price. But besides a good trnaceiver, also a good antenna is needed. I wanted to start with a mono band dipole, but it never took off.
I was told an end fed antenna would be a much better option. 
</i><br>
<hr>
<br>
<br>
<h2> the idea</h2>
I made a nice plan on where the antenna will be:<br>

<img src="garden.jpg" /><br>
On the right side is my garage (full with everything besides a car.)<br>
On the left side, a small tripod wil stand on the dormer.<br>
<br>
<h2> The kit </h2>
Here are the parts needed to make the entire antenna.<br>
I bought it as a complete kit at <a href="http://www.communicationworld.nl">www.communicationworld.nl</a>. <br>
They also gave me some excellent advice on how to build it.<br>  

<img src="IMG_20140218.jpg" /><br>

<h2> The coils </h2>
2 coils will be used<br>
1 34uH coil at 10m to cut off the higher frequencies so this becomes a multiband antenna.<br>
1 1-50 transformer at the base to make it look like an open end dipole.<br>
see the schematic below.<br>
<img src="1op50-trafo.jpg" /><br>
This is the most important part of the antenna. I am told that if it is not wound tight enough, the performance will decrease rapidly.<br>
<br>
<img src="IMG_20140219_202646.jpg" /><br>
<br>
here is the 34 uH coil which will float in the air.<br>
<img src="IMG_20140218_223758.jpg" /><br>
and a detail shot<br>
<img src="IMG_20140219_185106.jpg" /><br>
<br>
<h2>The casing</h2>
<br>
I think this image says enough. Note the small capacitor on the right.<br>
<img src="IMG_6859.JPG" /><br>
<br>
here you see the entire antenna. The coil is shrink wrapped in black.<br>
<img src="IMG_6858.JPG" /><br>
<br>
<h2>First tests</h2>
<img src="IMG_6862.JPG" /><br>
<br>
the first tests went well, could transmit with an swr of 1:1.1 on the 10, 20 and 40 meter band. 
Even got some reception on the 80 meter band. (could not send though)<br>

<br>
<h2> Mounting </h2>
<br>
Here you can see the lower part. The first 0.5meters is nylon wire. Then you have the isolator and further along is the coil.<br> 
<img src="mounting.jpg" /><br>
<img src="IMG_6856.JPG" /><br>
<br>
<br>
<h2> My First QSL </h2>
This is of course the reason I am doing this.<br>
It has yet to happen. But I will try to record it.
<br>
<br>
<br>
<Br>
<Br>
<br>
</html>

