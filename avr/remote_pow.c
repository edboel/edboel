/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// program sends at 2400 baud
#define F_CPU 8000000UL
#include <stdio.h>
#include <util/delay.h>
#include <avr/io.h>


void USART_vInit(void)
{
  /* INITIALIZE RS232*/
  UBRR0L = 207; // (uint8_t)(UART_BAUD_CALC(UART_BAUD_RATE,F_OSC)>>8);
  UBRR0H = (207>>8); //(uint8_t)UART_BAUD_CALC(UART_BAUD_RATE,F_OSC);
  // enable send
  UCSR0B = (1<<TXEN0);
}

void USART_vSendByte(uint8_t u8Data)
{
// Wait if a byte is being transmitted
 while((UCSR0A&(1<<UDRE0)) == 0);
// Transmit data
 UDR0 = u8Data;
}

void decodeSignal()
{
    // approx 4.5 ms to 1st bit
    unsigned int lowcount;
    unsigned int bitcount;
    unsigned char sdata[99];
    lowcount = 0;
    bitcount = 0;
    while(lowcount < 100)
    {
    	if (!(PINC & (1 << 5)))
    	{
		if(lowcount < 8)
		{
			sdata[bitcount] = '0';
		}
		else if(lowcount < 20)
		{
			sdata[bitcount] = '1';
		}
		bitcount++;
		lowcount=0;
		_delay_us(700);
    	}
	else
	{
	   lowcount++;
	   _delay_us(140);
	}
	
    }

    // try to decode to values
    USART_vSendByte('\n');
    char val = 0;
    int i=0;
    for(i=0; i<16; i++)
    {
   	val = val*2;
   	val = val + sdata[i];
    }
    USART_vSendByte(val);

    val = 0;
    for(i=16; i<32; i++)
    {
   	val = val*2;
   	val = val + sdata[i];
    }
    USART_vSendByte(val);

    USART_vSendByte('\n');
    if(val=='%')
    {
	USART_vSendByte('P');
	USART_vSendByte('O');
	USART_vSendByte('W');
	USART_vSendByte('\n');
	
	// PORTB |= _BV(PB0); 
	PORTB = 0x01; 
        _delay_ms(1000);
        /* set output to 5V, LED off  */
        //PORTB &= ~_BV(PB0);
	PORTB = 0x00;
    }

}

int main(void)
{

   /* enable PB0 as output */
   // DDRB  &= ~ _BV(PB0);
   // data direction register set all to output
   // DDRB = 0xff; 
   DDRB = 0x01;
   // set all off
   PORTB = 0x00;

   // enable pc5 as input ?
   PORTC |= _BV(PC5);
   USART_vInit(); 
   unsigned int startcount;	
   
      while (1) 
      {
	startcount =0;
	// wait for input
	while (!(PINC & (1 << 5)))
	{
	  startcount++;
	  _delay_us(140);
	}
	// the start sinal is the only one longer than 6 ms 'high'
	if (startcount > 45) 
	{
		USART_vSendByte('S');
		decodeSignal();
	}
	// send received signal
	
      }
	return(0);
}
