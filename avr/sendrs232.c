
#define F_CPU 16000000UL
#include <util/delay.h>
#include <avr/io.h>


void USART_vInit(void)
{
	  /* INITIALIZE */
UBRR0L = 207; // (uint8_t)(UART_BAUD_CALC(UART_BAUD_RATE,F_OSC)>>8);
UBRR0H = (207>>8); //(uint8_t)UART_BAUD_CALC(UART_BAUD_RATE,F_OSC);

// enable single speed asynchro
// UCSR0A = (0<<U2X0);

// umsel = mode select 
/*
UCSR0C =	(0 << UMSEL01)|
		(0 << UMSEL00)|
		(0 << UPM01) |
		(0 << UPM00) |
		(0 << USBS0) |
		(0 << UCSZ02) |
		(1 << UCSZ01)|
		(1 << UCSZ00);	
*/
// enable send
UCSR0B = (1<<TXEN0);
          
}

void USART_vSendByte(uint8_t u8Data)
{
// Wait if a byte is being transmitted
 while((UCSR0A&(1<<UDRE0)) == 0);
// Transmit data
 UDR0 = u8Data;
}

int main(void)
{

   /* enable PB0 as output */
   DDRB|= _BV(PB0);
   USART_vInit(); 	
   
       while (1) {
        PORTB&= ~_BV(PB0);
        _delay_ms(500);
	USART_vSendByte('H');
	USART_vSendByte('E');
	USART_vSendByte('L');
	USART_vSendByte('L');
	USART_vSendByte('O');
			
	PORTB|= _BV(PB0);
        _delay_ms(500);

          }
	return(0);
}

