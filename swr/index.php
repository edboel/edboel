<html>
<head><title> a digital swr meter </title></head>
<br>
<center>
<h2> a SWR meter (3..30MHz, 100W..2KW) for arduino and raspberry pi<h2>
</center><br>
Edsard Boelen, 20-feb-2015
<hr><i>
<a href="">Fox Delta</a> sells digital SWR kits in two parts, an analogue and a display part. 
The display part uses a pic microcontroller with a 16x2 character display. 
That can be replaced by a much fancier arduino or raspberry pi.
At the same time, with the <a href="">AD8307</a>, accurate power meters can be made. 
Nice to include that too.
<hr></i>
<br>
<h2> build the hardware  </h2>
<br>

<br>

<img src="hf-dual-schematic.jpg" />
<img src="kitcontents.jpg" />
<img src="build2.jpg" />
<img src="build3.jpg" />
<br>
<br>
<h2> calibrate it </h2>
<br>
To calibrate it, known values have to be used.
i.e. a known amount of power should be sent to a known load of which the SWR is 0.
<br>
A dummy load will do that.
To set the proper reverse power measurement, just switch the connectors.
<br>
The better the dummy load and the closer it is to the swr meter, the better the  accuracy. <br>
For me accuracy is not the biggest issue so I made a dummy load out of 20 5W resistors of 1k.
<br>
<img src="dummy1.jpg"/><br>
<br>
I have used an arduino with the lcd button shield in this test.
But any lcd screen can be used. 
The code simply measures the peak voltage on 2 analog inputs. After 5 sec, the values are reset to zero.
<br>
The code is as follows and can be <a href="calibcode.c">dowloaded here</a> <br>
<?php include("calibcode.html"); ?>
<br>
here is an image of the swr bridge connected to the analog1 and analog 2 pins of the arduino.
<br>
<img src="calibr.jpg"/>
<br>
<br>


<br>
<br>
<h2> Experiment ! </h2>
<br>

<br>
<video width="640" height="320" controls>
  <source src="sdr_ronde.webm" type="video/webm" />
</video>
<br><BR>
</html>
