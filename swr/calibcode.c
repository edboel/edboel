//swr calibrate tool, connect to fox delta hf bridge (fd-hfb2) as follows:
// sub d pin 1 -> +5v on arduino
// pin 2 -> gnd
// pin 3 -> A1
// pin 4 -> A2

#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);           // select the pins used on the LCD panel

int peakForward;
int peakReverse;
int counter;

void setup(){
   pinMode(A0,INPUT);
   pinMode(A1,INPUT);
   pinMode(A2,INPUT);
   lcd.begin(16, 2);               // start the library
   peakForward =0;
   peakReverse =0;
   counter = 0;
}
 
void loop(){
  counter++;
  if(counter==1000){
    counter=0;
    peakForward=0; 
    peakReverse=0;
  }
   lcd.setCursor(9,1);             // move cursor to second line "1" and 9 spaces over
   lcd.print(millis()/1000);       // display seconds elapsed since power-up

   lcd.setCursor(0,0);
   char buffer[20];
   
   if(analogRead(1) > peakForward){
       peakForward = analogRead(1);
   }
   if(analogRead(2) > peakReverse){
       peakReverse = analogRead(2);
   }
   sprintf(buffer,"v1 %d v2 %d     ",peakForward,peakReverse );
   lcd.print(buffer);
   
}
 
