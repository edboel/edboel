#include <ShiftRegLCD.h>

// declare LCD on pin D6 and D7
ShiftRegLCD srlcd(6, 7, TWO_WIRE,2);
                                                  
// variables used for receiving the messages
unsigned int pulseWidth = 0;
unsigned int latchStage = 0;
signed int bitCount = 0;
byte bit = 0;
byte prevBit = 0;

// variables for storing the data received
unsigned long sender = 0;
unsigned int recipient = 0;
byte command = 0;
bool group = false;

// variables for sending messages
byte messageType;
unsigned int messageCount;


void setup() {
  srlcd.print("ready.");

        // ensure the receiver pin is set for input
        DDRB &= ~_BV(PINB0);
        
        // disable PWM (default)
        TCCR1A = 0x00;
        
        // set prescaler to 1/8.  TCNT1 increments every 0.5 micro seconds
        // falling edge used as trigger
        TCCR1B = 0x02;
        
        // enable input capture interrupt for timer 1
        TIMSK1 = _BV(ICIE1);

}


ISR(TIMER1_CAPT_vect)
{
        // reset counter
        TCNT1 = 0;
        
        // get value of input compare register, divide by two to get microseconds
        pulseWidth = (ICR1 / 2);
        
        if(bit_is_clear(TCCR1B, ICES1))
        {       // falling edge was detected, HIGH pulse end
                
                if(latchStage == 1 && pulseWidth > 230 && pulseWidth < 280)
                {       // advanced protocol latch
                        
                        latchStage = 2;
                }
                else if(latchStage == 3 && (pulseWidth < 150 || pulseWidth > 500))
                {       // advanced protocol data out of timing range
                        
                        latchStage = 0;
                        bitCount = 0;
                        
                        sender = 0;
                        recipient = 0;
                }
                else if(latchStage == 1)
                {       // simple protocol data
                        
                        bitCount++;
                        
                        if(pulseWidth > 320 && pulseWidth < 430)
                        {
                                bit = 0;
                        }
                        else if(pulseWidth > 1030 && pulseWidth < 1150 && bitCount % 2 == 0)
                        {
                                bit = 0x08;
                        }
                        else
                        {       // start over if the low pulse was out of range
                                
                                latchStage = 0;
                                bitCount = 0;
                                
                                sender = 0;
                                recipient = 0;
                                // print error sign
                                srlcd.print("-");
                        }
                        
                        if(bitCount % 2 == 0)
                        {
                                if(bitCount < 9)
                                {
                                        sender >>= 1;
                                        sender |= bit;
                                }
                                else if(bitCount < 17)
                                {
                                        recipient >>= 1;
                                        recipient |= bit;
                                }
                                else
                                {
                                        command >>= 1;
                                        command |= bit;
                                }
                        }
                        
                        if(bitCount == 25)
                        {       // message is complete
                                
                                   srlcd.print("s"); 
                                   srlcd.print(sender); 
                                   srlcd.print("r"); 
                                    srlcd.print(recipient);
                                   srlcd.print("c");  
                                  
                                     srlcd.print(int(command)); 
                                     srlcd.print("e"); 
                                 
                                
                                latchStage = 0;
                                bitCount = 0;
                                
                                sender = 0;
                                recipient = 0;
                        }
                }
        }
        else
        {       // raising edge was detected, LOW pulse end
                
                if(latchStage == 0 && pulseWidth > 9480 && pulseWidth < 11500)
                {       // pause between messages
                
                        latchStage = 1;
                }
                else if(latchStage == 2 && pulseWidth > 2550 && pulseWidth < 2750)
                {       // advanced protocol latch
                        
                        latchStage = 3;
                }
                else if(latchStage == 3)
                {       // advanced protocol data
                     srlcd.print("v3");
                 }
        }
        
        // toggle bit value to trigger on the other edge
        TCCR1B ^= _BV(ICES1);
}




void loop()
{
  // do nothing, everything is handled via the interupt
  
}
