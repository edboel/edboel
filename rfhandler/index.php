<html>
<br>
<h1>arduino 433MHz web based tranceiver</h1>
<br>
edsard boelen, 16 mar 2011<br>
<br>
<hr>
<i>controlling your house with relay cards and cabled networks is just not going to make it. 
I have some RF controlled switches connected to some lights and devices in my living room. 
It would be nice if I could send and recieve the data from that system. 
I bought an aurel receiver from conrad and connected it to an arduino board. 
with some help from the homeeasy project I could easily decode the data!</i>
<br>
<hr>
<br>
<h3>The hardware</h3>
I used the following items:
<ul>
	<li>Arduino uno</li>
	<li>Arduino Ethernet shield</li>
	<li>Aurel 4m50rr30sf receiver</li>
	<li>Aurel tx-saw mid transmitter</li>
	<li>HD44780 compatible LCD</li>

</ul>


<h3>the protocol</h3>
The protocol that is used, is like that of the leaked domia energy manager protocol specification: 
There is no start sequence, or device negotiation, or ack. It just starts sending the data. Each bit takes 1.5 mS. 
The bits are being send as follows: <br>
A 'zero' consists of 375uS high, 1125 uS low, <br>
a 'one' consists of 1125uS high, 375uS low.<br>
<img src="protocol.jpg"/><br>
<br>
The data itself consists of 25 bits for the simple protocol, 64 for the advanced. I will only use the simple protocol.<br>
The simple protocol sends all the 'odd' bits as 'zero' the 'even bits' sends 3 values:<br>
Bit 2, 4, 6, 8. define the Sender ID<br>
Bit 10,12,14,16 define the Receiver ID<br>
Bit 18,20,22,24 define the Command<br>


<h3>Step 1: receiving data</h3>
Receiving data is my first step, because I have a cheap brand system called flamingo, so I really don't know what the device sends. 
First I tested the receiver with just a LED connected to the output pin. 
It did light up when I pressed a button on the flamingo remote.
After that I connected the Aurel receiver to PIN B0 (ardiono port 8). 
I connected an LCD screen to PIN D6 and D7 (arduino port 6,7).
It could also be done with the serial port, or some leds, instead of the LCD.
It is just to see what values are being send. <br>

This is how it's connected:<br>
<img src="receiver_schem.png"/><br> 
<br>
The code:<br>
Thankfully, the people from the homeeasy project have made a lot of progress, so I could use their code as an example. Saved me a lot of time.<br>
The main loop does nothing, evertything is handled via the interupt.<br>
<a href="receive_test.html">receive_test.c</a> <a href="receive_test.c">c</a> 
<a href="sketch_mar17b.pde">.pde</a> <br>


 
This is how it looks. notice the top right item, that is the transmitter, it is not connected yet, next to it in the alu housing is the receiver.<br>
<img src="receiver_ready.jpg"/><br>
<br>
Here you see some data received by the transmitter<br>
<img src="receiver_data.jpg"/><br>
<h3>Step 2: sending data</h3>
Now that I know what values are being send by the remote, 
I can test the transmitter. The setup is even more simple. 
Just the arduino and the Aurel TX-SAW MID module.  
The code will send an ON command to device 12, waits 2 seconds and then send an OFF command and waits two seconds.
That is in the main loop.<br>
The actual sending is being done in the timer interrupt routine.<br>
<a href="transmit_test.html">transmit_test.c</a> <a href="transmit_test.c">c</a> <a href="transmit_test.pde">.pde</a> <br>
<br>


<h3>Step 3: web connection</h3>
The <a href="http://arduino.cc/en/Guide/ArduinoEthernetShield">Ethernet shield</a> 
allows you to make web based devices in minutes. <br>
What the web connection has to do is, send rf commands via http requests. And if commands ore detected, send it to a pre-defined server.<br>
At first it did not work properly, after some testing, I discovered that pins 11,12,13 are being used for the ethernet connection<br>
Pins 8 and 9 are being used for the interrupt timer (COM1A registers control the behavior of OC1A).
<br>
To test if the webserver works, just open the ip adres in a browser, it should respond with 'rfhandler ready.'<br>
If so, just add the code after the ip adres: ex. http://10.10.10.111/07-12-11. This would send command '11' to device '12' <br>

<a href="ethernet.html">ethernet.c</a><br>
<br>
This is an image of the arduino with ethernet shield and on top of it a board with in front the receiver, and in the back the transmitter.<br>
<img src="ethernet.jpg" /><br>
<br>


<h3>Step 4: server app</h3>
Unfortunately, the IPv6 protocol is not supported yet by the ethernet shield. The wiznet w5100 chip has a hardware ipv4 stack, bypassing that (if even possible) would mean that the ip has to be handled by the arduino itself, which is an entirely new project on its own.<br>
to make it short, I will use my server as a bridge between the 2 networks.<br>
It consists of some php code which relays the commands.<br>
<img src="kastje.jpg"/><br>
this is the final device, the red and black cables sticking out are antennae for send and receive, the two leds are for signaling send and receive.<br>
<br>

<h3>Step 5: android app</h3>


<br>
<br>
<br>

<br>
<br>

</html>

