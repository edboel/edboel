<html>
  <head>
    <title>computer controlled switch</title>
    <meta content="">
    <style></style>
  </head>
  <body>
  <p>
  <h1>computer controlled switch</h1>
   Edsard Boelen<br>
v0.1, &nbsp; 26-apr-2009<BR>
<br>
  <hr><i>
As I am getting more and more comfortable with the atmega88 USB solution, it is time to make a larger system. The scematic is not very original, it will be the default v-usb demo project.
  </i><hr>
   <br>
<h3>day 0, the idea</h3>
The idea is to make some computer equipment be turned on/off by the computer itself. i.e. printer,scanner,amplifier,monitor,lights. But it should also be possible to do it by external buttons, going software only has its disadvantages. 
<br>
This will be the design:<br>
<img src="design.gif" /><br>


<h3>day 1, the prototype</h3>
It has been a nice sunday, and I had all time time to make the prototype, below you can see the result:<br>
<img src="switch1.jpg"/>

   <br>
<h3>day 2, write firmware</h3>
I used the firmware from the powerswitch project, modified it to suit the atmega88 controller and added buttons to it. The buttons have a debounce routine. And I removed the timer function, the timer was very clever made, but I didn't need it.<br>
<br>
The code can be downloaded <a href="">here</a> or view the html version below<br>
<br><a href="firmware.htm">main.c</a><br>
<h3>day 3, write application and web control</h3>
What fun is a switch if it cannot be controlled via the web. I created two system programs:<br>
<a href="">powerstatus</a><br>
<a href="">powertoggle</a><br>
The first one returns on each line the status of one port. The second one toggles the power with the number of the second argument.<br>

<br>
<br><Br><br><br>

  </body>
</html>
