# Noise level meter
eboelen 11 jan 2020

## hardware
- esp32 devkit board
- inmp441 or ICS-43434

```
pinout:
.bck_io_num = 14,       // BCKL -- SCK  gpio14= pin5
.ws_io_num = 15,        // LRCL -- WS   gpio15= pin3
.data_in_num = 32       // DOUT --SD    gpio32= pin10
```
