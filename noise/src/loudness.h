#include <Arduino.h>
#include <driver/i2s.h>
#define SAMPLES 1024
#define OCTAVES 9

class Loudness {
public:
    
    void begin();
    float readLoudness();
    void resetPeriod();

    uint16_t getPeakPeriod();
    uint16_t getAveragePeriod();
    float avPeriod = 0;
    float peakPeriod = 0;


private:

    int timePeriod =0;
    int readingsPeriod =0;
    float totalPeriod =0;
 
    void integerToFloat(int32_t * integer, float *vReal, float *vImag, uint16_t samples);
    void calculateEnergy(float *vReal, float *vImag, uint16_t samples);
    void sumEnergy(const float *bins, float *energies, int bin_size, int num_octaves);
    float decibel(float v);
    float calculateLoudness(float *energies, const float *weights, int num_octaves, float scale);
    uint16_t f2sflt16(float val);
};
