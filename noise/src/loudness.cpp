#include "loudness.h"
#include "arduinoFFT.h" 

const i2s_port_t I2S_PORT = I2S_NUM_0;
const int BLOCK_SIZE = SAMPLES;


// our FFT data
static float real[SAMPLES];
static float imag[SAMPLES]; 
static arduinoFFT fft(real, imag, SAMPLES, SAMPLES);
static float energy[OCTAVES];
// A-weighting curve from 31.5 Hz ... 8000 Hz
static const float aweighting[] = { -39.4, -26.2, -16.1, -8.6, -3.2, 0.0, 1.2, 1.0, -1.1 };


void Loudness::begin(){
    esp_err_t err;
    // The I2S config as per the example
    const i2s_config_t i2s_config = {
        .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_RX),      // Receive, not transfer
        .sample_rate = 22627,
        .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
        .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,   // although the SEL config should be left, it seems to transmit on right, or not
        .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,       // Interrupt level 1
        .dma_buf_count = 8,     // number of buffers
        .dma_buf_len = BLOCK_SIZE,      // samples per buffer
        .use_apll = true
    };

    // The pin config as per the setup
    const i2s_pin_config_t pin_config = {
        .bck_io_num = 14,       // BCKL -- SCK  gpio14= pin5
        .ws_io_num = 15,        // LRCL -- WS   gpio15= pin3
        .data_out_num = -1,     // not used (only for speakers)
        .data_in_num = 32       // DOUT --SD    gpio32= pin10
    };

    // Configuring the I2S driver and pins.
    // This function must be called before any I2S driver read/write operations.
    err = i2s_driver_install(I2S_PORT, &i2s_config, 0, NULL);
    if (err != ESP_OK) {
        Serial.printf("Failed installing driver: %d\n", err);
        while (true);
    }
    err = i2s_set_pin(I2S_PORT, &pin_config);
    if (err != ESP_OK) {
        Serial.printf("Failed setting pin: %d\n", err);
        while (true);
    }
    Serial.println("I2S driver installed.");
}

void Loudness::integerToFloat(int32_t * integer, float *vReal, float *vImag, uint16_t samples)
{
    for (uint16_t i = 0; i < samples; i++) {
        vReal[i] = (integer[i] >> 16) / 10.0;
        vImag[i] = 0.0;
    }
}

// calculates energy from Re and Im parts and places it back in the Re part (Im part is zeroed)
void Loudness::calculateEnergy(float *vReal, float *vImag, uint16_t samples)
{
    for (uint16_t i = 0; i < samples; i++) {
        vReal[i] = sq(vReal[i]) + sq(vImag[i]);
        vImag[i] = 0.0;
    }
}

// sums up energy in bins per octave
void Loudness::sumEnergy(const float *bins, float *energies, int bin_size, int num_octaves)
{
    // skip the first bin
    int bin = bin_size;
    for (int octave = 0; octave < num_octaves; octave++) {
        float sum = 0.0;
        for (int i = 0; i < bin_size; i++) {
            sum += real[bin++];
        }
        energies[octave] = sum;
        bin_size *= 2;
    }
}

float Loudness::decibel(float v)
{
    return 10.0 * log(v) / log(10);
}

// converts energy to logaritmic, returns A-weighted sum
float Loudness::calculateLoudness(float *energies, const float *weights, int num_octaves, float scale)
{
    float sum = 0.0;
    for (int i = 0; i < num_octaves; i++) {
        float energy = scale * energies[i];
        sum += energy * pow(10, weights[i] / 10.0);
        energies[i] = decibel(energy);
    }
    return decibel(sum);
}


float Loudness::readLoudness()
{
    static int32_t samples[BLOCK_SIZE];
    // Read multiple samples at once and calculate the sound pressure
        size_t num_bytes_read;
        esp_err_t err = i2s_read(I2S_PORT,
                                (char *) samples,
                                BLOCK_SIZE,        // the doc says bytes, but its elements.
                                &num_bytes_read,
                                portMAX_DELAY);    // no timeout
        //int samples_read = num_bytes_read / 8;
        // integer to float

        integerToFloat(samples, real, imag, SAMPLES);

        if(err != ESP_OK){
            Serial.print("err");
            Serial.println(err);

        }

        // apply flat top window, optimal for energy calculations
        fft.Windowing(FFT_WIN_TYP_FLT_TOP, FFT_FORWARD);
        fft.Compute(FFT_FORWARD);

        // calculate energy in each bin
        calculateEnergy(real, imag, SAMPLES);

        // sum up energy in bin for each octave
        sumEnergy(real, energy, 1, OCTAVES);

        // calculate loudness per octave + A weighted loudness
        float loudness = calculateLoudness(energy, aweighting, OCTAVES, 1.0);

        
        readingsPeriod++;

        totalPeriod += loudness;
        if(loudness > peakPeriod){
            peakPeriod = loudness;
        }
        avPeriod = totalPeriod / readingsPeriod;
        //Serial.println(avMinute);
    
        return loudness;

}

void Loudness::resetPeriod(){

            totalPeriod     =0;
            readingsPeriod  =0;
            avPeriod        =0;
            peakPeriod      =0;

            //timePeriod = millis();
}


uint16_t Loudness::getPeakPeriod(){
    float peak = peakPeriod;
    peak = peak / 100;
    return f2sflt16(peak);
}

uint16_t Loudness::getAveragePeriod(){
    float av = avPeriod;
    av = av / 100;
    return f2sflt16(av);
}

//from https://github.com/mcci-catena/arduino-lmic/blob/master/src/lmic/lmic_util.c
uint16_t Loudness::f2sflt16(float f) {
        if (f <= -1.0)
                return 0xFFFF;
        else if (f >= 1.0)
                return 0x7FFF;
        else
                {
                int iExp;
                float normalValue;
                uint16_t sign;

                normalValue = frexpf(f, &iExp);

                sign = 0;
                if (normalValue < 0)
                        {
                        // set the "sign bit" of the result
                        // and work with the absolute value of normalValue.
                        sign = 0x8000;
                        normalValue = -normalValue;
                        }

                // abs(f) is supposed to be in [0..1), so useful exp
                // is [0..-15]
                iExp += 15;
                if (iExp < 0)
                        iExp = 0;

                // bit 15 is the sign
                // bits 14..11 are the exponent
                // bits 10..0 are the fraction
                // we conmpute the fraction and then decide if we need to round.
                uint16_t outputFraction = ldexpf(normalValue, 11) + 0.5;
                if (outputFraction >= (1 << 11u))
                        {
                        // reduce output fraction
                        outputFraction = 1 << 10;
                        // increase exponent
                        ++iExp;
                        }

                // check for overflow and return max instead.
                if (iExp > 15)
                        return 0x7FFF | sign;

                return (uint16_t)(sign | (iExp << 11u) | outputFraction);
                }
        }
