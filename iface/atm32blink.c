#define F_CPU 4000000UL  /* 4 MHz CPU clock */
#include <util/delay.h>
#include <avr/io.h>


// avr-gcc -mmcu=atmega32 -Os atm32blink.c
// avr-objcopy -O ihex a.out blink32.hex
// avrdude -p m32 -c bsd -U flash:w:blink32.hex:i -U lfuse:w:0xe3:m -U hfuse:w:0xd9:m 
//

int main (void)
{
        DDRA = 0xFF; //set port A pins as outputs
		// clear all
		PORTA = 0xFF;
		// wat a sec
		_delay_ms(1000); 
		
        while (1)                       /* loop forever */
        {
		/* set PC0 on PORTC (digital high) and delay for 200mS */
		/* &= means AND compare so 00000000 AND 00000000     	~ means invert _BV(PA0) means bitvalue PA0 PA0 is 10000000 invert will make it 01111111 

		the result is that the first bit will always be zero, and the other bits will keep their state*/
                PORTA &= ~_BV(PA0);                
                _delay_ms(200);
                
		/* set PC0 on PORTC (digital high) and delay for 500mS */
                PORTA &= ~_BV(PA1);                
                _delay_ms(200);
                
		/* set PC0 on PORTC (digital high) and delay for 500mS */
                PORTA &= ~_BV(PA2);                
                _delay_ms(200);
                
		/* set PC0 on PORTC (digital high) and delay for 500mS */
                PORTA &= ~_BV(PA3);                
                _delay_ms(200);
                

        /*  PC0 on PORTC (digital 0) and delay for 500mS */
				PORTA |= _BV(PA0);
						_delay_ms(200);
				
				PORTA |= _BV(PA1);
						_delay_ms(200);
				
				PORTA |= _BV(PA2);
						_delay_ms(200);
				
				PORTA |= _BV(PA3);
						_delay_ms(200);
					
		}

		
        

        return(0);
}