<html>
<br>
<h1>solar panels</h1>
<br>
edsard boelen, 24 aug 2013<br>
<br>
<hr>
<i>I have looked at generating electricity with solar panels for quite some time, wanted to power my webserver with it. But after some calculations I could easily see that panels give only 10% of their specified power in januari here in the Netherlands. 
It is very inefficient. But last couple of years, there has been a sharp rise in the usage of solar panels. 
The prices have dropped far enough for me to buy a 50W panel for 50 euro's. Just to do some experiments. 
As a side event LED lights are powerfull enough to be used as 12V home lighting. 
</i><br>
<hr>
<br>
<?php
	$power = 0;
	$filename="../sensor6/solar.log";
	$lines=file($filename);
	$wstart=count($lines)-840;
 	$totpow=0.0;	
	for($i=$wstart;$i < count($lines)-1; $i++){
		$line=$lines[$i];
		$expl = explode("|",$line);
		if(count($expl) > 2){
		$pow = $expl[2];
		$totpow = $totpow + $pow;
		}
		
	}
	$avpow = round($totpow / 5000,3);
	
 	$lastline=$lines[count($lines)-1];
	$expl = explode("|",$lastline);	
	$power = $expl[2];
	
?>

Current power generation:<b> <?php echo $power; ?> W</b> 
total this week <?php echo $avpow; ?> KWh <a href="graph.php">view graph</a><br>
<br>
<h2>The panel</h2>
<br>
The panel has the following specs:<br>
- 60x60x3.5 cm 5.5kg<br>
- 36 cells polycristalline<br>
- 50W max<br>
- 21.6V open<br>
- 3.2A short<br>
<br>
<br>
<img src="solar1.jpg" /> . 
<img src="solar2.jpg" /><br>
<img src="solar3.jpg" /><br>
<br>
<h2>First tests</h2>
The first test results are in, on a sunny day in august.<br>
- no load gave 20.5V<br>
- A load of 12 Ohm gave 17.5V at 1.5A = 26W <br> 
- A load of 3 Ohm gave 6V at 2.5A = 15W <br> 
<br>
<br>
<h2>The battery</h2>
Some of the power will be stored in a accu at first. Maybe later on deliver it to the net. Or use it to power at least the router.<br>
The one I got now is a cheap lead acid car battery. 44AH for 40 euro. It will do just fine if I keep it in good condition. <br>
It is best to keep the charge voltage below 14V, this will prevent gas forming and over heating.
According to the graph below, the accu is in top condition if it gives 12.6V at 25 degrees.<br>
<img src="accu_voltage_d.png"/> <img src="accu1.jpg"/><br><br> 
The following image is the charge voltage image. <br>
<img src="accu_voltage_c.png"/><br>


<h2>Loading circuit</h2>
The accu should be charged with a voltage of 14.3 Volt<br>
I will use a LT431 with an IRFZ44 to charge only if the voltage is below 14.3.<br>
<img src="charge_circuit1.png" /><img src="charge_circuit2.jpg" /><br>
To calibrate this, set a 14.9V power supply instead of the solar panel on the connector. Set the variable resistor at such a point that the led will just light up.<br>

<h2>fallback switch circuit</h2>
As this website is now running partially from the solar panel, a fallback switch is needed,
 which will turn on the main power supply when the battery voltage is dropping below a certain point.
Also a low dropout power regulator will be used to prevent any damage to the equipment.<br>
<img src="fallback_circuit.jpg" /><br>

<h2>Monitoring circuit</h2>
What fun is a panel if you cannot monitor the output online.<br>
To measure the current  I used a 0.1 Ohm shunt resistor it will give 100mV at 1A. 
To measure the voltage I used a 1/4 voltage divider. it will give 3V at 12V.  
Both will go to the analog inputs of an arduino (which can be powered by the solar panel itself)
The arduino code can be downloaded <a href="read_solar.c">here readsolar.c</a>
Every 10 seconds it will send over the serial line the current voltage and current and if it is charging. It will also send the hourly average power generated. 
At first I thought I could use a FIFO, but the libraries don't have a function to read the entire fifo at once. (which I need to calculate the average. So now I just put it into an array for one hour of measurements (360) and keep
track of the position so each new value will overwrite the oldest one.
<br>
<img src="measure_solar.jpg" /><br>
<br>
As you can see I added some extra items to the charge circuit. 
One of the is a 4A fuse. The battery can give such high loads 
that in a case of short circuit a fire could easily happen. 
<br>
<br>
<br>
<br>
<?php echo $totpow; ?>
</html>

