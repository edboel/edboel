<?php
  include("phplot.php");
//  $timep = $_GET["timep"];
//  include("read_data.php");

$graph = new PHPlot(1000,400);


$stack = array();
$lfile="../sensor6/solar.log";
$mdata= file($lfile);
$cend= count($mdata);
$cbegin= $cend-636;

$i=$cbegin;
$labelcount=$prev=0;

for($i; $i<$cend;$i++){
	$mline=$mdata[$i];
	$expl= explode("_",$mline);

        $expl2=explode("|",$expl[1]);
	$volt = $expl2[0];
	$current = $expl2[1]; 
	$avp = $expl2[2];
	$charge = substr($expl2[3],0,-2);
	if($charge != '1') $charge =0;
	#rtrim($current,";");
	#echo $expl[0]." .. ".$expl2[0]." ..: ".$current ."==".$avp."  <br>";
	if($labelcount==30){
		array_push($stack, array($expl[0],$expl2[0],$current,$avp,$charge));
		$labelcount=0;
	}else{
		array_push($stack, array('',$expl2[0],$current,$avp));
		$labelcount++;
	}

}

//$graph->SetDataValues($example_data);


  $graph->SetXDataLabelAngle(90);
  $graph->SetDataValues($stack);
  $graph->SetYLabel("avg. Power (W)");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue","green","grey"));
  //$graph->SetLegend(array("room","outside"));
  $graph->SetLegendPixels(50,10);
  $graph->DrawGraph();

?>

