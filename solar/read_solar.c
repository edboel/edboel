int count=0;
double avarr[360];
boolean firstrun=true;

void setup() {
   Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A1);
  Serial.print("I:");
  double current = sensorValue / 16.6;
  Serial.println(current);
  delay(2500);
  
  sensorValue = analogRead(A2);
  double volt = sensorValue / 31.6;
  Serial.print("U:");
  Serial.println(volt);
  delay(2500);

  sensorValue = analogRead(A0);
  Serial.print("C:");
  if(sensorValue > 400 && sensorValue < 450){
    Serial.println("1");
  }else{
    Serial.println(sensorValue);
  }
  delay(2500);

  int maxv=360;
  double avp=0;
  double totp=0;
  if(firstrun){
    maxv = count+1;  
  }
  avarr[count]=current;
  for(int j=0;j<maxv;j++){
     totp=totp+avarr[j]; 
  }
  count++;
  if(count==360){
    firstrun=false;
    count=0; 
  }
  Serial.print("P:");
  avp=totp / maxv;
  avp = avp * 12;
  Serial.println(avp);
  delay(2500);
  
}
