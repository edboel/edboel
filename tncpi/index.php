<html>
<head><title>tnc pi project </title></head>
<br>
<center>
<h2> TNC PI <h2>
<h3>A raspberry pi TNC</h3>
</center><br>
Edsard Boelen, 09-november-2013
<hr><i>
I wanted to have a good TNC so I can do some APRS experiments. TNC's come in all kinds of variants and prices. I choose the TNCpi because of the good functionality, reviews and the cost.
<hr></i>
<br>
<h2>short introduction to TNC's</h2>
<br>
<br>
<h2>building the kit</h2>
<br>
<img src="tncpi1.jpg" /><br>
<img src="tncpi2.jpg" /><br>
<img src="tnc-pi-built.jpg" /><br>
The datacable for my yaesu 9800r is as follows:<br>
<img src="tncpi_cable.jpg" /><br>
<br>
<h2>install software and connect</h2>
The tncpi uses the standard kiss or ax25 protocol, just install ax25-tools which is available for most distributions and run: <br>
axlisten -a<br>
<img src="tncpi3.jpg" /><br>
after installing and configuring xastir, I could transmit to a local repeater/gateway. My sign appeared on aprs.fi within minutes.<br>
<img src="tncpi4.jpg" /><br>
<br>
<h2> adding LCD and RTC</h2>
I will use the I2C capabillities to add an LCD and real time clock to it.<br>
<br>
<h2>casing</h2>
For the power supply, I opened up an old adapter and put it in the case. 
The RTC and LCD fit barely in the case.<br>
 

<br>
