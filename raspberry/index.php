<html>
<br>
<h1>Raspberry pi projects</h1>
<br>
edsard boelen, 14 july  2012<br>
<br>
<hr>
<i>The <a href="http://raspberrypi.org">raspberry pi</a> is a low cost development board with specs that blows the competetion away.
For example the arduino which I used for a lot of other projects is about the same price
 but is a factor 10 slower, has 10 times less memory, no operating system etc. <br>
I finally got one after months of waiting. I have a couple of ideas:<br> 
1: alarm clock / radio / bedroom tv<br>
2: mame emulator game console <br>
3: rowing interface<br>

<br>
<hr>
<br>
<br>
<br>
<h3>the hardware interfaces</h3>
<br>
The raspberry pi has among many other connectors,  a gpio header, see the image below for the pinout. <br>
<img src="GPIOs.png" /> <br>
As you can see, the layout is a bit borked, so I will try to use the I2C interface. (GPIO 0 and GPIO 1) I will use the PCF8574 I2C to 8 bit expanders to connect other stuff to the raspberry pi.<br>

<h3>the I2C interface</h3>
<br>
The pins 3 and 5 on the connector are the I2C pins. The pins 2 and 6 are the power pins. You can power the raspberry pi from there instead of via the micro USB.<br>
I Made a small test board as shown on the image below.<br>
<img src="i2c_test_sch.png" /><br>
Note:<br>
The schematics have been made using <a href="http://www.kicad-pcb.org" >kicad</a><br>
<br>
<img src="i2c1.jpg"/><br>
To use the i2c devices in linux, you need to have the i2c-dev module compiled and loaded into your kernel.<br>
that can be done with modprobe or load it on boot by adding it to the /etc/modules.conf.<br>
Install i2c-tools with apt-get install i2c-tools<br>
<br>
The command i2cdetect should now detect a device on address 0x20 as the pins a0,a1 and a2 are connected to gnd.<br>
If that works, control some leds with i2cset.<br>
- i2cset -y 0 0x20 0x00<br>
- i2cset -y 0 0x20 0x40<br>
etc.<br>
<h3> buttons </h3>
The top 4 connections will be used for some buttons, 
I will use 2 normal push buttons, and one rotary pulse switch. One side of the switches are connected to the ground. The pins on the IC are all pulled up with 10K resistors.<BR>


<h3>LCD</h3>
<br>
Driving an lcd screen is a bit trickier. I will add another PCF8574 but put a0 to +5v with a 10k resistor so it will be addressed at 0x21.<br>
Most LCD screens are HD44780 compatible, I have a couple of them. Some python libraries are already available.
So I will use python for this, but it still needed some tweaking to make it work in my setup.
<br>
<img src="i2c_lcd_sch.png"/><br>
<br>
<img src="lcd1.jpg" /><br>
<br>
<br>
I have connected the lcd in 4 bit mode as follows to the PCF8574:<br>
P0 - D4<br>
P1 - D5<br>
P2 - D6<br>
P3 - D7<br>
P4 - RS<br>
P5 - R/W<br>
P6 - E<br>
<br>
the python code can be found <a href="pylcd2.py">pylcd2.py</a> 
and <a href="testlcd.py"> testlcd.py </a><br>
<br>
<br>
<h3> 7 segment digits </h3><br>
Although more primitive, it takes a lot more soldering to build the 7 segment drivers. <br>
The driver IC CD4511 will be used. The first 4 bits of the PCF8574 will be used as bcd input. The higher 4 bits will be used to select the digit.
<br>
<img src="i2c_digit_sch.png" /><br>

<br>
 
<img src="digit.jpg" /><br>
<br> 
<br>
The code that drives them. can be found at <a href="pydigit.py">pydigit.py</a> 
and a test app which shows the time is: <a href="pytime.py">pytime.py</a> <br> 


<br>
<h3>casing</h3>
The casing looks as follows:<br>
<img src="casing1.jpg" /><br>
and opened up:<br>
<img src="casing1o.jpg" /><br>
<br>
<h3> play music</h3>
To let the raspberry pi play music, only a speaker needs to be added. The rest is all in software.<br>
First the sound module needs to be loaded into the kernel. As it is still in beta.<br>
modprobe snd-bcm2835<br>
apt-get install alsa-utls mpg123<br>
a better alternative is mplayer, it can handle all sorts of audio formats and streams. <br>
add the module in /etc/modules.conf <br>
set volume with amixer and store it with alsactl<br>
<br>
When I connected a small speaker to the raspberrypi, I noticed that the power output was very low. So made a small power amplifier of two transistors I had lying around.<br>
<img src="amp1.png"/><br>

<h3> start on bootup </h3>
To have the clock, the lcd and the music to run automatically after boot,<br>
just add some scripts to /etc/rc.local.<br>

<Br>
<br>
<Br>
<br>
</html>

