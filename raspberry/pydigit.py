import smbus
from time import *

# General i2c device class so that other devices can be added easily
class i2c_device:
        def __init__(self, addr, port):
                self.addr = addr
                self.bus = smbus.SMBus(port)

        def write(self, byte):
                self.bus.write_byte(self.addr, byte)

        def read(self):
                return self.bus.read_byte(self.addr)

        def read_nbytes_data(self, data, n): # For sequential reads > 1 byte
                return self.bus.read_i2c_block_data(self.addr, data, n)

class digit:
	def __init__(self,addr,port):
		self.devi = i2c_device(addr,port)

	def writetime(self,hr,min):
		shr = str(hr)
		smin = str(min)
		if(hr < 10):
			self.devi.write(224)
			self.devi.write(208 + int(shr[0]))
		else:
			self.devi.write(224+int(shr[0]))
			self.devi.write(208+int(shr[1]))

                if(min < 10):
                        self.devi.write(176)
                        self.devi.write(112 + int(smin[0]))
                else:
                        self.devi.write(176+int(smin[0]))
                        self.devi.write(112+int(smin[1]))


	def clear(self):
		self.devi.write(0x0f)

