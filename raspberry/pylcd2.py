'''  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''

import smbus
from time import *

# General i2c device class so that other devices can be added easily
class i2c_dev:
	def __init__(self, addr, port):
		self.addr = addr
		self.bus = smbus.SMBus(port)

	def write(self, byte):
		self.bus.write_byte(self.addr, byte)

	def read(self):
		return self.bus.read_byte(self.addr)

	def read_nbytes_data(self, data, n): # For sequential reads > 1 byte
		return self.bus.read_i2c_block_data(self.addr, data, n)


class lcd:
	#initializes objects and lcd
	'''
	pinout is as follows: PCF8574 - HD44780
	P0 - LCD11 
	P1 - LCD12	
	P2 - LCD13
	P3 - LCD14	
	P4 - LCD4
	P5 - LCD5	
	P6 - LCD6	
	'''
	def __init__(self, addr, port):
		self.lcd_device = i2c_device(addr, port)
		self.lcd_device.write(0x03)
		self.lcd_strobe()
		sleep(0.0005)
		self.lcd_strobe()
		sleep(0.0005)
		self.lcd_strobe()
		sleep(0.0005)
		self.lcd_device.write(0x02)
		self.lcd_strobe()
		sleep(0.0005)

		self.lcd_write(0x28)
		self.lcd_write(0x08)
		self.lcd_write(0x01)
		self.lcd_write(0x06)
		self.lcd_write(0x0C)

		self.lcd_write(0x0F)

	# clocks EN to latch command
	def lcd_strobe(self):
		self.lcd_device.write((self.lcd_device.read() | 0x40))
		self.lcd_device.write((self.lcd_device.read() & 0xbF))

	# write a command to lcd
	def lcd_write(self, cmd):
		self.lcd_device.write((cmd >> 4))
		hi= self.lcd_device.read()
		self.lcd_strobe()
		self.lcd_device.write((cmd & 0x0F))
		lo= self.lcd_device.read()
		self.lcd_strobe()
		self.lcd_device.write(0x0)
		print 'cmd',cmd,hi,lo

	# write a character to lcd (or character rom)
	def lcd_write_char(self, charvalue):
			print "char",charvalue
			self.lcd_device.write((0x10 | (charvalue >> 4)))
			self.lcd_strobe()
			self.lcd_device.write((0x10 | (charvalue & 0x0F)))
			self.lcd_strobe()
			self.lcd_device.write(0x0)

	# put char function
	def lcd_putc(self, char):
		self.lcd_write_char(ord(char))

	# put string function
	def lcd_puts(self, string, line):
		if line == 1:
			self.lcd_write(0x10)
		if line == 2:
			self.lcd_write(0xC0)
		if line == 3:
			self.lcd_write(0x94)
		if line == 4:
			self.lcd_write(0xD4)

		for char in string:
			self.lcd_putc(char)

	# clear lcd and set to home
	def lcd_clear(self):
		self.lcd_write(0x1)
		self.lcd_write(0x2)

	# add custom characters (0 - 7)
	def lcd_load_custon_chars(self, fontdata):
		self.lcd_device.bus.write(0x40);
		for char in fontdata:
			for line in char:
				self.lcd_write_char(line)

