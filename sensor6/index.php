<html>
<br>
<h1>small online weather station</h1>
<br>
edsard boelen, 12 okt 2012<br>
<br>
<hr>
<i>
Since the arduino temp sensor died after 1 and a half year of operation, I thought maybe look around for a weather station to connect to the computer. 
I found an inexpensive one with a usb interface: based on the 
<a href="http://www.foshk.com" > Fine Offset WH1080</a> 
</i>
<br>
<hr>
<br>
view current graphs <a href="#graphs">here</a><br>
<br>

<br>
<h3> mounting the weather station </h3>
the package comes with a 20mm bar on which the various sensors can be mounted. I think I will place it on top of the dormer. A small wooden frame will hold everything in place.<br>
<img src="assemble.jpg" />
<br>
<h3>connecting to the computer</h3>
The data is sent wireless to a lcd display. The display has a usb connector which I have connected to a small server running linux.
 
The interface is not difficult. various projects are already there. I used the
<a href="http://code.google.com/p/fowsr">fowsr</a> It is a small program that writes the data to a xml file. I set the cron daemon to do that every 30 minutes.<br>
Every hour the new lines will be sent to the webserver, I made a small xml parser to <a href="table.php">view the table data</a>.<br> 
<br>
<h3>generating graphs</h3>
For the graph I still use <a href="http://www.phplot.com"/>phplot</a>.
But temperature, humidity, rainfall, wind speed/direction cannot be plotted in one graph, so multiple graphs will have to be generated.<br>
Phplot has the option to write the output to a file instead of realtime rendering. I will use another cron job which will call a PHP script that generates the different graphs.

<a id="graphs" />
<h3>the graphs</h3>
this week, updated hourly<br>
<br> 
<img src="temperature.png" /> <br>
<img src="humidity.png" />  <br>
<img src="pressure.png" />  <br>
<img src="rain.png" />  <br> 
<img src="wind.png" />  <br>

</html>

