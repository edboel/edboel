<?php
  include("phplot.php");
//  $timep = $_GET["timep"];
//  include("read_data.php");

$totempstack = array();
$tempstack = array();
$humstack = array();
$presstack = array();
$rainstack = array();
$windstack = array();
$directionstack = array();

$samplesperweek = 336;

$dirname="/var/www/web/projects/sensor6/";
$xml = simplexml_load_file($dirname.'fowsr.xml');

$count=0;
$timestampcount=0;
$rainbegin=0.0;
$itemcount=0;
foreach ($xml->children() as $elmname => $child){
	$datet="";
	$tempin="";
	$tempout="";
	$humin="";
	$humout="";
	$press="";
	$wind="";
	$rain=0.0;
	// < php 5.3
	$lines=count($xml->children());
 	$weekbegin=$lines-$samplesperweek;	
	foreach($child->children() as $cname => $cchild){
	  foreach($cchild->attributes() as $aname => $val){
		if($aname=="date"){
			$datet=$val;
		}

	 	if($aname=="temp_in"){
			$tempin=floatval($val);
		}
		if($aname=="temp_out"){
			$tempout=floatval($val);
		}
		if($aname=="hum_in"){
			$humin=floatval($val);
		}
		if($aname=="hum_out"){
			$humout=floatval($val);
		}
		if($aname=="abs_pressure"){
			$press=floatval($val);
		}
		if($aname=="wind_ave"){
			$wind=floatval($val);
		}
		if($aname=="rain"){
			$rain=floatval($val);
		}
	  }
		
		$count++;
		if($count == $weekbegin){
			$rainbegin = $rain;
		}
		if($count > $weekbegin){
		   $ldate="";
		   $timestampcount++; 
		   if($timestampcount==47){
			$ldate=$datet;
			$timestampcount=0;
		   }
		   if($timestampcount==15 || $timestampcount==30){
		        $ldate=substr($datet,11,13);
		   }
		   $itemcount++;
		   array_push($tempstack, array($ldate,$tempin,$tempout));
		   array_push($humstack, array($ldate,$humin,$humout));
		   array_push($presstack, array($ldate,$press));
		   array_push($rainstack, array($ldate,$rain-$rainbegin));
		   array_push($windstack, array($ldate,$wind));
		}
	}
}

$mfile = "temps.log";
$lines = file($dirname.$mfile);
$lcount = count($lines)-$samplesperweek-7;
$i =0;
$j=count($tempstack)-1;

for ($i=count($lines);$i--;$i>0){
	if(strrpos($lines[$i],'|') && $j >0){
			$temps=explode('|',$lines[$i]);
			$tempstack[$j]=array_merge($tempstack[$j],$temps);
			$j--;
	}
}



$graph = new PHPlot(900,400,$dirname."temperature.png");
  $graph->SetDataValues($tempstack);

$graph->SetIsInline(true);
 $graph->SetOutputFile($dirname."temperature.png");
  $graph->SetXDataLabelAngle(90);
  $graph->SetYLabel("Temp C");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  //$graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0),array(00,00,166),array(00,166,166),array(166,100,100),array(100,50,100)),"");
  $graph->SetLegend(array("room","outside","room2","outside2","r3","r4"));
  $graph->SetLegendPixels(70,10);
  $graph->DrawGraph();


$graph = new PHPlot(900,200,"$dirname.humidity.png");
  $graph->SetDataValues($humstack);

$graph->SetIsInline(true);
 $graph->SetOutputFile($dirname."humidity.png");
  $graph->SetXDataLabelAngle(90);
  $graph->SetYLabel("humidity %");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0)),"");
  $graph->SetLegend(array("room","outside"));
  $graph->SetLegendPixels(50,10);
  $graph->DrawGraph();


$graph = new PHPlot(900,200,$dirname."pressure.png");
  $graph->SetDataValues($presstack);
$graph->SetIsInline(true);
 $graph->SetOutputFile($dirname."pressure.png");
  $graph->SetXDataLabelAngle(90);
  $graph->SetYLabel("pressure hpa");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0)),"");
  $graph->SetLegend(array("outside"));
  $graph->SetLegendPixels(70,10);
  $graph->DrawGraph();


$graph = new PHPlot(900,200,$dirname."wind.png");
  $graph->SetDataValues($windstack);
$graph->SetIsInline(true);
 $graph->SetOutputFile($dirname."wind.png");
  $graph->SetXDataLabelAngle(90);
  $graph->SetYLabel("wind k");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  //$graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0)),"");
  //$graph->SetLegend(array("room","outside","nc1","nc2"));
  $graph->DrawGraph();



$graph = new PHPlot(900,200,$dirname."rain.png");
  $graph->SetDataValues($rainstack);
$graph->SetIsInline(true);
 $graph->SetOutputFile($dirname."rain.png");
  $graph->SetXDataLabelAngle(90);
  $graph->SetYLabel("rain mm");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  //$graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0)),"");
  //$graph->SetLegend(array("outside"));
  $graph->DrawGraph();


?>
