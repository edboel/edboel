<?php
  include("phplot.php");
//  $timep = $_GET["timep"];
//  include("read_data.php");

$graph = new PHPlot(800,400);


$stack = array();
$xml = simplexml_load_file("fowsr.xml");

$count=0;
foreach ($xml->children() as $elmname => $child){
	$tempin="";
	$tempout="";
	foreach($child->children() as $cname => $cchild){
			
	  foreach($cchild->attributes() as $aname => $val){
	 	if($aname=="temp_in"){
			$tempin=floatval($val);
		}
		if($aname=="temp_out"){
			$tempout=floatval($val);
		}
	  }
		
		$count++; 
		//echo "$count $tempin $tempout <br>";
		array_push($stack, array($count,$tempin,$tempout,''));
	}
}

//$cend= count($mdata);
//$cbegin= $cend-336;


//$graph->SetDataValues($example_data);

  $graph->SetXDataLabelAngle(90);
  $graph->SetDataValues($stack);
  $graph->SetYLabel("Temp C");
  $graph->SetPlotType("lines");
  $graph->SetDrawXDataLabels(true);
  $graph->SetLineStyles("solid");
  $graph->SetDataColors(array("red","blue",array(00,166,00),array(166,166,0)),"");
  $graph->SetLegend(array("room","outside","nc1","nc2"));
  //$graph->SetLegendPixels(50,10);
  $graph->DrawGraph();
?>

