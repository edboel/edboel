
<html>
<br>
<h1> APRS weather station</h1>
<br>
edsard boelen, 4 march 2014<br>
<br>
<hr><i>
Since I got my <a href="../tncpi/">tnc pi</a> working and a <a href="../sensor6/">digital weather station</a> , 
then a nice next step would be to connect them together. 
xastir: the aprs program i use has support for a wide range of weather station types.
So it might not be difficult to send the data to xastir and let it sent to the 
Nearby APRS repeater / gateway.
</i><br>
<hr>
<br>
<img src="weather.png" /><br>
<br>
<h2>The data formats</h2>
This was the most difficult part. Xastir supports many station types. But it was very difficult to find any documentation on the format of any of those types.
Also other programs and tools were all easy to install and use, but I wanted just to know which digits to send over to xastir so I can convert my own log files to it.<br>
In the end I just downloaded the source code of xastir and found out that it was all nicely explained in the comments there...<br>
here is a snippet<br>
<table border=0 bgcolor=#dddddd><tr><td>
<font size=-1>
<pre>
	  else if (12 == sscanf((const char *)data,
                    "%f %f %f %f %f %f %d %d %f %f %f %f",
                    &tmp1,
                    &tmp2,
                    &tmp3,
                    &tmp4,
                    &tmp5,
                    &tmp6,
                    &tmp7,
                    &tmp8,
                    &tmp9,
                    &tmp10,
                    &tmp11,
                    &tmp12)) {
                dallas_type = 12;
            }
            else {
                fprintf(stderr,"wx_fill_data:sscanf parsing error\n");
            }


            // The format of the data originates here:
            // http://weather.henriksens.net/

            // tmp1: primary temp (C)
            // tmp2: temp max (C)
            // tmp3: temp min (C)
            // tmp4: anemometer (mps)
            // tmp5: anemometer gust (peak speed MS)
            // tmp6: anemometer speed max * 0.447040972 (max speed MS)
            // tmp7: vane bearing - 1 (current wind direction)
            // tmp8: vane mode (max dir)
            // tmp9: rain rate
            // tmp10: rain total today
            // tmp11: rain total week
            // tmp12: rain since month
            // tmp13: Current Humidity
            // tmp14: Max Humidity
            // tmp15: Min Humidity
            // tmp16: Current Barometer
            // tmp17: Max Barometer
            // tmp18: Min Barometer
            // tmp19: Barometer Rate


</pre>
</font></td></tr></table>
<br>
So what this means is that I can send the station data in a long line of numbers:<br>
 &nbsp; &nbsp; 0.1 0.2 0.3 0.4 0.5 0.6 7 8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9<br>
 <br>
My weather station logs are written by fowsr in a xml format. I don't care, just read the last line and get the substrings from the right positions. No xml parsing done.<br>
<br>
<h2> APRS-IS data format </h2>
It is also possible to send the weather packets directly to the APRS-IS servers.
Then I can just run a daemon which sends the packet once an hour.<br>


<br>
The APRS WX format is as described on <a href="http://www.aprs.net/vm/DOS/WX.HTM">wx.htm</a><br> 
<br>
&nbsp; _X/XXX/gXXXtXXXrXXXpXXXPXXXhXXbXXXXX%type<br>
<br>
_ is begin wx and wind direction<br>
/ is 1 minute speed<br>
g is gust (5 minute wind speed)<br>
t is temp (IN FAHRENHEIT)<br>
r is rain last hour<br>
p is precip last 24h<br>
P is precip since midnight<br>
h is humidity<br>
b is baro<br> 
% is software type<br>
<br>
here is one example line :<br>
PD1CC>APX200,WIDE2-1,qAU,PD1CF:=5214.9 N/00608.4 E_360/000g000t056r000P000p000h53b0000XOWW<br>
PD1CC>APRS,TCPIP*:@181811z5214.91N00616.14E_360/000g000t056r000P000p000h81b0000XOWW<br>
the qAU and ,PD1CC are server generated q constructs and should not be sent by the client (unless it is an igate)<br>
<br>
You can send data to aprs-is by opening a telnet session and login with user ... pass ...<br>


<h2>The server program for APRS-IS</h2>
This program will read my weather station and sends it via internet to APRS-IS for CWOP.<br>
As specied on the site aprs2.net and aprs-is.net a socket connection to port 14580 on one of the load balancers is enough.
My program will open a data stream socket, send a login and a password.<br>
The login is my callsign, the password is just the callsign hashed by 73!
The server will then respond with verified and its name.<br>
Then I can send APRS data to it. Which then will be transmitted to all other sites and receivers.<br>
I will send just one line with the weather and then close the connection.<br>
After that I can view the raw packet in for example one of the server logs.<br>
<br>
get the code <a href="isclient.c">here</a><br>
<br>	
<h2>The server program for xastir</h2>
This program is to send weather data via the radio using xastir<br>
The result is as follows:<br>
I have written a small server program which reads the latest fowsr log 
converts it to a string with 19 numbers and hosts it at port 3490.<br>
get the code <a href="xastwserv.c">here</a><br>
<br>
<br>
<h2>configuring xastir</h2>
First set up the interface:<br>
<img src="interface.png" /><br>
After bringing it UP, you can check if it is working by selecting view-> own weather<br>
<img src="own.png" /><br>
It might take a couple of seconds to sync.<br>
Then set in File->configure->defaults the reporting to station / WS<br>
<img src="defaults.png" /><br>
You will see the weather info appear on the world map<br>
<img src="xastir1.png" /><br>
And we are done!, below you see it on my raspberry pi with the TNC and real data. The Nearby Igate from Deventer puts it online so it appears on <a href="http://aprs.fi">aprs.fi</a><br>
<img src="resultshot.png" /><br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</html>

