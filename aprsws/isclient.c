/*isclient.c - a aprs-is client
e.boelen 20-03-2014 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
/* the port users will be connecting to */
#define MYPORT 14580
#define HOSTNAME "euro.aprs2.net"
#define DATFILE "/tmp/fowsr.xml"

struct measurement{
  char *tempout;
  char *humout;
  char *press;
  char *windav;
  char *windgus;
  char *winddir;
  char *rainhour;
  char *rainday;
  
};

char *substring(char *string, int position, int length) 
{
   char *pointer;
   int c;
 
   pointer = malloc(length+1);
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(EXIT_FAILURE);
   }
   for (c = 0 ; c < position -1 ; c++) 
      string++; 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *string;      
      string++;   
   }
   *(pointer+c) = '\0';
   return pointer;
}

int isLastQuote(char *str)
{
  size_t len = strlen(str);
  if(*str && str[strlen(str + 1)] == '"'){
    memmove(str, str+1, len-2);
    str[len-2] = 0;
    return 1;
  }else{
    return 0;
  }
}

struct measurement readHumidity(){
   FILE *in;
   int max_len = 250 ;
   char buf[max_len + 1], *tempout, *humout;
   struct measurement reading;
   
   in = fopen(DATFILE, "r");
      /* always check return of fopen */
      if (in == NULL) {
        perror("fopen datfile");
        exit(-1);
      } 
     // get some rain values last day is 85 times back: 
     fseek(in, -(max_len*42), SEEK_END);
     fread(buf, max_len-1, 1, in);
     char *pch = strstr(buf, "rain");
     float frainday = strtof(substring(pch,7,5),NULL);
     
     //last hour is 500 char back
     fseek(in, -(max_len*2), SEEK_END);
     fread(buf, max_len-1, 1, in);
      pch = strstr(buf, "rain");
     float frainhour = strtof(substring(pch,7,5),NULL);
     fseek(in, -max_len, SEEK_END);
     fread(buf, max_len-1, 1, in);
     fclose(in);
     
     pch = strstr(buf, "temp_out");
     reading.tempout = substring(pch,11,4);
     pch = strstr(buf, "hum_out");
     reading.humout = substring(pch,10,4);
     pch = strstr(buf, "pressure");
     reading.press = substring(pch,11,4);
     pch = strstr(buf, "wind_ave");
     reading.windav = substring(pch,11,4);
     pch = strstr(buf, "wind_gust");
     reading.windgus = substring(pch,12,4);
     pch = strstr(buf, "wind_dir");
     reading.winddir = substring(pch,11,4);
     pch = strstr(buf, "rain");
     float fraincur = strtof(substring(pch,7,5),NULL);
     reading.rainday = substring(pch,7,4);
     reading.rainhour = substring(pch,7,4);
          
     // convert celcius to fahrenheit:  C*1.8 + 32
     float tmp = strtof(reading.tempout,NULL);
     tmp = tmp * 1.8 + 32;
     if(tmp < 100){
      sprintf(reading.tempout,"0%2.0f",tmp);
     }else{
       sprintf(reading.tempout,"%3.0f",tmp);
     }
     //humidity always 2 digits
     tmp = strtof(reading.humout,NULL);
     sprintf(reading.humout,"%2.0f",tmp);
     //pressure
     tmp=strtof(reading.press,NULL);
     if(tmp < 1000){
       sprintf(reading.press,"0%3.0f",tmp);
     }else{
       sprintf(reading.press,"%4.0f",tmp);
     } 
     if(isLastQuote(reading.windav)){
       //aha
     }
     tmp=strtof(reading.windav,NULL);
     if(tmp < 10){
      sprintf(reading.windav,"00%1.0f",tmp);
     }else{
       sprintf(reading.windav,"0%2.0f",tmp);
     }
      
     if(isLastQuote(reading.windgus)){
       //aha
     }
     tmp=strtof(reading.windgus,NULL);
     if(tmp < 10){
      sprintf(reading.windgus,"00%1.0f",tmp);
     }else{
       sprintf(reading.windgus,"0%2.0f",tmp);
     }
     
    if(isLastQuote(reading.winddir)){
       //aha
     }
     tmp=strtof(reading.winddir,NULL);
     if(tmp < 10){
      sprintf(reading.winddir,"360");
     }else if(tmp < 100){
       sprintf(reading.winddir,"0%2.0f",tmp);
     }else{
       sprintf(reading.winddir,"%3.0f",tmp);
     }

     frainday = fraincur-frainday;
     frainhour = fraincur-frainhour;
     
     if(frainhour<10.0){
       sprintf(reading.rainhour,"00%1.0f",frainhour);
     }else {
       sprintf(reading.rainhour,"0%2.0f",frainhour);
     }

    
     if(frainday<10.0){
       sprintf(reading.rainday,"00%1.0f",frainday);
     }else {
       sprintf(reading.rainday,"0%2.0f",frainday);
     }
     
     printf("capture: t[%s] h[%s] p[%s] a[%s] g[%s] d[%s] rh[%s] rd[%s]\n", reading.tempout, reading.humout, reading.press, reading.windav, reading.windgus, reading.winddir, reading.rainhour, reading.rainday);
       
     
    return reading;
}


 
int main(int argc, char *argv[ ])
{
int sockfd;
/* connector’s address information */
struct sockaddr_in their_addr;
struct hostent *he;
struct timeval timev;
struct measurement readings;
int numbytes;
char login[100];
char readbuf[255];

if (argc != 3)
{
fprintf(stderr, "Client-Usage: %s user pass\n", argv[0]);
exit(1);
}
 
/* get the host info */
if ((he = gethostbyname(HOSTNAME)) == NULL)
{
perror("Client-gethostbyname() error :( ");
exit(1);
}
else
printf("Client-gethostname() is OK...\n");

// set timout
timev.tv_sec=3;
timev.tv_usec=0;

setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timev,sizeof(struct timeval));


if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
{
perror("Client-socket() error :( ");
exit(1);
}
else
printf("Client-socket() sockfd is OK...\n");
 
/* host byte order */
their_addr.sin_family = AF_INET;
/* short, network byte order */
printf("Using port: %d\n",MYPORT);
their_addr.sin_port = htons(MYPORT);
their_addr.sin_addr = *((struct in_addr *)he->h_addr);
/* zero the rest of the struct */
memset(&(their_addr.sin_zero), '\0', 8);
 

if(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1)
{
    perror("connect()");
    exit(1);
}
else
    printf("Client-The connect() is OK...\n");

if((numbytes = recv(sockfd, readbuf, 255-1, 0)) == -1)
{
    perror("recv()");
    exit(1);
}
readbuf[numbytes] = '\0';
printf("Client-Received 1: %s", readbuf);
if(readbuf[0]=='j'){
	printf("java client found, retry other server");
	if (close(sockfd) != 0)
		printf("socket close failed");
	sleep(10);
	if(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1)
	{
    		perror("connect()");
    		exit(1);
	}
	if((numbytes = recv(sockfd, readbuf, 255-1, 0)) == -1)
	{
    		perror("recv()");
    		exit(1);
	}
	readbuf[numbytes] = '\0';
	printf("Client-Received new: %s", readbuf);


}
sprintf(login,"user %s pass %s vers isclient \n",argv[1],argv[2]);


if((numbytes = sendto(sockfd, login, strlen(login), 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1)
{
perror("Client-sendto() error :( ");
exit(1);
}
else
printf("Client-send login...\n");


if((numbytes = recv(sockfd, readbuf, 255-1, 0)) == -1)
{
    perror("recv()");
    exit(1);
}
readbuf[numbytes] = '\0';
printf("Client-Received 2: %s", readbuf);
sleep(1);

printf("Client-send weather data...\n");
printf("sent %d bytes to %s\n", numbytes, inet_ntoa(their_addr.sin_addr));

readings = readHumidity();

//char testline[] = "PD1CC>APRS,TCPIP*:=5214.91N/00616.14E_360/000g000t056r000P000p000h81b0000XOWW\n";
char line[255];

sprintf(line,"%s>APRS,TCPIP*:=5214.91N/00608.41E_%s/%sg%st%sr%sP%sp%sh%sb%s0XOWW\n",argv[1],readings.winddir,readings.windgus,readings.windgus,readings.tempout,readings.rainhour,readings.rainday,readings.rainday,readings.humout,readings.press);

printf("sending: %s \n",line);

if((numbytes = sendto(sockfd, line, strlen(line), 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1)
{
perror("Client-sendto() error :( ");
exit(1);
}
else
printf("Client-send data...\n");


printf("done...\n");

if (close(sockfd) != 0)
printf("Client-sockfd closing is failed!\n");
else
printf("Client-sockfd successfully closed!\n");
return 0;
}

 
