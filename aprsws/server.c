/* serverprog.c - a stream socket server demo */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdbool.h>

/* the port users will be connecting to */
#define MYPORT 3490
/* how many pending connections queue will hold */
#define BACKLOG 10
#define datfile "/var/fowsr.xml"

 
void sigchld_handler(int s)
{
    while(wait(NULL) > 0);
}

char *substring(char *string, int position, int length) 
{
   char *pointer;
   int c;
 
   pointer = malloc(length+1);
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(EXIT_FAILURE);
   }
   for (c = 0 ; c < position -1 ; c++) 
      string++; 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *string;      
      string++;   
   }
   *(pointer+c) = '\0';
   return pointer;
}

char *readHumidity(){
   FILE *in;
   int max_len = 210 + 10;
   char buf[max_len + 1], *tempout, *humout;
   
   in = fopen(datfile, "r");
      /* always check return of fopen */
      if (in == NULL) {
        perror("fopen datfile");
        exit(-1);
      } 
     fseek(in, -max_len, SEEK_END);
     fread(buf, max_len-1, 1, in);
     //ssize_t len = read(in, buf, max_len);
     fclose(in);
     //char *last_newline = strrchr(buf, '\n');
     //char *last_line = last_newline-1;
     char *pch = strstr(buf, "temp_out");
     tempout = substring(pch,11,4);
     pch = strstr(buf, "hum_out");
     humout = substring(pch,10,4);
     printf("capture: [%s] [%s]\n", tempout, humout);
     // The format of the data originates here:
            // http://weather.henriksens.net/

            // tmp1: primary temp (C)
            // tmp2: temp max (C)
            // tmp3: temp min (C)
            // tmp4: anemometer (mps)
            // tmp5: anemometer gust (peak speed MS)
            // tmp6: anemometer speed max * 0.447040972 (max speed MS)
            // tmp7: vane bearing - 1 (current wind direction)
            // tmp8: vane mode (max dir)
            // tmp9: rain rate
            // tmp10: rain total today
            // tmp11: rain total week
            // tmp12: rain since month
            // tmp13: Current Humidity
            // tmp14: Max Humidity
            // tmp15: Min Humidity
            // tmp16: Current Barometer
            // tmp17: Max Barometer
            // tmp18: Min Barometer
            // tmp19: Barometer Rate
     //char *result;
     sprintf(buf,"%s %s %s 0.0 0.0 0.0 16 0 0.0 0.0 0.0 0.0 %s %s %s 0.0 0.0 0.0 0.0\n",tempout,tempout,tempout,humout,humout,humout);
     //printf("%s",buf);
     //printf(" len = %d \n",strlen(buf));
    return strdup(buf);
}
 
int main(int argc, char *argv[ ])
{
/* listen on sock_fd, new connection on new_fd */
int sockfd, new_fd;
/* my address information */
struct sockaddr_in my_addr;
/* connector’s address information */
struct sockaddr_in their_addr;
int sin_size;
struct sigaction sa;
int yes = 1;
 
if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
{
    perror("Server-socket() error lol!");
    exit(1);
}
else
printf("Server-socket() sockfd is OK...\n");
 
if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
{
    perror("Server-setsockopt() error lol!");
    exit(1);
}
else
    printf("Server-setsockopt is OK...\n");
 
/* host byte order */
my_addr.sin_family = AF_INET;
/* short, network byte order */
my_addr.sin_port = htons(MYPORT);
/* automatically fill with my IP */
my_addr.sin_addr.s_addr = INADDR_ANY;
 
printf("Server-Using %s and port %d...\n", inet_ntoa(my_addr.sin_addr), MYPORT);
 
/* zero the rest of the struct */
memset(&(my_addr.sin_zero), '\0', 8);
 
if(bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1)
{
    perror("Server-bind() error");
    exit(1);
}
else
    printf("Server-bind() is OK...\n");
 
if(listen(sockfd, BACKLOG) == -1)
{
    perror("Server-listen() error");
    exit(1);
}
printf("Server-listen() is OK...Listening...\n");
 
/* clean all the dead processes */
sa.sa_handler = sigchld_handler;
sigemptyset(&sa.sa_mask);
sa.sa_flags = SA_RESTART;
 
if(sigaction(SIGCHLD, &sa, NULL) == -1)
{
    perror("Server-sigaction() error");
    exit(1);
}
else
    printf("Server-sigaction() is OK...\n");
 
/* accept() loop */
while(1)
{
sin_size = sizeof(struct sockaddr_in);
if((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size)) == -1)
{
    perror("Server-accept() error");
    continue;
}
else
    printf("Server-accept() is OK...\n");
printf("Server-new socket, new_fd is OK...\n");
printf("Server: Got connection from %s\n", inet_ntoa(their_addr.sin_addr));
 
/* this is the child process */
if(!fork())
{
    /* child doesn’t need the listener */
   close(sockfd);
   bool contins = true;
   char *hup;
   while(contins){

     hup = readHumidity();

     if(send(new_fd, hup, strlen(hup), 0) == -1){
        perror("Server-send() error lol!");
	contins = false;
      }
      sleep(10);
   }
   close(new_fd);
   exit(0);
}
else
    printf("Server-send is OK...!\n");
 
/* parent doesn’t need this*/
close(new_fd);
printf("Server-new socket, new_fd closed successfully...\n");
}
return 0;
}
