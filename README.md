projects
========

some of my home projects

|  name  | date  | description  |
|---|---|---|
| alarmsys        |   | arduino alarm system  |
| amplifier       |   | tda7294 amplifier  |
| amplifier2      |   | a better tda 7294 amplifier |
| aprsws          |   | APRS Weather station  |
| avr             |   | some atmega avr projects  |
| christmaslights |   | usb christmaslights  |
| clock           |   | binary clock  |
| dekai           | 2005  | installation of usb stick OS  |
| display         |   |   |
| endfed          |   | 10m 20m 40m endfed antenna  |
| energy          |   | energy meter  |
| greenhouse      | 2016 | computer controlled greenhouse  |
| iface           |   |   |
| morse           |   | morse decoder on 3 different microcontrollers  |
| movieloader     |   |   |
| nodemcu         |   | some ESP8266 nodemcu projects  |
| phplot          |   |   |
| printer         |   | reprap 3d printer  |
| raspberry       |   | some raspberry pi projects  |
| rfhandler       |   | 433MHz remote controlled lights tranceiver  |
| robot           |   | robot attempt  |
| robot2          |   | robot attempt 2  |
| roomlight       |   | rgb controller 1  |
| roomlight2      |   | rgb controller 2  |
| roomlight3      |   | rgb controller 3  |
| sdr             |   | rtl usb sdr test  |
| sensor          |   | temperature sensor  |
| sensor2         |   | temperature sensor  |
| sensor3         |   | temperature sensor  |
| sensor4         |   | temperature sensor  |
| sensor5         |   | temperature sensor  |
| sensor6         |   | temperature sensor  |
| senswi          |   | wireless temperature sensor  |
| solar           |   | solar panel controller  |
| sstv            |   | raspberry pi slow scan television  |
| switch          |   | computer controlled switch  |
| switch2         |   | computer controlled switch  |
| swr             |   | swr meter  |
| tncpi           |   | raspberry pi TNC  |
| usbkey          |   |   |
                 
                 