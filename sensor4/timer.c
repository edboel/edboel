/*
 * avr-gcc -mmcu=atmega88 -Os -o timer1.elf timer1.c 
 * avr-objcopy -j .text -O ihex timer1.elf timer1.hex
 * sudo avrdude -c usbasp -p m88 -U flash:w:inter2.hex:i -U hfuse:w:0xdf:m -U lftimer1.hex:i
 */
#define F_CPU 10000000UL  /* 10 MHz CPU clock */

#include<util/delay.h>
#include<avr/io.h>
#include<avr/interrupt.h>

uint8_t led;
int main( void )
{
     DDRC |=(0xFF); /* C output */
         PORTC |=(0xFF); /* C pins hi */
 
     TCNT0=0x00; /* start value of T/C0 */
     TCCR0A=0x00; /* normal mode, no pwm */ 
     TCCR0B=0x05; /* prescale ck/1024 */
     led = 0;
     for (;;)
     {
          // this while-loop checks the overflow bit
          // in the TIFR register
          //while (TIFR0 != 0x01) {
        while (TCNT0 < 0xD0){
        }
          led++;
          if (led == 31){
               PORTC ^= _BV(PC2); // toggle led 
                led=0;
        }
         TCNT0=0x00; 
     }
}


