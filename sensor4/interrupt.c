/*
 *   ** interupt.c test program to check how interrupts work
 *   avr-gcc -mmcu=atmega88 -Os -o inter.elf inter.c
 *   avr-objcopy -j .text -O ihex inter.elf inter.hex 
 *  sudo avrdude -c usbasp -p m88 -U flash:w:inter2.hex:i -U hfuse:w:0xdf:m -U lfuse:w:0x7f:m
 */


#define F_CPU 8000000UL  /* 8 MHz CPU clock */

#include<util/delay.h>
#include<avr/io.h>
#include<avr/interrupt.h>


//uint8_t led;
volatile int led;

ISR(INT0_vect){
        PORTC ^= _BV(PC2);
        if(led==0){
                led=1;
        }else{
                led=0;
        }
}

int main(void)
{
        DDRC |=(0xFF); /* C output */
        DDRD &=(0x00); /*D input */
        PORTC |=(0xFF); /* C pins hi */
        PORTD |=(0x00); /* D pull up off */
//      PORTD = 0b11110000;
        
 //     PCICR =0x01; // interrupt enable??
 //     PCMSK0 = 0x07;
        EICRA=0x1;      
        EIMSK=0x1;

        sei();
        led=0;  
        while (1) {
             if (bit_is_clear(PIND, PD0))
              {
                     _delay_ms(25);
                     if (bit_is_clear(PIND, PD0))
                     {
                       PORTC ^= _BV(PC0);
                      }
                      _delay_ms(250);
             }

            if(led==0){
                PORTC |= _BV(PC1);
            }else{
                 PORTC &= ~_BV(PC1);    
            }
       }
}

