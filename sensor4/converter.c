/* Name: converer.c
 * Project:  convert data pulses from freq counter and send to PC
 * Author: Edsard Boelen
 * Creation Date: 2009-12-12
 * Copyright: (c) 2009 Edsard Boelen
 * License: GNU GPL v2 (see License.txt) 
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "usbdrv.h"
#include "oddebug.h"
#include <util/delay.h>

/*

build with: make
or buid with
avr-gcc -Wall -Os -Iusbdrv -I. -mmcu=atmega88  -o converter.bin usbdrv/usbdrv.o usbdrv/usbdrvasm.o usbdrv/oddebug.o converter.c
write with: sudo avrdude -p atmega88 -c bsd -U flash:w:main.hex:i -U lfuse:w:0xff:m -U hfuse:w:0xdf:m
or
sudo avrdude -c usbasp -p m88 -U flash:w:converter.hex:i
*/

uchar ledstatus=0;
uchar twinkle=0x00;
volatile uint8_t puls=0;
volatile uint8_t tel=0;

/* We poll for the timer interrupt instead of declaring an interrupt handler
 * with global interrupts enabled. This saves recursion depth. Our timer does
 * not need high precision and does not run at a high rate anyway.
 */
static void timerInterrupt(void)
{
	if (bit_is_clear(PINC, PC5))
        {
		if(bit_is_clear(PINC,PC4)){
			if(puls==0){
			 	ledstatus++;	
				twinkle++;
				puls=1;
			}

		}else{
			twinkle=0;
			puls=0;
		}
		tel=0;
		
	}else{
		tel++;
		puls=0;

	}
		if(tel > 20){
			tel=0;	
			ledstatus=0;	
		        twinkle=0x00;	
			puls=0;
		}
	

}

USB_PUBLIC uchar usbFunctionSetup(uchar data[8])
{
usbRequest_t    *rq = (void *)data;
uchar           status = ledstatus;
static uchar    replyBuf[2];

    usbMsgPtr = replyBuf;
    if(rq->bRequest == 0){  /* ECHO */
        replyBuf[0] = rq->wValue.bytes[0];
        replyBuf[1] = rq->wValue.bytes[1];
	return 2;
    }
    if(rq->bRequest == 1){  /* GET_STATUS -> result = 2 bytes */
        replyBuf[0] = status;
        replyBuf[1] = twinkle;
	return 2;
    }
    if(rq->bRequest == 2 || rq->bRequest == 3){ /* SWITCH_ON or SWITCH_OFF, index = bit number */
        uchar bit = rq->wIndex.bytes[0] & 7;
        uchar mask = 1 << bit;
        uchar needChange, isOn = status & mask;
	if(rq->bRequest == 2){  /* SWITCH_ON */
            status |= mask;
            needChange = !isOn;
        }else{              /* SWITCH_OFF */
            status &= ~mask;
            needChange = isOn;
        }
        if(rq->wValue.bytes[0] == 0){   /* duration == 0 -> permanent switch */
            ledstatus = status;
        }else if(needChange){   /* temporary switch: value = duration in 200ms units */
            ledstatus = status;//actionTimers[bit] = rq->wValue.bytes[0];
        }
    }
    if(rq->bRequest == 4){  /* turn twinkle on*/
	ledstatus= 0xAA;
        twinkle=50;
    }
    return 0;
}

/* allow some inter-device compatibility */
#if !defined TCCR0 && defined TCCR0B
#define TCCR0   TCCR0B
#endif
#if !defined TIFR && defined TIFR0
#define TIFR    TIFR0
#endif

int main(void)
{
uchar   i;

    wdt_enable(WDTO_1S);
    odDebugInit();
    DDRD = ~(1 << 2);   /* all outputs except PD2 = INT0 */
    PORTC = 0xFF;  /* internall pullup */ 
    DDRC = 0;  /* portc all inputs  */
    PORTD = 0;
    PORTB = 0;          /* no pullups on USB pins */
/* We fake an USB disconnect by pulling D+ and D- to 0 during reset. This is
 * necessary if we had a watchdog reset or brownout reset to notify the host
 * that it should re-enumerate the device. Otherwise the host's and device's
 * concept of the device-ID would be out of sync.
 */
    DDRB = ~USBMASK;    /* set all pins as outputs except USB */
    ledstatus= 0xAA;
    usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
    i = 0;
    while(--i){         /* fake USB disconnect for > 500 ms */
        wdt_reset();
        _delay_ms(2);
    }
    usbDeviceConnect();
    TCCR0 = 5;          /* set prescaler to 1/1024 */
    usbInit();
    sei();
    for(;;){    /* main event loop */
        wdt_reset();
        usbPoll();
        if(TIFR & (1 << TOV0)){
            TIFR |= 1 << TOV0;  /* clear pending flag */
            timerInterrupt();
        }
    }
    return 0;
}


