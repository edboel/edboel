/*
* hum2.c  13 dec 2009
* a program which converts he frequemcy to a decimal value, 
* and sends it via an own protocol .
* avr-gcc -mmcu=atmega88 -Os -o hum2.elf hum2.c 
* avr-objcopy -j .text -O ihex hum2.elf hum2.hex
* sudo avrdude -c usbasp -p m88 -U flash:w:hum2.hex:i -U hfuse:w:0xdf:m 
*/

#define F_CPU 10000000UL  /* 10 MHz CPU clock */
#define DELAYT 50 /* clock time is 4x delay */

#include<util/delay.h>
#include<avr/io.h>
#include<avr/interrupt.h>

uint8_t led;
volatile long pcount;



ISR(INT0_vect){
        //PORTC ^= _BV(PC2);
	pcount++;
}


void startpulse(void){
	 PORTC = 0xFF;

	_delay_ms(DELAYT);	
	PORTC &= ~_BV(PC5);	
	_delay_ms(DELAYT);
	_delay_ms(DELAYT);
	PORTC |= _BV(PC5);
	_delay_ms(DELAYT);
	 PORTC &= ~_BV(PC4);
	 _delay_ms(DELAYT);

	int i=0;
	for(i=0;i<9;i++){
		PORTC &= ~_BV(PC5);
        	_delay_ms(DELAYT);
        	_delay_ms(DELAYT);
        	PORTC |= _BV(PC5);
		_delay_ms(DELAYT);
        	_delay_ms(DELAYT);
	}
	
	 PORTC &= ~_BV(PC5);
         _delay_ms(DELAYT);
         _delay_ms(DELAYT);
         PORTC |= _BV(PC5);
         _delay_ms(DELAYT);
	 PORTC |= _BV(PC4);
	 _delay_ms(DELAYT);
	 PORTC &= ~_BV(PC5);
        _delay_ms(DELAYT);
        _delay_ms(DELAYT);
         PORTC |= _BV(PC5);	
	_delay_ms(DELAYT);
}

void sendpulse(int val){
	startpulse();	
	int i=0;
	PORTC &= ~_BV(PC4);
	for(i=0;i<val;i++){
		_delay_ms(DELAYT);
		 PORTC &= ~_BV(PC5);
                _delay_ms(DELAYT);
                _delay_ms(DELAYT);
                PORTC |= _BV(PC5);
                _delay_ms(DELAYT);

	}
	 PORTC |= _BV(PC4);
}


int main( void )
{
     DDRC |=(0xFF); /* C output */
	 PORTC |=(0xFF); /* C pins hi */

     // init timer 
     TCNT0=0x00; /* start value of T/C0 */
     TCCR0A=0x00; /* normal mode, no pwm */ 
     TCCR0B=0x05; /* prescale ck/1024 */
     // init interrupt 
     EICRA=0x2;      
     EIMSK=0x1;
     sei();

     led=0;
     pcount=0; 
     for (;;)
     {
          // this while-loop checks the overflow bit
          // in the TIFR register
          //while (TIFR0 != 0x01) {
	while (TCNT0 < 0xF0){
	}
          led++;
          if (led == 32){
               PORTC = 0xFF;
               
		pcount = pcount -2000;
	 	if(pcount <100 || pcount > 1600){

			 PORTC &= ~_BV(PC2);

		}else{

			 PORTC |= _BV(PC2);
		}	
		int valc = pcount/100;
	        if(valc > 1 && valc < 16){ 
			PORTC |= _BV(PC3);	
			sendpulse(valc);
	       	}else{
			if(valc <1){
				sendpulse(3);
			}else{
				sendpulse(7);
			}
			 PORTC &= ~_BV(PC3);
		}
		valc=valc*100; 
		int vald = pcount%100;
		vald=vald/10;
		if(vald < 11){	
			sendpulse(vald);
		}	
		_delay_ms(2000);		

 
		pcount=0;	
		led=0;
	   }
         TCNT0=0x00; 
     }
}

