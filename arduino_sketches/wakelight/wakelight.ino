#include <Wire.h>  // Comes with Arduino IDE
#include <LiquidCrystal_I2C.h>
#include <Encoder.h>
#include "RTClib.h"

#include <TM1637Display.h>

#define CLK 13
#define DIO 12


Encoder myEnc(2, 3);

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
TM1637Display display(CLK, DIO);

int count =1;
int tcount =1;
long oldPosition  = -999;
const int led = 5;
const int led2 = 6;
const int offPin = 9;
const int modePin = 8;
const int alarmPin = 11;

boolean alarmOn = true;
int wakeState = 0;
int wakeHour=6;
int wakeMinute=45;
int mode = 0;
int lightState = 255;

int digitBrightness = 9;

RTC_DS1307 rtc;




void setup()
{
  // initialize the digital pin as an output.
  pinMode(offPin, INPUT);
  pinMode(modePin, INPUT);
  
  pinMode(led, OUTPUT); 
  analogWrite(led, 255);
  pinMode(led2, OUTPUT); 
  analogWrite(led2, 255);  
  display.setBrightness(digitBrightness);
  
  pinMode(alarmPin, OUTPUT); 
  digitalWrite(alarmPin,HIGH);
  
   Wire.begin();
  //myEnc.write(255);
  //rtc.adjust(DateTime(2015, 6, 10, 20, 40, 0));
  lcd.begin(20,4);   // initialize the lcd for 16 chars 2 lines, turn on backlight

  lcd.backlight(); // finish with backlight on  
  lcd.setCursor(0,0); //Start at character 4 on line 0
  lcd.print("Hello, world!");
  delay(1000);
  lcd.setBacklight(0);
  
}

void loop()
{
  boolean result = checkButtons();
  if(result){
    lcd.backlight();
    count = 15000;
  }

  if(tcount < 5){
    printTime();
    printWakeTime();
    
    tcount=(100);
  }
  
  
  delay(2);
  
  if(count > 1){
   count--; 
   tcount--;
    delay(2);
    checkAlarm();
    setDisplay();
  }else{
    delay(2);
    checkAlarm();
    setDisplay();
    if(count == 1){
      lcd.setBacklight(0);
      lcd.clear();
      mode = 0;
      count = 0;
    }
   }
  
}

void setDisplay(){
  DateTime now = rtc.now();
  int k = 0;
  
  k=now.hour()*100;
  k=k+now.minute();
  display.showNumberDec(k, true);
  
}

void checkAlarm(){
  if(alarmOn && wakeState == 0){
    DateTime now = rtc.now();
    if(now.hour() == wakeHour && now.minute() == wakeMinute){
      lcd.setCursor(9,1);
      lcd.print("ALARM!");
      wakeState = 250;
    }
  }else{ 
    if(wakeState > 0){
      wakeState--;
      lcd.setCursor(9,1);
      lcd.print("ALARM!");
      lcd.print(wakeState);
      if(wakeState < 3 && lightState > 100){
        lightState--;
        // set lightstate
        analogWrite(led, lightState);
        if(lightState < 220){
          analogWrite(led2, lightState+30);
        }else{
          analogWrite(led2,255);
        }
        wakeState = 25;
      }
    }
  }
}  

boolean checkButtons(){
  long newPosition = myEnc.read();
  boolean positionUp = false;
  if (newPosition != oldPosition) {
    if(newPosition > oldPosition){
      positionUp = true;
    }
    if(mode != 0){
    lcd.setCursor(0,1);
      lcd.print("mode: ");
      lcd.print(mode);
    }
    switch(mode){
        case 0:
          setLight(positionUp);
          break;
        case 1:
          setLight(positionUp);
          break;
        case 2:
          if(alarmOn){
            alarmOn = false;
            digitalWrite(alarmPin,LOW);
          }else{
            alarmOn = true;
            digitalWrite(alarmPin,HIGH);
          }
          printWakeTime();
           ///lcd.print("set alarm on");
         break;
        case 3:
          alarmOn = true;
         if(positionUp){
           wakeHour++;
         }else{
           wakeHour--;
         }
         printWakeTime();
        break; 
        case 4:
         alarmOn = true;
         if(positionUp){
           wakeMinute++;
         }else{
           wakeMinute--;
         }
         printWakeTime();
        break; 
        case 5:
          
          if(positionUp){
            adjustTime(1,0);
            }else{
            adjustTime(-1,0);
            }
        break; 
        case 6:
         if(positionUp){
            adjustTime(0,1);
            }else{
            adjustTime(0,-1);
            }
        break;
        case 7:
         if(positionUp){
           digitBrightness++;
         }else{
           digitBrightness--;
         }
         display.setBrightness(digitBrightness);
        break;
         case 8:
         lcd.print("                   ");
         mode = 0;
        break;
    }
    oldPosition = newPosition;
    
    return true;
  }
  
  
  
  int buttonState = 0;
  //for(int i=0;i < attempts;i++){
    buttonState = digitalRead(offPin);
    if(buttonState == HIGH){
      lightState = 255;
      analogWrite(led2, 255);
      analogWrite(led, 255);
      lcd.setBacklight(0);
      lcd.clear();
      mode = 0;
      //only disable alarm during alarm
      if(wakeState > 0){
        alarmOn = false;
      }
      wakeState = 0;
      
      return false;
    }
    
    buttonState = digitalRead(modePin);
    if(buttonState == HIGH){
      lcd.setCursor(0,1);
      mode++;
      lcd.print("mode: ");
      lcd.print(mode);
      lcd.setCursor(0,2);
      switch(mode){
        case 0:
        case 1:
           lcd.print("set light      ");
         break;
        case 2:
           lcd.print("set alarm on   ");
         break;
        case 3:
         lcd.print("set alarm hour    ");
        break; 
        case 4:
         lcd.print("set alarm minute  ");
        break; 
        case 5:
         lcd.print("set current hour  ");
        break; 
        case 6:
         lcd.print("set current minute");
        break;
         case 7:
         lcd.print("set brightness    ");
        break;
        case 8:
         lcd.print("                   ");
         mode = 0;
        break;
        
      }
      
      
      
      delay(200);
      return true;
    }
    
  
  return false;
}

void adjustTime(int thour, int tminute){
     DateTime tnow = rtc.now(); 
     rtc.adjust(DateTime(tnow.year(), tnow.month(), tnow.day(), tnow.hour()+thour, tnow.minute()+tminute, 0));
     printTime();
     delay(500);       
  }
  
void setLight(boolean positionUp){
    if(positionUp){
      lightState--;
    }else{
      lightState++;
    }
  
    if(lightState < 50){
      lightState = 50;
    } 
    if(lightState > 255){
      lightState = 255;
    }
    
    // print lightstate
    lcd.setCursor(0,3);
    lcd.print("   ");
    lcd.setCursor(0,3);
    lcd.print(255 - lightState);
    
    // set lightstate
    analogWrite(led, lightState);
    if(lightState < 220){
      analogWrite(led2, lightState+30);
    }else{
      analogWrite(led2,255);
    }
  
}

void printTime(){
    DateTime now = rtc.now();
    lcd.setCursor(0,0);
    lcd.print("    ");
    lcd.print(now.hour());
    lcd.print(":");
    lcd.print(now.minute());
    lcd.print(":");
    lcd.print(now.second());
    lcd.print("   ");
}

void printWakeTime(){
    lcd.setCursor(6,3);
    if(alarmOn == false){
      lcd.print("alarm off    ");
    }else{
      if(wakeHour < 0 || wakeHour > 23){
        wakeHour = 0;
      }
      if(wakeMinute < 0 || wakeMinute > 59){
        wakeMinute = 0;
      }
      
      lcd.print("wake on: ");
      lcd.print(wakeHour);
      lcd.print(":");
      lcd.print(wakeMinute);
      lcd.print("  ");
    }
}


