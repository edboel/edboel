#include <SoftwareSerial.h>
#define DST_IP "192.168.1.12"
SoftwareSerial mySerial(5, 6);

int measTempCount;
char inData[255]; // Allocate some space for the string
char inChar=-1; // Where to store the character read
byte index = 0;

void setup()  
{
  pinMode(13, OUTPUT);     
  Serial.begin(9600);

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  Serial.println("Hello, world?");
  if (mySerial.available())
    Serial.print((char)mySerial.read());
    
    int measTempCount = 0;
    digitalWrite(13,HIGH); 
    delay(400);
    digitalWrite(13,LOW);
    setConnection();
      sendData();
}


void setConnection(){
    
    
  mySerial.println("AT+RST");
  while (mySerial.available())
    Serial.print((char)mySerial.read());
//Wait for the WiFi module to bootup
  delay(800);
//Switch the chip to "client" mode
  mySerial.println("AT+CWMODE=1");
  while (mySerial.available())
    Serial.print((char)mySerial.read());
  delay(800);

  Serial.println();

/*  mySerial.println("AT+CWLAP");
  delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
  delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
    delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
  */  
    delay(2000);
    Serial.println("begin connecting");
    delay(100);
    mySerial.println("AT+CWJAP=\"edland\",\"nathalie09\"");
    delay(500);
  while (mySerial.available())
    Serial.print((char)mySerial.read());  
    
  delay(3000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
    delay(100);
    //mySerial.println("AT+CWLIF");
  //delay(100);
    //Serial.println("ap check 1----");

    //delay(2000);
  
  
    while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    mySerial.println("AT+CIPSTATUS");
  delay(100);
    Serial.println("ap check 2----");

    delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    delay(100);
 
  
}

void sendData(){
  Serial.println("connect----");
    
    String cmd = "AT+CIPSTART=\"TCP\",\""; //make this command: AT+CPISTART="TCP","192.168.88.35",80
    cmd += DST_IP;
    cmd += "\",80";
    mySerial.println(cmd);
    delay(1000);

    while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    delay(100);
    Serial.println("connect--2");
    
     

    cmd = "GET /status.php HTTP/1.0\r\n";
   
    
    //The ESP8266 needs to know the size of the GET request
    mySerial.print("AT+CIPSEND=");
    mySerial.println(cmd.length());
    delay(200);
    while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    //mySerial.print(cmd);
    
    mySerial.print("GET /status.php");
    mySerial.println(" HTTP/1.0");
      delay(20);
    mySerial.println("Host: 192.168.1.12");
    mySerial.println("");

  delay(350);
  while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            inData[index] = '\0';
   }
   delay(10);
    while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            inData[index] = '\0';
   }
   delay(10);
    while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            inData[index] = '\0';
   }
    Serial.println(inData);
    
  
}

void loop() // run over and over
{    
    //send data every 1 mins
    if( measTempCount > 20){
      setConnection();
      sendData();
      measTempCount=0;
      Serial.println("wait 1 minute");
    }
    delay(2600);
    digitalWrite(13,HIGH); 
    delay(400);
    digitalWrite(13,LOW);
    measTempCount++;
    
    
    
}
