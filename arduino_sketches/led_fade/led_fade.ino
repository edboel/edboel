/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
#include <Wire.h>
#include "RTClib.h"

// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 5;
int led2 = 6;
const int offPin = 2;
const int upPin = 3;
RTC_DS1307 rtc;

int ledState = 255;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(offPin, INPUT);
  pinMode(upPin, INPUT);
  
  pinMode(led, OUTPUT); 
  analogWrite(led, 255);
  pinMode(led2, OUTPUT); 
  analogWrite(led2, 255);  
 
  Wire.begin(); 
  Serial.begin(9600);
  //rtc.adjust(DateTime(2015, 6, 3, 19, 28, 0));
}

// the loop routine runs over and over again forever:
void loop() {
  DateTime now = rtc.now();
  
  Serial.print(now.hour());
  Serial.println(now.minute());
  if(now.hour() == 6 && now.minute() == 45){
    Serial.print("wake");
     wakeUp(); 
  }
  //sleep 29 seconds
  checkButton(20);
}

void wakeUp(){
  
  analogWrite(led2, 255);
  analogWrite(led, 255);
  delay(2000);
  analogWrite(led, 250);
  if(checkButton(10)) return;
  for (int i=250; i > 220; i--){
    analogWrite(led, i);   // turn the LED on (HIGH is the voltage level)
    if(checkButton(40)) return;
  }
  
  for (int i=220; i > 100; i--){
    analogWrite(led, i);   // turn the LED on (HIGH is the voltage
     analogWrite(led2, (i+30));
    if(checkButton(20)) return;
  }
 
  
}

boolean checkButton(int attempts){
  int buttonState = 0;
  for(int i=0;i < attempts;i++){
    buttonState = digitalRead(offPin);
    if(buttonState == HIGH){
      analogWrite(led2, 255);
      analogWrite(led, 255);
      ledState = 255;
      return true;
    }
    
    buttonState = digitalRead(upPin);
    if(buttonState == HIGH){
      ledState--;
      ledState--;
       analogWrite(led, ledState);
       if(ledState < 220){
         analogWrite(led2, (ledState+30));
       }
    }
    
    delay(100);
  }
  return false;
}
