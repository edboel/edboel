
#include <kk3.h>

unsigned int countr=0;
unsigned int cc=0;

void setup() {
  Serial.begin(9600);
  kk_init();
  Serial.println("A: hoi ");
  kk_send(14977198,5,0,0);
  delay(1000);
  kk_send(14977198,6,0,0);
}

char * parse(char **s)
  {
    char *p = *s;
    char *q;
    while (*p==' ') p++;
    q = p;
    while ((*p!=' ') && (*p!=0)) p++;
    if (*p==' ') *p++ = 0;
    *s = p;
    return q;
  }  
  
void loop() 
{
   unsigned long address;
   unsigned char unit,dimlevel,onoff;
   static char inbuf[20];
   static unsigned char i=0;
   char *p,*s,c;
   if (kk_available())
     {
           kk_receive(&address,&unit,&onoff,&dimlevel);
           Serial.print("A: ");
           Serial.print(address);
           Serial.print("U: ");
           Serial.print(unit);
           Serial.print("C: ");
           Serial.print((onoff==0)?"Off":(onoff==1)?"On ":"Dim");
           Serial.print("D: ");
           Serial.println(dimlevel);        
      }
   if  (Serial.available())
     {
       c = Serial.read();
       if (c=='\n')
         {
           inbuf[i++]=0;
           i=0;
           Serial.println(inbuf);        
           p=inbuf;
           if (strlen(p)>5)
             {
               s=parse(&p);
               address=atol(s);
               s=parse(&p);
               unit=atoi(s);
               s=parse(&p);
               onoff=0;
               if (     strcmp(s,"off")==0)
                 onoff=0;
               else if (strcmp(s,"on") ==0)
                 onoff=1;
               else if (strcmp(s,"dim")==0)
                 onoff=2;
               if (onoff==2)
                 {
                   s=parse(&p);
                   dimlevel=atoi(s);
                 }
               else
                   dimlevel=0;  
               Serial.print("a: ");
               Serial.print(address);
               Serial.print("u: ");
               Serial.print(unit);
               Serial.print("c: ");
               Serial.print((onoff==0)?"Off":(onoff==1)?"On ":"Dim");
               Serial.print("d: ");
               Serial.println(dimlevel);        
               kk_send(address,unit,onoff,dimlevel);
             }
         } 
       if ((i<19) && (c!=13))
         {
           inbuf[i++] = c;
         }
     }  
     countr++;
     if(countr==65000){
       cc++;
       Serial.print("b");
       Serial.println(cc);
       countr=0;
     }
}
