#define dotPeriod (analogRead(A1))
#define dashPeriod (dotPeriod*3)
#define relaxTime (dotPeriod)
#define letterSpace (dotPeriod*3)
#define wordSpace (dotPeriod*4)
#define buzz (analogRead(A2)+200)

#define tonePin A0
#define ledPin 13

void setup(){
  
  pinMode(tonePin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
}

void loop(){
 
   playLetter('D');
   playLetter('V');
   playLetter('D');
   playLetter('R');
   playLetter('A');
   delay(4000);
}

void di()
{
  tone(tonePin, buzz);
  digitalWrite(ledPin, HIGH);
  delay(dotPeriod);
  noTone(tonePin);
  digitalWrite(ledPin, LOW);
  delay(relaxTime);
}

void dah()
{
  tone(tonePin, buzz);
  digitalWrite(ledPin, HIGH);
  delay(dashPeriod);
  noTone(tonePin);
  digitalWrite(ledPin, LOW);
  delay(relaxTime);
}

void playLetter(char x)
{
	switch (x){
		case 'E':
			di(); break;
		case 'T':
			dah(); break;
		case 'A':
			di();dah(); break;
                case 'R':
			di();dah();di(); break;
		case 'O':
			dah();dah();dah(); break;
                case 'P':
			di();dah();dah();di(); break;
                case 'D':
			dah();di();di(); break;
		case '1':
			di();dah();dah();dah();dah(); break;
		case 'C':
			dah();di();dah();di(); break;
                case 'F':
			di();di();dah();di(); break;
                case 'V':
			di();di();di();dah(); break;
		case ' ':
			delay(wordSpace);
        }
        delay(letterSpace);
}  
