/*
  Reading a serial ASCII-encoded string.
 
 This sketch demonstrates the Serial parseInt() function.
 It looks for an ASCII string of comma-separated values.
 It parses them into ints, and uses those to fade an RGB LED.
 
 Circuit: Common-anode RGB LED wired like so:
 * Red cathode: digital pin 3
 * Green cathode: digital pin 5
 * blue cathode: digital pin 6
 * anode: +5V
 
 created 13 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.
 */

// pins for the LEDs:
int led = 13;

int l1 = 4;
int l2 = 5;
int l3 = 6;
int l4 = 7;

int l5 = 8;
int l6 = 9;
int l7 = 10;
int l8 = 11;


// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
  pinMode(l1, OUTPUT);     
  pinMode(l2, OUTPUT);
pinMode(l3, OUTPUT);     
pinMode(l4, OUTPUT);     

  pinMode(l5, OUTPUT);     
  pinMode(l6, OUTPUT);
pinMode(l7, OUTPUT);     
pinMode(l8, OUTPUT);     

  Serial.begin(9600);

}

void loop() {
  // if there's any serial available, read it:
  while (Serial.available() > 0) {

    int val = Serial.parseInt();
   switch(val){
      case 1:
        digitalWrite(l1, HIGH);
        delay(300);
        digitalWrite(l1, LOW);
        break;
      
    case 2:
        digitalWrite(l2, HIGH);
        delay(300);
        digitalWrite(l2, LOW);
        break;
        
    case 3:
        digitalWrite(l3, HIGH);
        delay(300);
        digitalWrite(l3, LOW);
        break;
      
    case 4:
        digitalWrite(l4, HIGH);
        delay(300);
        digitalWrite(l4, LOW);
        break;
    
    case 5:
        digitalWrite(l5, HIGH);
        delay(300);
        digitalWrite(l5, LOW);
        break;
      
    case 6:
        digitalWrite(l6, HIGH);
        delay(300);
        digitalWrite(l6, LOW);
        break;
    
    case 7:
        digitalWrite(l7, HIGH);
        delay(300);
        digitalWrite(l7, LOW);
        break;
      
    case 8:
        digitalWrite(l8, HIGH);
        delay(300);
        digitalWrite(l8, LOW);
        break;
      
      
   } 
    
    
    // look for the newline. That's the end of your
    // sentence:
    if (Serial.read() == '\n') {
      // constrain the values to 0 - 255 and invert
      // if you're using a common-cathode LED, just use "constrain(color, 0, 255);"
      digitalWrite(l1, LOW);
      digitalWrite(l2, LOW);
      digitalWrite(l3, LOW);
      digitalWrite(l4, LOW);
      digitalWrite(l5, LOW);
      digitalWrite(l6, LOW);
      digitalWrite(l7, LOW);
      digitalWrite(l8, LOW);
      
      
    }
  }
}








