#include <Wire.h>
#include <GOFi2cOLED.h>
#include <kk.h>


GOFi2cOLED GOFoled;
boolean heh = false;
boolean hah = false;
int counter=50;
const int REMOTE1 = 7114338;
const int REMOTE2 = 14977198;
const int REMOTE2 = 17957282; // wall button 10, 11

void setup()
{
  pinMode(A1, INPUT);    
  pinMode(A0, INPUT);    
  pinMode(A2, OUTPUT);   
  // default slave address is 0x3D
  GOFoled.init(0x3C);  //initialze  OLED display
  
  kk_init();
  
  //GOFoled.display(); // show splashscreen
  //delay(2000);
  GOFoled.clearDisplay();
  
  GOFoled.setTextSize(1);
  GOFoled.setTextColor(WHITE);
  GOFoled.setCursor(0,0);
  GOFoled.println("Hello, world!");
  
  GOFoled.display();

}

void loop()
{
  unsigned long address;
  unsigned char unit,dimlevel,onoff;
  if (kk_available())
     {
           kk_receive(&address,&unit,&onoff,&dimlevel);
           GOFoled.clearDisplay();
           GOFoled.setCursor(0,0);
           GOFoled.println("Hello, world! A: ");
           GOFoled.println(address);
           GOFoled.setTextSize(2);
           GOFoled.print("U:   ");
           GOFoled.println(unit);
           GOFoled.setTextSize(1);
           GOFoled.print("C: ");
           GOFoled.println((onoff==0)?"Off":(onoff==1)?"On ":"Dim");
           GOFoled.print("D: ");
           GOFoled.println(dimlevel);
           GOFoled.display();        
      }
      
      if(digitalRead(A0)== HIGH){
        if(heh){
          kk_send(REMOTE2,4,0,0);
          heh = false;
        }else{
          kk_send(REMOTE2,4,1,0);
          heh = true;
        }
        delay(1000);
      }
      
      if(digitalRead(A1)== HIGH){
        if(hah){
          kk_send(REMOTE2,5,0,0);
          hah = false;
        }else{
          kk_send(REMOTE2,5,1,0);
          hah = true;
        }
        delay(1000);
      }
      
      if(counter == 40){
      GOFoled.setBrightness(255);
      }
      if (counter == 30){
        GOFoled.setBrightness(25);
      }
      if(counter == 20){
      GOFoled.setBrightness(0);
      }
      if (counter == 10){
        counter = 50; 
      }
      counter--;
      delay(100);
      
      
}
