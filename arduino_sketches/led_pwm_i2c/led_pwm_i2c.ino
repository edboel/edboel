// Wire Slave Receiver
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Receives data as an I2C/TWI slave device
// Refer to the "Wire Master Writer" example for use with this

// Created 29 March 2006

// This example code is in the public domain.


#include <Wire.h>

#define DIRECTION_PIN 8  // HIGH is up (relais is low then),  LOW is down direction
#define POWER_PIN 7

static int analogpins[] = {3,5,6,9,10,11};
boolean shutterState = false;
boolean shutterDirection = false;



void setup()
{
  Wire.begin(0x42);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  analogWrite(analogpins[0],255);
  analogWrite(analogpins[1],255);
  analogWrite(analogpins[2],255);
  
  analogWrite(analogpins[3],255);
  analogWrite(analogpins[4],255);
  analogWrite(analogpins[5],255);
  
  
  pinMode(DIRECTION_PIN,OUTPUT);
  digitalWrite(DIRECTION_PIN,HIGH);
  pinMode(POWER_PIN,OUTPUT);
  digitalWrite(POWER_PIN,HIGH);
  
}

void loop()
{
  delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{
  while(1 < Wire.available()) // loop through all but the last
  {
    int channel = Wire.read();    // receive byte as an integer
    int value = Wire.read();
    
    if(channel < 10){
      analogWrite(analogpins[channel],255-value);
      delay(250);
    }
    
    if(channel == 10){
      if(value == 1){
        //if already moving up, halt gracefully
        if (shutterState && shutterDirection == true){
          digitalWrite(POWER_PIN,HIGH);
          delay(7000);
        }
        shutterState = true;
      }else{
        shutterState = false;
      }
      //shutterDirection = false;
      //digitalWrite(DIRECTION_PIN,HIGH);
      delay(250);
      if(shutterState){
        digitalWrite(POWER_PIN,LOW);
      }else{
        digitalWrite(POWER_PIN,HIGH);
      }

    }
    
    if(channel == 11){
      if(value == 1){
        //if already moving up, halt gracefully
        if (shutterState == true && shutterDirection == false){
          digitalWrite(POWER_PIN,HIGH);
          delay(30000);
        }
        shutterDirection = true;
      }else{
        shutterDirection = false;
      }
      //shutterDirection = true;
      
      delay(500);
      if(shutterDirection){
        digitalWrite(DIRECTION_PIN,LOW);
        delay(30000);
        //digitalWrite(POWER_PIN,LOW);
        
      }else{
        digitalWrite(DIRECTION_PIN,HIGH);
        delay(5000);
        //digitalWrite(POWER_PIN,HIGH);
      }
    }
    
    
  }
          
}
