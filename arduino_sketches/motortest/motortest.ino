/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;

int l1 = 0;
int l2 = 1;
int l3 = 2;
int l4 = 3;

int l5 = 4;
int l6 = 5;
int l7 = 6;
int l8 = 7;


// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
  pinMode(l1, OUTPUT);     
  pinMode(l2, OUTPUT);
pinMode(l3, OUTPUT);     
pinMode(l4, OUTPUT);     

  pinMode(l5, OUTPUT);     
  pinMode(l6, OUTPUT);
pinMode(l7, OUTPUT);     
pinMode(l8, OUTPUT);     


}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(500);


  digitalWrite(l1, HIGH);
  delay(300);
  digitalWrite(l1, LOW);
  digitalWrite(l2, HIGH);
  delay(300);
  digitalWrite(l2, LOW);
  digitalWrite(l3, HIGH);
  delay(300);
  digitalWrite(l3, LOW);
  digitalWrite(l4, HIGH);
  delay(300);
  digitalWrite(l4, LOW);
  delay(500);
  delay(500);
  delay(500);
  
  
  digitalWrite(l5, HIGH);
  delay(300);
  digitalWrite(l5, LOW);
  digitalWrite(l6, HIGH);
  delay(300);
  digitalWrite(l6, LOW);
  digitalWrite(l7, HIGH);
  delay(300);
  digitalWrite(l7, LOW);
  digitalWrite(l8, HIGH);
  delay(300);
  digitalWrite(l8delay(500);
  , LOW);
  
  
  // wait for a second
}
