// Sample RFM69 sender/node sketch, with ACK and optional encryption

#include <RFM69.h>    //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPI.h>
#include "DHT.h"



#define NODEID        2    
#define NETWORKID     100  
#define GATEWAYID     1
#define FREQUENCY   RF69_433MHZ
#define ENCRYPTKEY    "1234567812345678" //exactly the same 16 characters/bytes on all nodes!
#define ACK_TIME      30 // max # of ms to wait for an ack

#define SERIAL_BAUD   9600

#define DHTPIN        4  //P1 DIO1  temp sensor
#define DOOR_PIN      5  //P2 DIO2
#define WATER_PIN     6  //P3 DIO3
#define LIGHT_PIN     7  //P4 DIO4
#define LSWITCH_PIN   A3 //P4 AIO4

#define DHTTYPE DHT22

int TRANSMITPERIOD = 150; //transmit a packet to gateway so often (in ms)
char payload[] = "123 ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//boolean requestACK = true;
RFM69 radio;
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(LIGHT_PIN, OUTPUT);      
  pinMode(DOOR_PIN, INPUT);
  pinMode(WATER_PIN, INPUT);
  pinMode(LSWITCH_PIN, INPUT);
  
  Serial.begin(SERIAL_BAUD);
  Serial.println(  "begin" );
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.encrypt(ENCRYPTKEY);
  
  dht.begin();
  
}

long lastPeriod = 0;
void loop() {
  float h = dht.readHumidity();
  // Read temperature as Celsius
  float t = dht.readTemperature();
  int doorState = digitalRead(DOOR_PIN);  
  int prevState = doorState;
  int waterState = digitalRead(WATER_PIN); 
  byte sendSize=0;
  char buff[64];
  // send a alive packet each minute
  //radio.setMode(RF69_MODE_TX);
  
  
  if (isnan(h) || isnan(t)) {
    sprintf(buff, "basement: temp failed D: %d W: %d",doorState,waterState);
  }else{
    sprintf(buff, "basement: %f C %f % D: %d W: %d", t,h,doorState,waterState);
  }
  sendSize=strlen(buff);
  
  
  radio.send(GATEWAYID, payload, sendSize,true);
  
  //radio.setMode(RF69_MODE_RX);
  
  for(int i=0;i<600; i++){
    delay(100);
    doorState = digitalRead(DOOR_PIN);
    if(doorState == HIGH){
      digitalWrite(LIGHT_PIN, HIGH);
    }else{
      digitalWrite(LIGHT_PIN, LOW);
    }
    
    if(prevState != doorState){
      
      sprintf(buff, "basement: Door event %d", doorState);
      sendSize=strlen(buff);
  
  
      radio.send(GATEWAYID, payload, sendSize,true);
      
      prevState == doorState;
    }
    
    
  }
}



