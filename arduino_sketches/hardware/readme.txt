Drop the "Moteino" folder and all its contents in your Arduino Sketches/hardware folder.
To find the location of this on your machine go to "File > Preferences" in Arduino IDE.
Then restart the IDE and under "Tools > Boards" you should have 2 new entries: Moteino, MoteinoMEGA.
This was tested on Arduino IDE v 1.0.6