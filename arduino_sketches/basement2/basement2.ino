// Sample RFM69 sender/node sketch, with ACK and optional encryption

#include <RFM69.h>    //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPI.h>
#include "DHT.h"



#define NODEID        2    
#define NETWORKID     100  
#define GATEWAYID     1
#define FREQUENCY   RF69_433MHZ
#define ENCRYPTKEY    "1234567812345678" //exactly the same 16 characters/bytes on all nodes!
#define ACK_TIME      30 // max # of ms to wait for an ack

#define DHTPIN        4  //P1 DIO1  temp sensor
#define DOOR_PIN      5  //P2 DIO2
#define WATER_PIN     6  //P3 DIO3
#define LIGHT_PIN     7  //P4 DIO4
#define LSWITCH_PIN   A3 //P4 AIO4
#define SEND_INTERVAL 6000 //6000*100ms = 10 mins
#define LIGHTTIME     1800 //1800*100ms = 3 mins

#define DHTTYPE DHT22

#define SERIAL_EN //comment this out when deploying to an installed SM to save a few KB of sketch size
#define SERIAL_BAUD 9600
#ifdef SERIAL_EN
  #define DEBUG(input) {Serial.print(input); delay(1);}
  #define DEBUGln(input) {Serial.println(input); delay(1);}
#else
  #define DEBUG(input);
  #define DEBUGln(input);
#endif

#define TRANSMITPERIOD = 150; //transmit a packet to gateway so often (in ms)
//char payload[] = "123 ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//boolean requestACK = true;
RFM69 radio;
DHT dht(DHTPIN, DHTTYPE);

int prevDoorState = 0;
int prevLightState = 0;


void setup() {
  pinMode(LIGHT_PIN, OUTPUT);      
  pinMode(DOOR_PIN, INPUT);
  pinMode(WATER_PIN, INPUT);
  pinMode(LSWITCH_PIN, INPUT);
  
  #ifdef SERIAL_EN
    Serial.begin(SERIAL_BAUD);
  #endif
  DEBUGln(  "begin" );
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.encrypt(ENCRYPTKEY);
  
  dht.begin();
  
}

void loop() {
  int lightDelay = 0; 
  
  sendStatus();
    
  //radio.setMode(RF69_MODE_RX);
  
  for(int i=0;i<SEND_INTERVAL; i++){
    delay(100);
    lightDelay = checkSensors(lightDelay);
    
    checkReceive();
    
  }
}


void sendStatus(){
  
  float h = dht.readHumidity();
  // Read temperature as Celsius
  float t = dht.readTemperature();
  delay(15); // wait for values..
  int doorState = digitalRead(DOOR_PIN);  
  int waterState = digitalRead(WATER_PIN); 
  int lightState = analogRead(LSWITCH_PIN); 
  byte sendSize=0;
  char buff[64];
  // send a alive packet each minute
  //radio.setMode(RF69_MODE_TX);
  char temp[6];
  char humm[6];
   dtostrf(t,2,2,temp);
   dtostrf(h,2,1,humm);
   if(waterState == 0){
     waterState = 1;
   }else{
     waterState = 0;
   }
   
  if (isnan(h) || isnan(t)) {
    sprintf(buff, "basement: T:E;C H:E;%% D:%d; W:%d; L:%d;",doorState,waterState,lightState);
  }else{
    sprintf(buff, "basement: T:%s;C H:%s;%% D:%d; W:%d; L:%d;",temp,humm,doorState,waterState,lightState);
  }
  DEBUGln(buff);
  sendSize=strlen(buff);
  radio.sendWithRetry(GATEWAYID, buff, sendSize,true);
}
  

int checkSensors(int lightDelay){
    byte sendSize=0;
    char buff[64];
    int doorState = digitalRead(DOOR_PIN); 
//  waterstate is not urgent, controller will handle the alert
//    int waterState = digitalRead(WATER_PIN);
    
    // 230Vdetect
    int lightState = digitalRead(LSWITCH_PIN);
    if(prevLightState != lightState){
       sprintf(buff, "basement: Light event %d - %d - %d ", lightState, analogRead(LSWITCH_PIN), lightDelay);
       DEBUGln(buff);
       sendSize=strlen(buff);
       radio.send(GATEWAYID, buff, sendSize,true);
           
       prevLightState = lightState;
    }
    
    if(prevDoorState != doorState){
      delay(100);
      if(prevDoorState != doorState){
        
          sprintf(buff, "basement: Door event %d - %d", doorState, lightDelay);
          DEBUGln(buff);
          sendSize=strlen(buff);
          radio.send(GATEWAYID, buff, sendSize,true);
          
          if(lightDelay == 0){
            toggleLight();
            lightDelay = LIGHTTIME;
          }
          prevDoorState = doorState;
       
      }
    } 
    
    if (lightDelay > 0){
      lightDelay--; 
      if(lightDelay == 1){
        toggleLight();
         
      }
    }
    
//    if(waterState == HIGH){
//      
//    }
  return lightDelay;
}

void checkReceive(){
  
    //----- RECEIVED COMMAND ----
    
    if (radio.receiveDone())
    {
      delay(10);
      DEBUG('[');DEBUG(radio.SENDERID);DEBUGln("] ");
      
      for (byte i = 0; i < radio.DATALEN; i++){
        DEBUG((char)radio.DATA[i]);
      }
      
      DEBUG("   [RX_RSSI:");DEBUG(radio.RSSI);DEBUGln("]");
      
      if (radio.ACKRequested())
      {
        //byte theNodeID = radio.SENDERID;
        radio.sendACK();
        DEBUGln(" - ACK sent.");
      }
  
      //STATUS
      if (radio.DATALEN==7 && radio.DATA[0]=='S' && radio.DATA[1]=='T' 
        && radio.DATA[2]=='A' && radio.DATA[3]=='T' && radio.DATA[5]=='U' && radio.DATA[5]=='S') 
       // && radio.DATA[4]>='0' && radio.DATA[4]<='2' && (radio.DATA[6]=='0' || radio.DATA[6]=='1'))
      {
        delay(100);
        sendStatus();
        delay(100);
        sendStatus();
      }
      
      //LIGHT
      if (radio.DATALEN==6 && radio.DATA[0]=='L' && radio.DATA[1]=='I' 
        && radio.DATA[2]=='G' && radio.DATA[3]=='H' && radio.DATA[5]=='T') 
       // && radio.DATA[4]>='0' && radio.DATA[4]<='2' && (radio.DATA[6]=='0' || radio.DATA[6]=='1'))
      {
        toggleLight();
      }
      
    } 
  
}

int digitalReadOutputPin(uint8_t pin)
{
  uint8_t bit = digitalPinToBitMask(pin);
  uint8_t port = digitalPinToPort(pin);
  if (port == NOT_A_PIN) 
    return LOW;

  return (*portOutputRegister(port) & bit) ? HIGH : LOW;
}


//TODO read 230V detect with optocoupler circuit connected to AIO4

void toggleLight(){
  int doorState = digitalRead(DOOR_PIN);
  int lightState = digitalReadOutputPin(LIGHT_PIN); 
  byte sendSize=0;
  char buff[64];
    
  // int lightState = digitalRead(LSWITCH_PIN);  
    
  if(lightState == LOW && doorState == 1){
        digitalWrite(LIGHT_PIN, HIGH);
        sprintf(buff, "basement: Light event %d", 1);
          DEBUGln(buff);
          sendSize=strlen(buff);
          radio.send(GATEWAYID, buff, sendSize,true);
      }else{
        digitalWrite(LIGHT_PIN, LOW);
        sprintf(buff, "basement: Light event %d", 0);
          DEBUGln(buff);
          sendSize=strlen(buff);
          radio.send(GATEWAYID, buff, sendSize,true);
      }
}




