#include <Wire.h>  // Comes with Arduino IDE
#include <LiquidCrystal_I2C.h>
#include <Encoder.h>
#include "RTClib.h"

Encoder myEnc(2, 3);
//Define the pin for the on board LED
int led = 13;
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
int count =1;
int tcount =1;
long oldPosition  = -999;
RTC_DS1307 rtc;

void setup()
{
   Wire.begin();
  lcd.begin(20,4);   // initialize the lcd for 16 chars 2 lines, turn on backlight

  lcd.backlight(); // finish with backlight on  
  lcd.setCursor(0,0); //Start at character 4 on line 0
  lcd.print("Hello, world!");
  delay(1000);
  lcd.setBacklight(0);
  
}

void loop()
{
  long newPosition = myEnc.read();
  if (newPosition != oldPosition) {
    oldPosition = newPosition;
    lcd.setCursor(0,3);
    lcd.print(newPosition);
    lcd.backlight();
    count = 15000;
  }
  
  if(tcount < 5){
    DateTime now = rtc.now();
    lcd.setCursor(0,1);
    lcd.print(now.hour());
    lcd.print(":");
    lcd.print(now.minute());
    lcd.print(":");
    lcd.print(now.second());
    lcd.print("   ");
    tcount=(100);
  }
  
  if(count > 0){
   count--; 
   tcount--;
    delay(2);
  }else{
    lcd.setBacklight(0);
  }
  
  
  
}
