#include <Wire.h>
#include <GOFi2cOLED.h>
#include <SoftwareSerial.h>
#include "DHT.h"
#define DHTPIN 4
#define DHTTYPE DHT22
#define DST_IP "192.168.0.105"

// nov 2015

SoftwareSerial mySerial(2, 3);
GOFi2cOLED GOFoled;
DHT dht(DHTPIN, DHTTYPE);


float humidity;
float t;
String tempAsString;
String humAsString;
char temp[10];
int measTempCount;

char inData[255]; // Allocate some space for the string
char inChar=-1; // Where to store the character read
byte index = 0;


void setup(){

  Serial.begin(9600);
  Serial.println("Hello, world?");
  mySerial.begin(9600);
  GOFoled.init(0x3C);
  dht.begin();
  
  GOFoled.clearDisplay();
     GOFoled.setCursor(0,0);
     GOFoled.print("init ");
     GOFoled.display();
     delay(800);
   if (mySerial.available())
    Serial.print((char)mySerial.read());
  
     measTempCount=0;
    setConnection();
      sendData();
     
}


void setConnection(){
     humidity = dht.readHumidity();
    t = dht.readTemperature(); 
    dtostrf(t,1,2,temp);
      tempAsString = String(temp);
      dtostrf(humidity,1,2,temp);
      humAsString = String(temp);
    Serial.print(humidity);
    Serial.print("H ");
    Serial.print(tempAsString);
    Serial.println("c ");
    
     GOFoled.clearDisplay();
      GOFoled.setTextSize(1);
     GOFoled.setCursor(0,0);
     GOFoled.print("hum: ");
     GOFoled.println(humidity);
      GOFoled.println(" ");
      GOFoled.println(" ");
     GOFoled.setTextSize(3);
     GOFoled.print(tempAsString);
     GOFoled.println(" C");
    GOFoled.display();
    
    
  mySerial.println("AT+RST");
  while (mySerial.available())
    Serial.print((char)mySerial.read());
//Wait for the WiFi module to bootup
  delay(800);
//Switch the chip to "client" mode
  mySerial.println("AT+CWMODE=1");
  while (mySerial.available())
    Serial.print((char)mySerial.read());
  delay(800);

  Serial.println();

/*  mySerial.println("AT+CWLAP");
  delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
  delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
    delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
  */  
    delay(2000);
    Serial.println("begin connecting");
    delay(100);
    mySerial.println("AT+CWJAP=\"edland\",\"****\"");
    delay(500);
  while (mySerial.available())
    Serial.print((char)mySerial.read());  
    
  delay(3000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
    delay(100);
    //mySerial.println("AT+CWLIF");
  //delay(100);
    //Serial.println("ap check 1----");

    //delay(2000);
  
  
    while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    mySerial.println("AT+CIPSTATUS");
  delay(100);
    Serial.println("ap check 2----");

    delay(1000);
  while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    delay(100);
 
  
}

void sendData(){
  Serial.println("connect----");
    
    String cmd = "AT+CIPSTART=\"TCP\",\""; //make this command: AT+CPISTART="TCP","192.168.88.35",80
    cmd += DST_IP;
    cmd += "\",80";
    mySerial.println(cmd);
    delay(1000);

    while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    delay(100);
    Serial.println("connect--2");
    
     humidity = dht.readHumidity();
    t = dht.readTemperature(); 
    dtostrf(t,1,2,temp);
      tempAsString = String(temp);
      dtostrf(humidity,1,2,temp);
      humAsString = String(temp);

    //cmd = "GET /status.php HTTP/1.0\r\n";
    cmd = "GET /projects/senswi/statusg.php?s=0&t=";
      cmd.concat(tempAsString);
      cmd.concat("&h=" + humAsString);
      cmd.concat(" HTTP/1.0\r\n");
    cmd += "Host: 192.168.1.12\r\n\r\n";
    
    //The ESP8266 needs to know the size of the GET request
    mySerial.print("AT+CIPSEND=");
    mySerial.println(cmd.length());
    delay(200);
    while (mySerial.available())
    Serial.print((char)mySerial.read());
    
    //mySerial.print(cmd);
    
    mySerial.print("GET /projects/senswi/statusg.php?s=0&t=");
    mySerial.print(tempAsString);
    mySerial.print("&h=" + humAsString);
    mySerial.println(" HTTP/1.0");
      delay(20);
    mySerial.println("Host: 192.168.1.12");
    mySerial.println("");

  delay(350);
  while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            inData[index] = '\0';
   }
   delay(10);
    while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            inData[index] = '\0';
   }
   delay(10);
    while (mySerial.available()){
    inChar = mySerial.read(); // Read a character
            inData[index] = inChar; // Store it
            index++;
            inData[index] = '\0';
   }
    Serial.println(inData);
    
  
}

void loop() // run over and over
{    
    //send data every 15 mins
    if( measTempCount > 300){
      setConnection();
      sendData();
      measTempCount=0;
    }
    delay(2600);
    digitalWrite(13,HIGH); 
    delay(400);
    digitalWrite(13,LOW);
    measTempCount++;
    
    
    
}





