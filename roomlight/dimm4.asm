; dimm4.asm
; a button dims a led in 8 steps
;

	list P = 16F628a	;microcontroller identity
		; e.g: 0x033 = hex value

	__Config 3F18h 

;****************************************************************
;Equates
;****************************************************************
status	equ	0x03
cmcon	equ	0x1F
rp1	equ	0x06	;this is bit 6 in file 03
rp0	equ	0x05	;this is bit 5 in file 03

flags	equ	0x30	;flag file



	cblock 	0x20 			;start of general purpose registers
		count1 			;used in delay routine
		counta 			;used in delay routine 
		countb 			;used in delay routine
    CountR  
    CountRoff
    CountGoff 
    CountBoff
    Acount 
    aDel	  
   endc

; countvalues they are all 255 by default



;****************************************************************
;Beginning of program
;****************************************************************
reset:	org	0x00	;reset vector address	
	goto	SetUp	;goto set-up
	nop
	nop
	nop
	org	4	;interrupts go to this location 
	goto	isr1	;goto to Interrupt Routine  - not used
			;    in these experiments.
			;isr1 must be written at address 004
			;   otherwise bcf status,rp1 will be 
			;   placed at address 01 by assembler!	

;****************************************************************
;* Port A and B initialisation			*
;****************************************************************

SetUp	bcf	status,rp1		;select bank 1 
	bsf	status,rp0

	movlw	0xFF		;make all Port A inputs
	movwf	0x05			
	movlw	0x0F		; out out out out  in in in in 
	movwf	0x06		; 

	bcf	status,rp0		;select programming area - bank0
	movlw	0x07		;turn comparators off and enable
	movwf	cmcon		;    pins for I/O functions
	
    movlw	0x86 ; set w to 150
    movwf   CountRoff ; counterOff is 150
    movwf   CountGoff ; green off is 150
    movlw	0x06
    movwf   CountBoff
    movlw   0xFE;
    movwf   CountR
    goto 	Main

;****************************************************************
;* Interrupt Service Routine will go here (not used)	*
;****************************************************************
isr1

;****************************************************************
;* Main 					*
;****************************************************************

Main
   	bsf	0x06,5	;Port B, output bit 5 = turn on red LED pin 11
    bsf	0x06,4	;Port B, output bit 4 = turn on green LED pin 11
    bsf	0x06,6

;*** begin pwm ***********************************
	movf CountR,0  ; move 'on' time in W
    movwf Acount ;  move W into the counter
CRon    decfsz Acount,1 ; decrease acount until its zero
    goto CRonn
    goto CRonc
CRonn 
    movf CountRoff,0 ; 
    XORWF Acount,0 ;XOR cuurent count and CountRoff
    BTFSC 03,2   ;Test Z flag  If Z flag is SET (ie 1) the two files are the SAME!  
    bcf	0x06,5 ; then turn off red
    
    movf CountGoff,0 ; 
    XORWF Acount,0 ;XOR current count and Count green off
    BTFSC 03,2   ;Test Z flag  If Z flag is SET (ie 1) the two files are the SAME!  
    bcf	0x06,4 ; then turn off green

    movf CountBoff,0 ; 
    XORWF Acount,0 ;XOR current count and Count green off
    BTFSC 03,2   ;Test Z flag  If Z flag is SET (ie 1) the two files are the SAME!  
    bcf	0x06,6 ; then turn off blue
;**** end pwm ********************************************

    goto CRon
;**** begin check buttons ********************************
CRonc ;movf CountR,0
    ;movwf Acount ; set Acount back to its original value
 	btfsc	0x06,0	;Port B, input bit 0 = red button pushed?
    goto PushRed
    btfsc   0x06,1 ; portb = green button pushed
    goto PushGreen
    btfsc   0x06,2 ; portb = green button pushed
    goto PushBlue


    clrf	flags	;clear flag file
    goto	Main


;*******************************************************
;************ subroutines              ******************

; push red button routine
PushRed btfsc	flags,0	;test flag (button has been pushed previous loop) 
	goto	Main
    movlw 0x0A ;  
    addwf CountRoff,1
    MOVLW 0xA0 
    SUBWF CountRoff,0
    btfsc 03,0
    call RedCarry

    bsf	flags,0	;set the flag
	goto Main

RedCarry
   movlw	0x02 ; set w to 150
   movwf   CountRoff ; counterOff is 150 
   return

; push green button routine
PushGreen btfsc	flags,0	;test flag (button has been pushed previous loop) 
	goto	Main
    movlw 0x0B ;  
    addwf CountGoff,1
 
    bsf	flags,0	;set the flag
	goto Main

; push green button routine
PushBlue btfsc	flags,0	;test flag (button has been pushed previous loop) 
	goto	Main
    movlw 0x0B ;  
    addwf CountBoff,1

    bsf	flags,0	;set the flag
	goto Main
;*******************************************************
; delay
Dels    movlw 0x10 ; 32 counts
        movwf aDel
Loop1   decfsz  aDel,1     ;
        goto Loop1    
     
     return  

	
Del  movlw	d'2'			;delay 250 ms (4 MHz clock)
	movwf	count1
d1	movlw	0xC7
	movwf	counta
	movlw	0x01
	movwf	countb
Delay_0
	decfsz	counta, f
	goto	$+2
	decfsz	countb, f
	goto	Delay_0

	decfsz	count1	,f
	goto	d1
	retlw	0x00

	END
