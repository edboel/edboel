<h2>Computer controlled robot arm</h2>
<br>
Edsard Boelen<br>
24-jul-2003<br>
<br>
<hr><i>
I am going to build some robots, at first I will begin with a robot arm. I think those are more fun than some car riding circles on the floor. 
I got this idea when I was doing some really stupid work and wondering why these jobs still exsists. Those should be replaced by robots long ago. 
The technology is far beyond that point . Maybe economics, availabillty and installation are the problems now. 
I got this dream about 2 robot arms besides my monitor, USB connection, pneumatic driven, being able to do different things by themselves. That would be cool.
</i><hr>
<br>
1) need info<br>
2) function list<br>
3) mechanical design<br>
4) electric design<br>
5) software design<br>
some links<br>
<a href="http://www.fischertechnik.nl">dutch fischer technik importer</a><br>
<a href="http://www.fischertechnik.de">official fischer technik site</a><br>
<br>
I think I will start with an easy one, small weak and noisy LEGO or Fischer Technik  arm. connected to parallel port.
My end goal is a large, strong and silent freely moveable arm. With a good software interface.<br>
<br>
<h2> mechanical construction </h2>
<b> Day 1</b><br>
I have taken my old lego box from the attic There is not much technic lego left, or I never had much. 
Wanted to buy extra parts, but lego doesn't seem to supply those things anymore. 
So I switched to fisher technik, it seems that I had much more Fisher technik than I thought. 
Now it's time to play a bit with fisher technic and try to make a small Robot arm. Later on, I can bould a larger (more difficult) version of it.
Here are some results: 
<br><br>
<img src="robot/eddo_080403_003.jpg"><br><br>
<img src="robot/eddo_080403_002.jpg"><br><br>
<img src="robot/eddo_080203_002.jpg"><br><br>



<h2> the parallel port computer interface </h2>

I already have done a lot with the parallel port, Anyway, here we go again.
At first I found a cable which had the following connectors and pinouts:<br>
<table border = 1>
<tr><td>25-pol Sub-D connector &nbsp; <br> pin number:</td><td>9-pol Sub-D connetor &nbsp; <br> pin number:</td><td> Function </td></tr>
<tr><td> 25,24,23,22 </td><td> 1 </td><td> GND </td></tr>
<tr><td> 2 </td><td> 9 </td><td> Data out 1 </td></tr>
<tr><td> 3 </td><td> 8 </td><td> Data out 2 </td></tr>
<tr><td> 4 </td><td> 7 </td><td> Data out 3 </td></tr>
<tr><td> 5 </td><td> 6 </td><td> Data out 4 </td></tr>

<tr><td> 10 </td><td> 2 </td><td> Data in 1 </td></tr>
<tr><td> 11 </td><td> 3 </td><td> Data in 2 </td></tr>
<tr><td> 12 </td><td> 4 </td><td> Data in 3 </td></tr>
<tr><td> 13 </td><td> 5 </td><td> Data in 4  </td></tr>
</table>
<br>
<br>
First I will build a testbed with 4 switches and 4 leds, just to see if the software works. 
After that I will build a buffer / power board. The engines operate at 9V and can use up to 250mA. therefore I will use BD137 transistors. 
<br><br>
After some testing, the following schematic came out<br><br>

<a href="robot/parportMdriver2.ps"><img border=0 src="robot/parportMdriver2.png"></a>
<br>
The final board looks like this:<br>
<img src="robot/eddo_080503_003.jpg"><br>


<h2>software</h2>
At first, I wrote an ugly test routine, but it uses ncurses library, no more enter pressing. 
You can view the source code <a href="robot/test4.c.html">here</a>
<br>


<br>
some images:<br>
<br>
You can see that this is a crane; well, this is robot arm v0.1 and it's pretty difficult to construct a solid arm which can lift things, so I first test the whole system with a much easier object.<br>
<img src="robot/eddo_080503_001.jpg"><br><br>
<img src="robot/eddo_080603_005.jpg"><br><br>


<br>
<br>
<br>




