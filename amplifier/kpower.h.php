<?
/************* kpower.h *******************/
#include <kapp.h>
#include <kmainwindow.h>
#include <qpushbutton.h>

class KPower : public KMainWindow
{
  Q_OBJECT
public:
  KPower();
  void closeEvent(QCloseEvent *);
public slots:
  void slotPwrOn();
  void slotPwrOff();
  void slotExit();
private:
  QPushButton *btnPwrOn;
  QPushButton *btnPwrOff;
  QPushButton *btnExit;
};
?>
