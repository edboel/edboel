<h2>Programming</h2>
Linux -- KDE<br>
<hr>
This tells in short how to control the serial port in linux.<br>
I first searched for information on <a href="http://www.linuxdoc.org">linuxdoc.org</a> and found the serial-programming-HOWTO<br>
now I tried to make some little programs in C<br>
the final program was <a href="programming.php?showsource=1">serialcontrol.c </a><br>
<?php if($showsource=='1'){
	echo"<a href=\"programming.php\"><b>close this source</b></a><br>"; 
	include ("serialcontrol.html"); 
	echo"<a href=\"programming.php\"><b>close this source</b></a>"; 
	}?> 
<br>
here are some main functions:<br>
open the serial port with<br>
<b>fd = open(/dev/ttyS1, O_RDWR);</b><br>
 /dev/ttyS1 is the second serial port, O_RDWR means Open with ReaD WRite<br>
then set a pin to "high" with<br>
<b>ioctl(fd, TIOCMBIS,TIOCM_DTR);</b><br>
<br>
I made some cables to check how it's working<br>
<img src="rs232-cable.jpg"> <a href="http://www.beyondlogic.org/"><img src="rs232-cable-sch.gif"></a><br>
<br>
When that worked well I installed <a href="http://kdevelop.kde.org">kdevelop</a> and <a href="http://www.trolltech.com">QT designer</a> and tried some tutorials<br>
It didn't work out so good. But one day I got a tip-at-startup screen that said there was another develop page for kde: <a href="http://develop.kde.org"> develop.kde.org</a><br>
there were some nice tutorials including <a href="http://kdevelop.kde.org/documentation/tutorials/khello">this one</a><br>
I rewrote the program to let it turn a LED on and off<br>

<a href="programming.php?showsource=2">view the source codes</a>
	<?php if($showsource=='2'){
		include("powerbutt");
		echo"<br><BR>start file:<br>";
		show_source("main.cc.php");
		echo"<BR><BR>header<br>";
		show_source("kpower.h.php");
		echo"<BR><BR>main code:<br>";
		show_source("kpower.cc.php");
	}
	?>
<br>
<br>
Now, the code isn't finished yet, there are some things that have to be added<br>
<br>
<br>
<Br>
