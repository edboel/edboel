
<pre>
#include curses.h>
#include stdlib.h>
#include stdio.h>
#include unistd.h>
#include fcntl.h>
#include termios.h>
#include string.h>
#include sys/ioctl.h>

/* global variables */
char device[255] = "/dev/ttyS0";                          /* name of device to open */

int main()
{
  int fd,dtr,count=0;
  unsigned int arg;

   /* open port */
  fd = open(device, O_RDWR);
  if (fd == -1) {
    char s[255];
    sprintf(s, "statserial: can't open device `%s'", device);
    perror(s);
    exit(1);
  }

  
    /* loop forever */
  for (;;) {


    printf("Device: %s\n\n", device);
    printf("Signal  Pin  Pin  Direction  Status  Full\n");
    printf("Name    (25) (9)  (computer)         Name\n");
    printf("-----   ---  ---  ---------  ------  -----\n");
    printf("FG       1    -      -           -   Frame Ground\n");
    printf("TxD      2    3      out         %1d   Transmit Data\n", !!(arg & TIOCM_ST));
    printf("RxD      3    2      in          %1d   Receive  Data\n", !!(arg & TIOCM_SR));
    printf("RTS      4    7      out         %1d   Request To Send\n", !!(arg & TIOCM_RTS));
    printf("CTS      5    8      in          %1d   Clear To Send\n", !!(arg & TIOCM_CTS));
    printf("DSR      6    6      in          %1d   Data Set Ready\n", !!(arg & TIOCM_DSR));
    printf("GND      7    5      -           -   Signal Ground\n");
    printf("DCD      8    1      in          %1d   Data Carrier Detect\n", !!(arg & TIOCM_CAR));
    printf("DTR     20    4      out         %1d   Data Terminal Ready\n", !!(arg & TIOCM_DTR));
    printf("RI      22    9      in          %1d   Ring Indicator\n", !!(arg & TIOCM_RNG));
    printf("\ncounter: %1d\n",count);
    count++;
    if (count == 3) {
	    printf("\nwh00000000000t\n\n hehehe\n");
	    dtr=TIOCM_DTR;
	    // Clear DTR (4)
	    ioctl(fd, TIOCMBIC,&dtr);
    }

 if (count == 6) {
	    printf("\nwh00000000000t\n\n hehehe\n");
	    dtr=TIOCM_DTR;
	    // Set DTR
	    ioctl(fd, TIOCMBIS,&dtr);
    }

 if (count == 9) {
	    printf("\nwh00000000000t\n\n hehehe\n");
	    dtr=TIOCM_RTS;
	    // Clear RTS
	    ioctl(fd, TIOCMBIC,&dtr);
    }

 if (count == 12) {
	    printf("\nwh00000000000t\n\n hehehe\n");
	    dtr=TIOCM_ST;
	    // Set ST ()
	    ioctl(fd, TIOCMBIS,&dtr);
    }


 if (count == 15) {
	    printf("\nwh00000000000t\n\n hehehe\n");
	    dtr=TIOCM_RTS;
	    // Set RI ()
	    ioctl(fd, TIOCMBIS,&dtr);
    }


   // refresh();
    /* home cursor */
//:    move(0,0);
     /* delay 1 second between loops */
    sleep(1);
    }

  return 0;
}  
