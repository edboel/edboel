<html>
<title>homebuild computer controlled TDA7294 Amplifier</title>
<center>
<h2>Homebuild 200W stereo amplifier with computer controlled power and volume using TDA7294</h2>
<br>
current status<br>
<a href="amplifier/versterker_klaar.jpg"><img src="amplifier/versterker_final_thumb.jpg"></a><br>
<br>
<img src="amplifier/main schematic.bmp"><br>
<br>
<a name="programming" href="amplifier/programming.html">Programming</a><br>
I made a software application that resides in the system tray and can turn the amplifier on/off by clicking on it<br>
It also handles the volume from the amplifier<br>
<br>

<a name="interface" href="amplifier/rs232_serial_interface.html">Interface</a><br>
the interface is very simple, that was also the meaning of my project<br>
<br>

<a name="switch" href="amplifier/digital_power_switch.html">Digital Power switch</a><br>
the power switch is also very simple, just a relay and a very small power supply<br>
<br>

<a name="volume" href="amplifier/digital_volume_control.html">Digital Volume Control</a><br>
The volume control is fully electronic but has low distortion<br>
<br>

<a name="supply" href="amplifier/power_supply.html">Power Supply</a><br>
power supply is probably the most expensive part. But it is needed for a good sound<br>
<br>

<a name="amplifier" href="amplifier/tda_7294_amplifier.html">Amplifier</a><br>
The amplifier is based on the high quality TDA 7294 mono amplifier IC and is again easy to build<br>
<br>

<a name="speaker" href="amplifier/speaker.html">Speakers</a><br>
To know which speakers I use is not really relevant, but it will show some pictures of the final result<br>
<br>

<?php
	include("amplifier/countmain.php");
?>
