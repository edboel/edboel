<?
/************* kpower.cc ******************/
#include "kpower.moc"
#include <kmessagebox.h>
#include <fcntl.h>
#include <sys/ioctl.h>

char device[255]= "/dev/ttyS0";
int dtr,fd;

KPower::KPower() : KMainWindow()
{
  btnPwrOn = new QPushButton("Power On", this);
  btnPwrOn->setGeometry(45,30,65,25);
  btnPwrOn->show();
  connect(btnPwrOn, SIGNAL(clicked()), this, SLOT(slotPwrOn()));

  btnPwrOff = new QPushButton("Power Off", this);
  btnPwrOff->setGeometry(120,30,65,25);
  btnPwrOff->show();
  connect(btnPwrOff, SIGNAL(clicked()), this, SLOT(slotPwrOff()));


  btnExit = new QPushButton("Exit", this);
  btnExit->setGeometry(200,30,50,25);
  btnExit->show();
  connect(btnExit, SIGNAL(clicked()), this, SLOT(slotExit()));
}

void KPower::closeEvent(QCloseEvent *e)
{
  kapp->beep();
  KMainWindow::closeEvent(e);
}

void KPower::slotPwrOn()
{
     /* open port */
  fd = open(device, O_RDWR);
  if (fd == -1) {
	KMessageBox::information(0,"can't open device","Important");
    close();
  }

  /*set DTR bit */
 	dtr=TIOCM_DTR;
 	ioctl(fd, TIOCMBIS,&dtr);
}

void KPower::slotPwrOff()
{
 /*clear DTR bit */
 	dtr=TIOCM_DTR;
 	ioctl(fd, TIOCMBIC,&dtr);
}

void KPower::slotExit()
{
  close();
}
?>

