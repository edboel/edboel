<html>
  <head>
    <title>day1: test RS232</title>
    <meta content="">
    <style></style>
  </head>
  <body>
    pref <a href="index.php">home</a> <a href="index2.php">next</a><br><center>day1: test RS232</center><br>
    First the basics, always start with a small thing;
    <h2>sending to RS232</h2>
    I tested if I could write i tiny program that sends signals to the RS232 port.<br>
    here is the schematic:<br>
    <img src="rs232-to-led.gif">
    <img src="rs232-to-led-pict.gif"><br>
    now it was time to turn the led on with a linux program.
    after searching on the internet and some testing, it finally worked,<br>
    <li> <a href="rs232-to-led.c.txt">click here</a> to view the source
    <li> <a href="http://www.linuxdoc.org/HOWTO/Serial-Programming-HOWTO/index.html">click here</a> to get the rs232 serial-programming-HOWTO
    <li> <a href="http://www.cm.cf.ac.uk/Dave/C/"> http://www.cm.cf.ac.uk/Dave/C/</a> a good c reference site with many examples
    <li> <a href="http://www.acm.uiuc.edu/webmonkeys/book/c_guide/">click here</a> for another good c reference
    <h2>receiving from RS232</h2>
    now sending worked, it was time to use it to receive data. I added a switch to the LED, and tried to let the program recieve the data.<br>
    Finding the right example on the internet that work with my test, took me an hour or so. <br>
    <img src="rs232-to-switch.gif">
    <img src="rs232-to-led-pict.gif"><br>
    <li> <a href="rs232-to-led.c.txt">click here</a> to view the source<br>
    As you can see it is first needed to set that led on, so the right voltage is available.<br>



    <br><br><br><br><br><br><br><br><br><br><br>
  </body>
</html>
