<html>
  <head>
    <title>day3: build prototype</title>
    <meta content="">
    <style></style>
  </head>
  <body>
    <a href="index2.php">prev</a> <a href="index.php">home</a> <a href="index4.php">next</a><br><center>day3: build prototype</center><br>
    <br>
    schematics once more<br>
    <img src="schema.png"><br>
    <br>
    now it's time for the component list and go shopping:<br>
    <br>
    <li> 1 . TLC549
    <li> 1 . MAX232 or ICL232
    <li> 1 . KTY10-6
    <li> 1 . variable resistor 5K
    <li> 1 . variable resistor 10K
    <li> 4 . resistors 10K
    <li> 4 . capacitors 1u
    <li> 1 . 9 pins sub-d connector female    <br>
    <hr><br>
    Let's make it<br>
    <img src="begin_building_small.png">

