<?php
include("phplot.php");
$graph = new PHPlot(700,400);
$mode="silent";
include("read_data.php");

//$graph->SetDataType("data-data");  //Must be called before SetDataValues
$graph->SetDataValues($stack);

//$graph->SetXDataLabelAngle(90);
//$graph->SetVertTickIncrement(1);
$graph->SetPlotAreaWorld(0,15,$end,30);
//$graph->SetXLabel("day");
$graph->SetYLabel("Temp C");
$graph->SetDrawDataLabels('1');
$graph->SetLabelScalePosition('1');


//$graph->SetHorizTickIncrement(1);
$graph->SetPlotType("lines");
/*
$graph->SetXGridLabelType("time");
$graph->SetXLabel("");
$graph->SetYLabel("Volume");
$graph->SetXTimeFormat("%b %y");
$graph->SetHorizTickIncrement(1);
//$graph->SetDrawXDataLabels(1);
*/

$graph->DrawGraph();

?>
