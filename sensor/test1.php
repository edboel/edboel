<?php
include("phplot.php");
$graph = new PHPlot(600,400);

include("read_data.php");

$graph->SetDataType("data-data");  //Must be called before SetDataValues
$graph->SetDataValues($stack);

$graph->SetXDataLabelAngle(90);
$graph->SetVertTickIncrement(1);
$graph->SetPlotAreaWorld(0,15,$en,25);
$graph->SetXLabel("day");
$graph->SetYLabel("Temp");
$graph->SetDrawDataLabels('1');
$graph->SetLabelScalePosition('1');

/*
//$graph->SetHorizTickIncrement(1);
//$graph->SetPlotType("thinbarline");

$graph->SetXGridLabelType("time");
$graph->SetXLabel("");
$graph->SetYLabel("Volume");
$graph->SetXTimeFormat("%b %y");
$graph->SetHorizTickIncrement(1);
//$graph->SetDrawXDataLabels(1);
$graph->SetPlotAreaWorld(100,0,100,90);
*/

$graph->DrawGraph();
?>
