
</html>
<center>
<h2>welcome to <a href="network/index.html#defiant">Defiant</a></h2>
<br>
<?php include ("phplot.php");


$graph = new PHPlot;

$graph->SetDataType( "linear-linear");


$graph->SetDataValues($data);



 //Specify plotting area details
$graph->SetImageArea(455,180);
$graph->SetPlotType( "lines");
$graph->SetTitleFontSize( "2");
$graph->SetTitle( "Social Security trust fund asset estimates, in $ billions");
$graph->SetPlotAreaWorld(2000,0,2035,2000);
$graph->SetPlotBgColor( "white");
$graph->SetPlotBorderType( "left");
$graph->SetBackgroundColor( "white");

 //Define the X axis
$graph->SetXLabel( "Year");
$graph->SetHorizTickIncrement( "5");
$graph->SetXGridLabelType( "plain");

 //Define the Y axis
$graph->SetVertTickIncrement( "500");
$graph->SetPrecisionY( "0");
$graph->SetYGridLabelType( "right");
$graph->SetLightGridColor( "blue");

// $graph->SetDataColors( array( "red"), array( "black") );
$graph->DrawGraph();

?>
