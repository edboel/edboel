<html>
  <head>
    <title>RS232 Temp sensor with logger</title>
    <meta content="">
    <style></style>
  </head>
  <body><center>
  <h1> Temperature Sensor and<br>online logger</h1>
  <hr><br></center>
    This little system will register the outside temperature, CPU temp, and the temp of the computer room. The data can be viewed online
     in a chart. the setup consists of easy components.<br>
      analog sensor -> a/d convertor -> RS232 Buffer -> logging program -> chart generation script. <br>
      component types: KTY10-6, TCL549, ICL232, linux C, PHP3 script<br>
   <hr><center>
   -- <b><a href="sensor/graph3.php?time=lastday">View last 48 measurings</a><br>
	 <a href="sensor/graph.php">View current graph!</a> -- <br>
	 <a href="sensor/graph.php?begin=24&end=48">View of other day</a><br>
   <br>
   <b>Day 1: <a href="sensor/index1.php">Test RS232</a></b><br>
   <b>Day 2: <a href="sensor/index2.php">draw schematics</a></b><br>
   <b>Day 3: <a href="sensor/index3.php">build prototype</a></b><br>
   <b>Day 4: <a href="sensor/index4.php">write software</a></b><br>
   <b>Day 5: <a href="sensor/index5.php">calibrate it</a></b><br>
   <b>Day 6: <a href="sensor/index6.php">documentation</a></b><br>
   <br>
  <hr><br>prototype 1<br><img width=600 src="sensor/build1.jpg">
  <hr><br>ugly schematic<br><img src="sensor/schema.png"><br>
  <hr><br>graph of monday 11 mar 2002 (first graph!)<br><img width=600 src="sensor/graph.png"><br>

   <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
  </body>
</html>
