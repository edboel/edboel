<?php
include("phplot.php");
$graph = new PHPlot(700,800);
$mode="silent";

/*

include("read_data.php");
$graph->SetPrintImage(0); //Don't draw the image yet
//$graph->SetDataType("data-data");  //Must be called before SetDataValues
$graph->SetDataValues($stack);

//$graph->SetXDataLabelAngle(90);
//$graph->SetVertTickIncrement(1);

$graph->SetNewPlotAreaPixels(90,40,640,390);

$graph->SetPlotAreaWorld(0,15,$end,30);
//$graph->SetXLabel("day");
$graph->SetYLabel("Temp C");
$graph->SetDrawDataLabels('1');
$graph->SetLabelScalePosition('1');


//$graph->SetHorizTickIncrement(1);
$graph->SetPlotType("lines");
$graph->SetTitle("Room temp");

/*
$graph->SetXGridLabelType("Room temp");
$graph->SetXLabel("");
$graph->SetYLabel("Volume");
$graph->SetXTimeFormat("%b %y");
$graph->SetHorizTickIncrement(1);
//$graph->SetDrawXDataLabels(1);
*/

 $graph->DrawGraph();


//Now do the second chart on the image
unset($example_data);
$graph->SetPrintImage(1); //Now draw the image

include("read_data3.php");
$graph->SetDataValues($stack);

$graph->SetNewPlotAreaPixels(90,480,640,750);
$graph->SetLegend(array('Room temp','Case temp','CPU temp')); //Lets have a legend

$graph->SetPlotAreaWorld(0,0,$end,50);
$graph->SetYLabel("Temp C");
$graph->SetDrawDataLabels('1');
//$graph->SetLabelScalePosition('1');

$graph->SetPlotType("lines");


$graph->DrawGraph();

$graph->PrintImage();
?>
