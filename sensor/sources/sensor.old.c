
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

void OpenDevice(char *);
void InitDaemon(void);

int main(int argc, char *argv[])
{
int fd,dtr,x,i,arg,status;

char device[255] = "/dev/ttyS0";
  fd = open(device, O_RDWR);
  if (fd == -1) {
    char s[255];
    sprintf(s, "statserial: can't open device `%s'", device);
    perror(s);
    exit(1);
  }
  //dtr=TIOCM_RTS;
  // Set RTS
  //ioctl(fd, TIOCMBIS,&dtr);
  //sleep(1);
  dtr=TIOCM_DTR;
  ioctl(fd, TIOCMBIC,&dtr);
  dtr=TIOCM_RTS;
  ioctl(fd, TIOCMBIC,&dtr);
  sleep(0.1);

  ioctl(fd, TIOCMBIS,&dtr);
  sleep(0.1);
  dtr=TIOCM_DTR;
    sleep(0.1);
  ioctl(fd, TIOCMBIS,&dtr);
  sleep(0.1);
  ioctl(fd, TIOCMBIC,&dtr);
  sleep(0.1);
  ioctl(fd, TIOCMBIS,&dtr);
  sleep(0.1);
   ioctl(fd, TIOCMBIC,&dtr);
   dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIC,&dtr);
    sleep(0.1);
    ioctl(fd, TIOCMBIS,&dtr);
    dtr=TIOCM_DTR;
   sleep(0.1);
/*  printf("Sensor v0.2 \n"
     "usage: powerdaemon [options] \n"
     "  -d  Daemon mode \n"
     "  -s X Other device then ttyS1\n"
     "  ex. -s ttyS0 \n");
  /* delay 1 second between loops */

 for(;;){
  i++;
  if(i==8){
     ioctl(fd, TIOCMBIS,&dtr);
     sleep(1);
      ioctl(fd, TIOCMBIC,&dtr);
  }
  status = ioctl(fd, TIOCMGET, &arg);
    if (status != 0) {
      printf("statserial: TIOCMGET failed");
      //exit(1);
    }
  x=!!(arg & TIOCM_CTS);
  if(x==1) printf("hoi \n");
  sleep(1);
  //    printf("value : %d \n", !!(arg & TIOCM_CTS));
  x=!!(arg & TIOCM_CTS);
  if(x==1) printf("hoi \n");
  dtr=TIOCM_DTR;
  ioctl(fd, TIOCMBIS,&dtr);
  sleep(1);
  ioctl(fd, TIOCMBIC,&dtr);
  }
 return(0);
}

void OpenDevice(char device[]){
  int fd,dtr,i;
  fd = open(device, O_RDWR);
  if (fd == -1) {
    char s[255];
    sprintf(s, "statserial: can't open device `%s'", device);
    perror(s);
    exit(1);
  }
  dtr=TIOCM_RTS;
  // Set RTS
  ioctl(fd, TIOCMBIS,&dtr);
}


void InitDaemon() {
  int pid;
  pid = fork();
  if ( pid < 0 )
    perror("Daemon init failed - program abort");
  if ( pid > 0 ) {
    //printf("Daemon started with PID= %s \n",pid);
    exit(1);
  }
  chdir("/");
  umask(022);

}

