#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>

int main(int argc, char *argv[])
{
int fd,dtr,t,x,i=0,arg,status,sleeptime,data[10];
float temp;
FILE *fp,*fopen();
struct timeval tv;
struct timezone tz;
//char *asctime();
char filename[255] = "/home/eddo/sensor/temp.log";
char device[255] = "/dev/ttyS0";
sleeptime=1800;                 // 1800 seconds is 30 mins
  fd = open(device, O_RDWR);
  if (fd == -1) {
    char s[255];
    sprintf(s, "statserial: can't open device `%s'", device);
    perror(s);
    exit(1);
  }
  printf("%s is open\n",device);
  dtr=TIOCM_DTR;
  ioctl(fd, TIOCMBIC,&dtr);
  dtr=TIOCM_RTS;
  ioctl(fd, TIOCMBIC,&dtr);
  sleep(1);
  for(;;){
    i++;
    printf("device cleared\n");
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIS,&dtr);
    printf("start receiving... \n");
    for(t=0;t!=10;t++){
      ////////////////////  receive
      status = ioctl(fd, TIOCMGET, &arg);
        if (status != 0) {
          printf("statserial: TIOCMGET failed");
	  exit(1);
         }
      x=!!(arg & TIOCM_CTS);
      data[t]=x;
      ////////////////// clock
       dtr=TIOCM_DTR;
      ioctl(fd, TIOCMBIS,&dtr);
      sleep(1);
      ioctl(fd, TIOCMBIC,&dtr);
      sleep(1);
    }
    /////////////done
    printf("received data: %d%d%d%d%d%d%d%d ",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
    x = data[0]*128 + data[1]*64 + data[2]*32 + data[3]*16 + data[4]*8 + data[5]*4  + data[6]*2 + data[7];
    printf(" - decimal value: %d \n",x);
    temp = 200-x;
    temp = temp/5;
    //temp = roundf(temp);
    printf(" - temp: %f ",temp);

    gettimeofday(&tv, &tz);
    printf(" - time: %s ", asctime(localtime(&tv.tv_sec)));
    if(i==1) printf("testrun complete ");
    else{
      close(fd);
      fp = fopen(filename,"a");
      if (fp == NULL) {
          sprintf("statserial: can't open file `%s'", filename);
          //perror(s);
  	  exit(1);
      }
      fprintf(fp,"%f ",temp);
      fprintf(fp,"%s",asctime(localtime(&tv.tv_sec)));
      /*if(write(fp,&x,sizeof(int))==-1){
          sprintf("statserial: write to file `%s'", filename);
          exit(1);
      }*/
      fclose(fp);
      printf("data is written on %s\n", filename);
      printf("data hold for %d seconds \n",sleeptime);
      fd = open(device, O_RDWR);
    }
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIS,&dtr);
    sleep(1);
      dtr=TIOCM_DTR;
      ioctl(fd, TIOCMBIS,&dtr);
      sleep(1);
      ioctl(fd, TIOCMBIC,&dtr);
      sleep(1);
      dtr=TIOCM_DTR;
      ioctl(fd, TIOCMBIS,&dtr);
      sleep(1);
      ioctl(fd, TIOCMBIC,&dtr);
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIC,&dtr);
    sleep(sleeptime);
  }
}

