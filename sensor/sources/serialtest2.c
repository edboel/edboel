#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/* global variables */
char device[255]= "/dev/ttyS0";

int main()
{
  int fd,dtr,cho;
  unsigned int arg;

     /* open port */
  fd = open(device, O_RDWR);
  if (fd == -1) {
    char s[255];
    sprintf(s, "error: can't open device `%s'", device);
    perror(s);
    exit(1);
  }

  /*clear DTR bit */
 	dtr=TIOCM_DTR;
 	ioctl(fd, TIOCMBIC,&dtr);

  for (;;) {
  /* show port status */
 printf("RTS port status: %d \n", TIOCM_RTS);
 printf("Data Term ready?  %d \n", !!(arg & TIOCM_DTR));
 printf("RTS value?  %d \n\n", !!(arg & TIOCM_RTS));

 printf("open port:\n");
 printf("1 ->  pin 3	 Transmit Data  TIOCM_ST\n");
 printf("2 ->  pin 4	Data Terminal Ready TIOCM_DTR  %d\n", !!(arg & TIOCM_DTR));
 printf("3 ->  pin 7	Request to send TIOCM_RTS\n\n");

 printf("close port\n");
 printf("5 ->  pin 3	 Transmit Data  TIOCM_ST\n");
 printf("6 ->  pin 4	Data Terminal Ready TIOCM_DTR  %d \n", !!(arg & TIOCM_DTR));
 printf("7 ->  pin 7	Request to send TIOCM_RTS\n\n");

 printf("9 switch device (current %s)\n\n", device);
 printf("0 -> exit\n");
scanf("%d", &cho);
switch(cho)
{
	case 1:
	 /*set ST bit */
 	dtr=TIOCM_ST;
 	ioctl(fd, TIOCMBIS,&dtr);
	break;
	case 2:
 	/*set DTR bit */
 	dtr=TIOCM_DTR;
 	ioctl(fd, TIOCMBIS,&dtr);
	break;
	case 3:
	 /*set RTS bit */
 	dtr=TIOCM_RTS;
 	ioctl(fd, TIOCMBIS,&dtr);
	break;
	case 5:
	/*clear ST bit */
 	dtr=TIOCM_ST;
 	ioctl(fd, TIOCMBIC,&dtr);
	break;
	case 6:
	/*clear DTR bit */
 	dtr=TIOCM_DTR;
 	ioctl(fd, TIOCMBIC,&dtr);
	break;
	case 7:
	/*clear RTS bit */
 	dtr=TIOCM_RTS;
 	ioctl(fd, TIOCMBIC,&dtr);
	break;
	case 9:
	if (device == "/dev/ttyS0"){
		printf("changing\n");
		device == "/dev/ttyS1";
		}
	else {
		printf("back\n");
		device == "/dev/ttyS0";
		}
	printf("closing device...\n");
	close(fd);
	 printf(" opening device %s \n", device);
	fd = open(device, O_RDWR);
	if (fd == -1) {
		char s[255];
		sprintf(s, "error: can't open device `%s'", device);
		perror(s);
		exit(1);
	}
	break;
	case 0:
	printf("closing port...\n");
	close(fd);
	exit(1);
	break;
	}

 printf("Data Term ready?  %d \n", !!(arg & TIOCM_DTR));
 printf("RTS port value: %d \n", !!(arg & TIOCM_RTS));
 }
 printf("closing port...\n");
 close(fd);
 return 0;
}

