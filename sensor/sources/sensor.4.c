/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>

int main(int argc, char *argv[])
{
int fd,dtr,i,t,x,prevx,arg,status,sleeptime,data[10];
float temp;
FILE *fp,*fopen();
struct timeval tv;
struct timezone tz;
char filename[255] = "/web/html/sensor/temp.log";
char device[255] = "/dev/ttyS0";
sleeptime=1800;                 // 1800 seconds is 30 mins
for (i=0;i!=2;i++){
  x=0;
  prevx=1;
  fd = open(device, O_RDWR);
  if (fd == -1) {
    char s[255];
    sprintf(s, "statserial: can't open device `%s'", device);
    perror(s);
    exit(1);
  }
  while(x!=prevx){
//   printf(" x was %d prevx was %d:  retry \n",x,prevx);
    prevx=x;                     // copy value to preveous before receiving
    dtr=TIOCM_DTR;
    ioctl(fd, TIOCMBIC,&dtr);
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIC,&dtr);
    sleep(1);
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIS,&dtr);
    usleep(10);
    for(t=0;t!=10;t++){
      ////////////////////  receive
      status = ioctl(fd, TIOCMGET, &arg);
        if (status != 0) {
          printf("statserial: TIOCMGET failed");
          exit(1);
         }
      x=!!(arg & TIOCM_CTS);
      data[t]=x;
      ////////////////// clock
       dtr=TIOCM_DTR;
      ioctl(fd, TIOCMBIS,&dtr);
      usleep(10);
      ioctl(fd, TIOCMBIC,&dtr);
      usleep(10);
      }
    /////////////done
    x = data[0]*128 + data[1]*64 + data[2]*32 + data[3]*16 + data[4]*8 + data[5]*4  + data[6]*2 + data[7];
    temp = 190-x;
    temp = temp/4.1;

    gettimeofday(&tv, &tz);
  }

  close(fd);
  fp = fopen(filename,"a");
  if (fp == NULL) {
        printf("statserial: can't open file `%s'", filename);
        exit(1);
    }
  fprintf(fp,"%f ",temp);
  fprintf(fp,"%s",asctime(localtime(&tv.tv_sec)));
  fclose(fp);
  fd = open(device, O_RDWR);

  dtr=TIOCM_RTS;
  ioctl(fd, TIOCMBIS,&dtr);
  usleep(1);
    dtr=TIOCM_DTR;
    ioctl(fd, TIOCMBIS,&dtr);
    usleep(1);
    ioctl(fd, TIOCMBIC,&dtr);
    usleep(1);
    dtr=TIOCM_DTR;
    ioctl(fd, TIOCMBIS,&dtr);
    usleep(1);
    ioctl(fd, TIOCMBIC,&dtr);
  dtr=TIOCM_RTS;
  ioctl(fd, TIOCMBIC,&dtr);
  sleep(sleeptime);
  }
}

