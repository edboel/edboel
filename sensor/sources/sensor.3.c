<?php
# sensor.c  v0.2
# begin: 7 mar 2002 by e.boelen
# a program that reads data from a tlc549 ad converter and writes it to a logfile

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

void OpenDevice(char *);
void InitDaemon(void);

int main(int argc, char *argv[])
{
int fd,dtr,t,x,i,arg,status,data[10];
FILE *fp,*fopen();
char filename[255] = "/home/eddo/.temp.log";
char device[255] = "/dev/ttyS0";
  fd = open(device, O_RDWR);
  if (fd == -1) {
    char s[255];
    sprintf(s, "statserial: can't open device `%s'", device);
    perror(s);
    exit(1);
  }
  for(;;){
    dtr=TIOCM_DTR;
    ioctl(fd, TIOCMBIC,&dtr);
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIC,&dtr);
    sleep(1);
    printf("cleared\n");
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIS,&dtr);
    printf("start receiving... \n");
    for(t=0;t!=10;t++){
      ////////////////////  receive
      status = ioctl(fd, TIOCMGET, &arg);
        if (status != 0) {
          printf("statserial: TIOCMGET failed");
	  exit(1);
         }
      x=!!(arg & TIOCM_CTS);
      if(x==1) printf("+");
      else printf(".");
      data[t]=x;
      ////////////////// clock
       dtr=TIOCM_DTR;
      ioctl(fd, TIOCMBIS,&dtr);
      sleep(1);
      ioctl(fd, TIOCMBIC,&dtr);
      sleep(1);
    }
    /////////////done
    printf("\n received data: %d%d%d%d%d%d%d%d \n",data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
    x = data[0]*128 + data[1]*64 + data[2]*32 + data[3]*16 + data[4]*8 + data[5]*4  + data[6]*2 + data[7];
    printf("decimal value: %d \n",x);
    //x = x-100;
    close(fd);
    fp = fopen(filename,"a");
    if (fp == NULL) {
        sprintf("statserial: can't open file `%s'", filename);
        //perror(s);
	exit(1);
    }
    fprintf(fp,"%d\n",x);
    /*if(write(fp,&x,sizeof(int))==-1){
        sprintf("statserial: write to file `%s'", filename);
        exit(1);
    }*/
    fclose(fp);
    printf("data is written\n");
    fd = open(device, O_RDWR);
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIS,&dtr);
    sleep(1);
      dtr=TIOCM_DTR;
      ioctl(fd, TIOCMBIS,&dtr);
      sleep(1);
      ioctl(fd, TIOCMBIC,&dtr);
      sleep(1);
      dtr=TIOCM_DTR;
      ioctl(fd, TIOCMBIS,&dtr);
      sleep(1);
      ioctl(fd, TIOCMBIC,&dtr);
    dtr=TIOCM_RTS;
    ioctl(fd, TIOCMBIC,&dtr);
    sleep(1);
  }
}

?>
