/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * avr-gcc -mmcu=atmega88 -Os -o clock.elf clock.c 
 * avr-objcopy -j .text -O ihex clock.elf clock.hex
 * sudo avrdude -c usbasp -p m88 -U flash:w:clock.hex:i -U lfuse:w:0xe2:m -U hfuse:w:0xdf:m 
 */
#define F_CPU 8000000UL  /* 8 MHz CPU clock */

#include<util/delay.h>
#include<avr/io.h>
#include<avr/interrupt.h>
//#include<avr/signal.h>

#define CLK PC0
#define D1 PC3
#define D2 PC2
#define D3 PC1
#define ENABLE PC4
#define CLR PC5


/*
SIGNAL(SIG_PCINT) {
  PORTD &= ~_BV(PD6);
   _delay_ms(2000);
}
*/

void init(void){
     DDRC |=(0xFF); /* C output */
     PORTC |=(0x00); /* C pins hi */
     
     DDRD = 0x00; //Set all pins input
     PORTD = 0xFF;  //enable pull up resistor...
	
    /* PCMSK |= (1<<PCINT16); //  tell pin change mask to listen to pin16 (PD0)
     PCMSK |= (1<<PCINT17); //  tell pin change mask to listen to pin17 (PD1)
     GIMSK  |= (1<<PCIE); // enable PCINT interrupt in the general interrupt mask
 
     sei();
  */
}

void clk(void){
      PORTC |= _BV(CLK);
      _delay_ms(5);
      PORTC &= ~_BV(CLK);
      _delay_ms(5);
      
      
}

void oreset(void){
    PORTC |= _BV(CLR); //on
    PORTC |= _BV(CLK);
    _delay_ms(5);
    PORTC &= ~_BV(CLK);
    _delay_ms(5);

}

void int2bin(int val, char *buffer){
       //char buffer[8];
       int i=0;
       for(i=0;i<6;i++){
        if(val & 0x01){
	  buffer[i]='1';
	}else{
	  buffer[i]='0';
	}
	val >>= 1; // shift right
       }
}

void sendData(char *csec, char *cmin, char *chou){
  oreset();
  int i=0;
  for(i=0;i<7;i++){
    PORTC &= ~_BV(D1); //off
    PORTC &= ~_BV(D2); //off
    PORTC &= ~_BV(D3); //off
	
    if(csec[i]=='1'){
	PORTC |= _BV(D1);
    }
    if(cmin[i]=='1'){
	PORTC |= _BV(D2);
    }
    if(chou[i]=='1'){
	PORTC |= _BV(D3);
    }
    clk();
  }
  PORTC |= _BV(ENABLE); //on
  _delay_ms(10);
  PORTC &= ~_BV(ENABLE); //off
  //_delay_ms(900);
}

int checkButton(int btn, int time){
    if(bit_is_clear(PIND,btn)){
      time++;
    }
    return time;
}

int checkMButton(int time){
    if(bit_is_clear(PIND,PD0)){
      time++;
      _delay_ms(100);
    }
    return time;
}

int checkHButton(int time){
    if(bit_is_clear(PIND,PD1)){
      time++;
      _delay_ms(100);
    }
    return time;
}

int main( void )
{
    init();
       int sec=0;
       int min=0;
       int hour=0;
     
    for (;;){
       
       char binsec[6];
       char binmin[6];
       char binhou[6];
       int i=0;
       
       int2bin(sec,binsec);
       int2bin(min,binmin);
       int2bin(hour,binhou);
	
       sendData(binsec,binmin,binhou);
       for(i=0;i<9;i++){
	 min=checkMButton(min);
	 hour=checkHButton(hour);
	 _delay_ms(100);
       }
       
       sec++;
       
       if(sec>59){
	 sec=0;
	 min++;
       }
       if(min>59){
	 min=0;
	 hour++; 
       }
       if(hour>23){
	 hour=0;
       }
    }


}