#!/usr/bin/env python
#
#
#Copyright 2008 Edsard Boelen
 
import usb
import random
import time

class UsbDevice:
  def __init__(self,vendor_id,product_id):  
    busses = usb.busses()
    self.handle = None
    pscmd_on = 0x02
    pscmd_off = 0x03
    for bus in busses:
      devices = bus.devices
      for dev in devices:
        print "Device:", dev.filename
        print "  idVendor:",dev.idVendor
        print "  idProduct:",dev.idProduct
        if dev.idVendor==vendor_id and dev.idProduct==product_id:
            print "  found dev !"
            self.dev = dev
            self.handle = self.dev.open()
            return

class UsbControl:
  # USB_ENDPOINT_IN = 0x80 = 10000000
  # USB_TYPE_VENDOR = 0x40 = 01000000
  # combined = 11000000 is 192
  requestType = 0xc0
  lbuffer = (0,0,0,0,0,0,0,0)
 
  def __init__(self):
    # current vendor
    vendor_id = 5824
    product_id = 1500
    print " init dev "
    self.dev=UsbDevice(vendor_id, product_id)

  def turnOn(self,port):  
    # request is from the device itself,
    request = 0x02
    # value is the delay time
    value = 0
    self.dev.handle.controlMsg(self.requestType, request, self.lbuffer, value, port, timeout=100)

  def turnOff(self,port):  
    request = 0x03
    value = 0
    self.dev.handle.controlMsg(self.requestType, request, self.lbuffer, value, port, timeout=100)

  def twinkle(self,port):
    request = 0x04
    value = 0
    self.dev.handle.controlMsg(self.requestType, request, self.lbuffer, value, port, timeout=100)


if __name__ == '__main__':
  print "begin "
  uctrl=UsbControl()
  for i in range(0, 8):
    UsbControl.turnOn(uctrl,i)

  while (True):
      for i in range(0, 8):
        lchoice = random.choice([0,1])
        if lchoice == 0:
	  UsbControl.twinkle(uctrl,i)
	if lchoice == 1:
	  print "hoi "
      time.sleep(1)



  

