<html>
<head><title>temp sensor project 2 </title>
</head>
<br>
<center>
<h2>Sensor2</h2>
<h3>multiple temp sensors</h3>
</center>
<br>
Edsard Boelen, 02-march-2007
<hr><i>
this project is to record the CPU, room and outside temperature and show it in one graph. This time, the digital, factory calibrated DS1820 IC's are used. 
</i><hr>
<br>
<a href="graph.php?timep=week"> see past week graph</a><br>
<a href="graph.php?timep=24h"> see past 24 hours</a><br>
<a href="graph.php?timep=all"> see all time graph</a><br>
<br>
<h3>the idea</h3>
the ds1820 uses the dallas 1wire protocol, 
the time window of the data is 50us,
 which is just too fast for a non-realtime multitasking OS. 
These days, microcontrollers are very cheap and advanced, 
so let them do the task of converting the 1wire data to simple rs232 digits
 with the temp. <br>
the data is logged in a plain text file, and phplot can draw nice graphs from it.
<Br>
<h3>the interface</h3>
I used quozl's temp sensor project with the PIC12C509, see
<a href="http://quozl.us.netrek.org/ts/">http://quozl.us.netrek.org/ts/</a> <br>
a picture of the board:<br>
<img src="interface.jpg"><br>
the outside sensor<br>
<img src="outside_sensor.jpg" /><br>
<br>
<h3>the temp logger</h3>
At the moment the device is connected to the serial port, 
and the serial port is enabled, it will send the data at 2400 baud. 
So in fact cat /dev/ttyS0 or /dev/ttyUSB0 will show you a readout every second.
 If not, then check the com port. for example run 
<a href="com.c">comtest</a> or set the tty speed with stty -F /dev/ttyS0 2400 
<br><br>
I wrote quickly a C app which will be run every 30 minutes by the cron daemon<br>
<div name="lcode"> </div>
<a href="templog.c.html">templog.c</a><br>
<?php
  include("templog.c.html");
?>
<br>

<br>
<h3>the image generator</h3>
I used <a href="http://www.jeo.net/php/phplot/doc/">phplot</a> 
for the image generation, the source codes are:<br>
<Br>
<?php
  show_source("graph.php");
  echo "<br><br><BR> and read_data is<br> ";
  show_source("show_data.php");
?><br>
<Br>


 
