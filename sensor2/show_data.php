<?php
  $stack = array();
  $lfile = "***.log";
  $data = file($lfile);
 
  $cend = count($data);
  $cbegin = 0;
  if (($timep=="24h") && ($cend > 100))
  { 
	$cbegin = $cend-48;
  } 
  if (($timep=="week") && ($cend > 400))
  {
	$cbegin = $cend-336;  
  }

  $step = ($cend-$cbegin)/30;
  $lcnt = $step;
  $i=$cbegin;
  for($i; $i<$cend;$i++)
  {
    $line = $data[$i];

    $expl = explode("|",$line);
    if ($lcnt >= $step)
    {
      
      $outformat = '%d %B %H:%M';
      $tstamp = mktime($expl[2], $expl[3],0,1,$expl[1],2007);   
      $dd = strftime($outformat, $tstamp);
      array_push($stack, array($dd,$expl[4],$expl[5],trim($expl[6])));  
      $lcnt=0;
    }
    else
    { 
       array_push($stack, array(" ",$expl[4],$expl[5],trim($expl[6])));
      $lcnt++;
    }
    if ($mode != "silent")
      echo "$step $expl[4]  -  $expl[5] - $expl[6] <br> ";
  } 
?>
