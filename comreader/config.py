import json
import glob

from serial import *
from serial import SerialException

class Config():
    #self.config = StrVar()
    def __init__(self):
        self.reload()
        

    def reload(self):
        self.data = json.load(open('config.json', 'r'))
        serports = self.serial_ports()
        if len(serports) == 0:
            print("serial ports not found")
            self.data['Com1']['enabled'] = "False"
            self.data['Com2']['enabled'] = "False"

    def store(self):
        json.dump(self.data,open('config.json', 'w'), indent=2)
        print("saved")

    def serial_ports(self):
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = Serial(port)
                s.close()
                result.append(port)
            except (OSError, SerialException):
                pass
        return result
