from tkinter import *
from tkinter import messagebox

from config import Config
from inputSettingsForm import InputSettingsForm

import datetime
import csv

class ReaderGui(Frame):
    
    # Define settings upon initialization. Here you can specify
    def __init__(self,master, config):
        #master=None
        # parameters that you want to send through the Frame class. 
        Frame.__init__(self, master)   

        #reference to the master widget, which is the tk window                 
        self.master = master
        self.config = config

        #print (config.data['Timestamp']['enabled'])
        
        self.init_menu()
        self.init_screen()

    def init_menu(self):
        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        file = Menu(menu)
        file.add_command(label="Exit", command=self.client_exit)
        menu.add_cascade(label="File", menu=file)

        run = Menu(menu)
        run.add_command(label="Start", command=self.setInputs)
        run.add_command(label="Pause", command=self.setInputs)
        menu.add_cascade(label="Run", menu=run)

        settings = Menu(menu)
        settings.add_command(label="Set inputs", command=self.setInputs)
        settings.add_command(label="Set output", command=self.setOutputs)
        menu.add_cascade(label="Settings", menu=settings)

        about = Menu(menu)
        about.add_command(label="about", command=self.showAbout)
        menu.add_cascade(label="About", menu=about)

        

    #Creation of main_window
    def init_screen(self):
        self.master.title("ComReader")
        self.pack(fill=BOTH, expand=1)

        self.status = Label(self, text="ready…", bd=1, relief=SUNKEN, anchor=W)
        self.status.pack(side=BOTTOM, fill=X)

        self.writeButton = Button(self, text="Write log", command=self.write_log, bg="green2",height=2)
        self.writeButton.pack(side=BOTTOM, fill=X)

        self.time_frame = Frame(self, width=200, bg="lightgrey")
        self.time_frame.pack_propagate(0)

        self.scanner_frame = Frame(self, width=200, bg="grey")
        self.scanner_frame.pack_propagate(0)
        
        self.com1_frame = Frame(self, width=200, bg="lightgrey")
        self.com1_frame.pack_propagate(0)

        self.com2_frame = Frame(self, width=200, bg="grey")
        self.com2_frame.pack_propagate(0)

        self.right_frame = Frame(self, width=400, bg="lightgrey")
        self.right_frame.pack_propagate(0)


        self.lbf_timestamp = LabelFrame(self.time_frame, text="Timestamp")
        self.lbf_timestamp.pack(expand=1)
        self.timestamp = Text(self.lbf_timestamp,height=1,bg="white")
        self.timestamp.insert(INSERT,"00:00:00")
        self.timestamp.pack(expand=1)

        self.lbf_scanner = LabelFrame(self.scanner_frame, text="Scanner")
        self.lbf_scanner.pack(expand=1)
        self.scanner = Text(self.lbf_scanner,height=1,bg="white")
        self.scanner.insert(INSERT," ")
        self.scanner.pack(expand=1)
        self.scanner.focus_set()

        self.lbf_com1 = LabelFrame(self.com1_frame, text="Com1")
        self.lbf_com1.pack(expand=1)
        self.text_com1 = Text(self.lbf_com1,height=1,bg="white")
        self.text_com1.insert(INSERT," ")
        self.text_com1.pack(expand=1)

        self.lbf_com2 = LabelFrame(self.com2_frame, text="Com2")
        self.lbf_com2.pack(expand=1)
        self.text_com2 = Text(self.lbf_com2,height=1,bg="white")
        self.text_com2.insert(INSERT," ")
        self.text_com2.pack(expand=1)

        self.time_frame.pack(side=LEFT, fill=BOTH)
        self.scanner_frame.pack(side=LEFT, fill=BOTH)
        self.com1_frame.pack(side=LEFT, fill=BOTH)
        self.com2_frame.pack(side=LEFT, fill=BOTH)
        self.right_frame.pack(side=LEFT, expand=1, fill=BOTH)

        
        

    def setInputs(self):
        settingsform = InputSettingsForm(self,self.config)

    def setOutputs(self):
        settingsform = OutputSettingsForm(self,self.config)

    def showAbout(self):
        self.status.config(text="about")
        messagebox.showinfo("About:", "ComReader v0.1")
        
    def readPort(self):
        self.status.config(text="proc")

    def write_log(self):
        row = [self.timestamp.get("1.0",END).rstrip(), self.scanner.get("1.0",END).rstrip(), self.text_com1.get("1.0",END).rstrip(), self.text_com2.get("1.0",END).rstrip()]
        with open(self.config.data['csv']['file'], 'a') as csvFile:
            writer = csv.writer(csvFile)
            writer.writerow(row)

        csvFile.close()
        self.status.config(text="log written")

    def client_exit(self):
        exit()

    def updateValues(self):
        now = datetime.datetime.now()
        self.timestamp.delete("1.0",END)
        self.timestamp.insert(INSERT,now.strftime(self.config.data['Timestamp']['format']))#"20:00:10")
        
        self.text_com1.delete("1.0",END)
        if self.config.data['Com1']['enabled'] == "True":
            self.readSerial(com1,self.text_com1)
        #self.text_com1.insert(INSERT,readSerial(com1))#"20:00:10")

        #self.text_com2.delete("1.0",END)
        #self.text_com1.insert(INSERT,readSerial(com2))#"20:00:10")
        
        self.after(500, self.updateValues)

    def readSerial(self,ser,txtbox):
        while True:
            c = ser.read().decode(sys.stdout.encoding)
            if len(c) == 0:
                break
            global serBuffer
            if c == '\r':
                c = '' # don't want returns. chuck it
            if c == '\n':
                serBuffer += "\n"
                #log.insert('0.0', serBuffer)
                txtbox.insert(INSERT,serBuffer)
                serBuffer = "" # empty the buffer
            else:
                serBuffer += c

