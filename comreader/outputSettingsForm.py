from tkinter import *
from tkinter import messagebox
from config import Config

class OutputSettingsForm(Toplevel):
    def __init__(self, master, config):
        super().__init__(master)
        self.config=config
        self.csvEnabled = BooleanVar(value=self.config.data['csv']['enabled'])

        self.title("Set output")
        self.main_frame =  Frame(self, bg="lightgrey")

        self.lbf_csv = LabelFrame(self.main_frame, text="CSV")
        self.lbf_csv.pack(expand=1)
        self.enable_csv = Checkbutton(self.lbf_csv,variable=self.csvEnabled, \
                                            onvalue = True, offvalue = False,bg="white", text="enable csv output")
        self.enable_csv.pack(expand=1)
        self.lbl_csv =  Label(self.lbf_csv, text="output file", bg="lightgrey", fg="black")
        self.lbl_csv.pack(side=LEFT)
        self.csv = Text(self.lbf_csv,height=1,bg="white")
        self.csv.insert(INSERT,self.config.data['csv']['file'])
        self.csv.pack(expand=1,side=RIGHT,fill=X)

        
        
        self.submit_button =  Button(self.main_frame, text="Save settings", command=self.storeSettings)
        self.submit_button.pack(side= TOP, fill= X, pady=(10,0), padx=10)
        self.cancel_button =  Button(self.main_frame, text="Cancel", command=self.reloadSettings)
        self.cancel_button.pack(side= TOP, fill= X, pady=(10,0), padx=10)
        
        self.main_frame.pack(expand=1, fill= BOTH)
        #self.submit_button.pack(side= TOP, fill= X, pady=(10,0), padx=10)

    def reloadSettings(self):
        config.reload()
        self.destroy()

    def storeSettings(self):
        self.config.data['csv']['enabled'] = str(self.csvEnabled.get())
        self.config.data['csv']['file'] = self.csv.get("1.0",END).rstrip()
       
        
        config.store()
        self.destroy()
