from tkinter import *
from tkinter import messagebox

from config import Config


class InputSettingsForm(Toplevel):
    def __init__(self, master, config):
        super().__init__(master)
        self.config = config
        
        SPEEDOPTIONS = (1200,2400,4800,9600,19200,57600,115200)
        
        self.timestampEnabled = BooleanVar(value=config.data['Timestamp']['enabled'])
        self.scannerEnabled = BooleanVar(value=config.data['Scanner']['enabled'])
        self.scannerFocus = BooleanVar(value=config.data['Scanner']['enabled'])
        self.com1Enabled = BooleanVar(value=config.data['Com1']['enabled'])
        self.com2Enabled = BooleanVar(value=config.data['Com2']['enabled'])
        self.com1Speed = IntVar(value=config.data['Com1']['speed'])
        self.com2Speed = IntVar(value=config.data['Com2']['speed'])

        self.title("Set inputs")
        self.main_frame =  Frame(self, bg="lightgrey")

        self.lbf_timestamp = LabelFrame(self.main_frame, text="Timestamp")
        self.lbf_timestamp.pack(expand=1)
        self.enable_timestamp = Checkbutton(self.lbf_timestamp,variable=self.timestampEnabled, \
                                            onvalue = True, offvalue = False,bg="white", text="enable Timestamp")
        self.enable_timestamp.pack(expand=1)
        self.lbl_timestamp =  Label(self.lbf_timestamp, text="Time format", bg="lightgrey", fg="black")
        self.lbl_timestamp.pack(side=LEFT)
        self.timestamp = Text(self.lbf_timestamp,height=1,bg="white")
        self.timestamp.insert(INSERT,config.data['Timestamp']['format'])
        self.timestamp.pack(expand=1,side=RIGHT,fill=X)

        self.lbf_scanner = LabelFrame(self.main_frame, text="Scanner")
        self.lbf_scanner.pack(expand=1,fill= BOTH)
        self.enable_scanner = Checkbutton(self.lbf_scanner,variable=self.scannerEnabled, onvalue = True, offvalue = False,bg="white", text="enable scanner")
        self.enable_scanner.pack(expand=1)
        self.focus_scanner = Checkbutton(self.lbf_scanner,variable=self.scannerFocus, onvalue = True, offvalue = False,bg="white", text="autofocus scanner")
        self.focus_scanner.pack(expand=1)

        self.lbf_com1 = LabelFrame(self.main_frame, text="Com1")
        self.lbf_com1.pack(expand=1,fill= BOTH)
        self.enable_com1 = Checkbutton(self.lbf_com1,variable=self.com1Enabled, onvalue = True, offvalue = False,bg="white", text="enable com1")
        self.enable_com1.pack(expand=1)

        self.fr_port_com1= Frame(self.lbf_com1)
        self.lbl_port_com1 =  Label(self.fr_port_com1, text="Port", bg="lightgrey", fg="black")
        self.lbl_port_com1.pack(side=LEFT)
        self.port_com1 = Text(self.fr_port_com1,height=1,bg="white")
        self.port_com1.insert(INSERT,config.data['Com1']['port'])
        self.port_com1.pack(expand=1,side=RIGHT,fill= BOTH)

        self.fr_speed_com1= Frame(self.lbf_com1)
        self.lbl_speed_com1 =  Label(self.fr_speed_com1, text="Speed", bg="lightgrey", fg="black")
        self.lbl_speed_com1.pack(side=LEFT)
        self.speed_com1 = OptionMenu(self.fr_speed_com1,self.com1Speed, *SPEEDOPTIONS)
        self.speed_com1.pack(expand=1,side=RIGHT)
        self.fr_port_com1.pack()
        self.fr_speed_com1.pack()

        self.lbf_com2 = LabelFrame(self.main_frame, text="Com2")
        self.lbf_com2.pack(expand=1,fill= BOTH)
        self.enable_com2 = Checkbutton(self.lbf_com2,variable=self.com2Enabled, \
                                          onvalue = True, offvalue = False,bg="white", text="enable com2")
        self.enable_com2.pack(expand=1)

        self.lbl_port_com2 =  Label(self.lbf_com2, text="Port", bg="lightgrey", fg="black")
        self.lbl_port_com2.pack()
        self.port_com2 = Text(self.lbf_com2,height=1,bg="white")
        self.port_com2.insert(INSERT,config.data['Com2']['port'])
        self.port_com2.pack(expand=1,fill= BOTH)
        
        self.speed_com2 = OptionMenu(self.lbf_com2,self.com2Speed, *SPEEDOPTIONS)
        self.speed_com2.pack(expand=1)
        
        self.submit_button =  Button(self.main_frame, text="Save settings", command=self.storeSettings)
        self.submit_button.pack(side= TOP, fill= X, pady=(10,0), padx=10)
        self.cancel_button =  Button(self.main_frame, text="Cancel", command=self.reloadSettings)
        self.cancel_button.pack(side= TOP, fill= X, pady=(10,0), padx=10)
        
        self.main_frame.pack(expand=1, fill= BOTH)

    def reloadSettings(self):
        self.config.reload()
        self.destroy()

    def storeSettings(self):
        self.config.data['Timestamp']['enabled'] = str(self.timestampEnabled.get())
        self.config.data['Timestamp']['format'] = self.timestamp.get("1.0",END).rstrip()
        self.config.data['Scanner']['enabled'] = str(self.scannerEnabled.get())
        self.config.data['Scanner']['autofocus'] = str(self.scannerFocus.get())
        self.config.data['Com1']['enabled'] = str(self.com1Enabled.get())
        self.config.data['Com1']['port'] = self.port_com1.get("1.0",END).rstrip()
        self.config.data['Com1']['speed'] = str(self.com1Speed.get())
        self.config.data['Com2']['enabled'] = str(self.com2Enabled.get())
        self.config.data['Com2']['port'] = self.port_com2.get("1.0",END).rstrip()
        self.config.data['Com2']['speed'] = str(self.com2Speed.get())
        
        self.config.store()
        self.destroy()
