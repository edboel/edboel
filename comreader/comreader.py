from tkinter import *
from tkinter import messagebox

from config import Config
from readerGui import ReaderGui


## temporary
root = Tk()
#statusprompt = StringVar()
root.geometry("850x300")

config = Config()

print ("config loaded")
serBuffer = ""
if config.data['Com1']['enabled'] == "True":
    com1 = Serial(config.data['Com1']['port'] , config.data['Com1']['speed'], timeout=0, writeTimeout=0) #ensure non-blocking
if config.data['Com2']['enabled'] == "True":
    com2 = Serial(config.data['Com2']['port'] , config.data['Com2']['speed'], timeout=0, writeTimeout=0) #ensure non-blocking
#creation of an instance
app = ReaderGui(root,config)

root.after(500, app.updateValues)

#mainloop 
root.mainloop() 
