/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <LiquidCrystal_SR.h>
#include <SPI.h>
#include <Ethernet.h>

/************ ETHERNET STUFF ************/
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xFF, 0xFE, 0xED };
byte serverip[] = { 192, 168, 10, 102 }; // home
EthernetClient  client;


LiquidCrystal_SR srlcd(2, 3, TWO_WIRE);

#define RXD        0
#define TXD        1
#define LCD1       2
#define LCD2       3
#define SELECT_BTN 4
#define UP_BTN     5
#define DOWN_BTN   6
#define BEEPER     7
#define ALARM      8
#define STATUS_LED 9

#define DOOR1      A0
#define DOOR2      A1
#define WINDOW1    A2
#define WINDOW2    A3
#define PIR1       A4
#define PIR2       A5

#define PRE_ALARMT 600 // 30sec to deactivate
#define ALARM_TIME 3000 // alarm for 2.5 minutes
#define DEACT_TIME 80 // press buttons 4 sec to turn of alarm
#define PIR_TIME 32 // 1.6 sec to detect movement
#define PIR_COOLDOWN 6000 //don't spam with pir messages, one is enough.
#define CONNECT_TIME 6000 //check internet every 5 minutes

boolean systemOn = false;
boolean network = false;
boolean door1Open = false;
boolean door2Open = false;
boolean window1Open = false;
unsigned int alarmTime=0;
unsigned int preAlarm=0;
unsigned int deactTime=80;
unsigned int pir1On=0;
unsigned int connectTimer=0;
unsigned int pirCoolDown=0;

void setup() {
  
  pinMode(LCD1, OUTPUT); 
  pinMode(LCD2, OUTPUT);
  pinMode(SELECT_BTN, INPUT); 
  pinMode(UP_BTN, INPUT);
  pinMode(DOWN_BTN, INPUT); 
  pinMode(BEEPER, OUTPUT); 
  pinMode(ALARM, OUTPUT); 
  pinMode(STATUS_LED, OUTPUT); 
  
  pinMode(DOOR1, INPUT);
  pinMode(DOOR2, INPUT);
  pinMode(WINDOW1, INPUT);
  pinMode(PIR1, INPUT);
  pinMode(PIR2, INPUT);
  
  srlcd.begin(16,2);
  srlcd.print("ready ");
    
  setupNetwork();
  
}

void loop()
{
  delay(50);
  
  if(digitalRead(SELECT_BTN)){
    if(preAlarm==0 && alarmTime==0){
      toggleSystemStatus();
    }
  }
  
  if(preAlarm > 0 || alarmTime > 0){
    handleAlarms();
  }

  if(digitalRead(DOOR1)){
    if(!door1Open){
      sendMessage("door1Open");
      door1Open = true;
      checkAlarm();
    }
  }else{
    door1Open = false; 
  }
  
  if(digitalRead(DOOR2)){
    if(!door2Open){
      sendMessage("door2Open");
      door2Open = true;
      checkAlarm();
    }
  }else{
    door2Open = false;
  }
  
  if(digitalRead(WINDOW1)){
    if(!window1Open){
      sendMessage("window1Open");
      window1Open = true;
      checkAlarm();
    }
  }else{
    window1Open = false; 
  }
  
  // IR detect routine
  //
   if(digitalRead(PIR2) && pir1On < PIR_TIME){
      pir1On++;
   }else{
      if(!digitalRead(PIR2)){
        pir1On=0; 
      }
   }
 
   if(pir1On == PIR_TIME && pirCoolDown != 0){
        sendMessage("pir1Open");
        checkAlarm();
        pirCoolDown = PIR_COOLDOWN;
        if(!digitalRead(PIR2)){
          pir1On=0;
        }
   }
   
   if(pirCoolDown > 0){
      pirCoolDown--; 
   }

   // check for control messages
   connectTimer++;
   if(connectTimer == CONNECT_TIME){
     connectTimer=0; 
     checkForMsg();
   }
}

void toggleSystemStatus(){
    delay(500);  
    if(systemOn){
      sendMessage("SysOff");
      
      systemOn = false;
      
      digitalWrite(BEEPER, HIGH);
      delay(150);
      digitalWrite(BEEPER, LOW); 
      delay(100); 
      digitalWrite(BEEPER, HIGH);
      delay(150);
      digitalWrite(BEEPER, LOW);  
      
      digitalWrite(STATUS_LED, LOW);
      
    }else{
      // Got 2 minutes to get out.
      sendMessage("sysOn");
      systemOn = true; 
      for(int i=60;i--;i>0){
        delay(50);
        digitalWrite(BEEPER, LOW);   
        digitalWrite(STATUS_LED, LOW);
        
        if(digitalRead(SELECT_BTN)){
           i=0;
           sendMessage("SysOff");
           systemOn = false;
           delay(2000); 
        }else{
           delay(1950);
           digitalWrite(BEEPER, HIGH);
           digitalWrite(STATUS_LED, HIGH);
        }
        srlcd.setCursor(0,1);
        srlcd.print(i);
      }
      digitalWrite(BEEPER, LOW);   
    } 
}

void checkAlarm(){
      if(systemOn){
        srlcd.setCursor(1,1);
        srlcd.print(" -- INTRUSION! -- ");
        srlcd.setCursor(0,0);
        if(preAlarm==0 && alarmTime==0){
          preAlarm=PRE_ALARMT;
        }
      }else{
         delay(2000);
         srlcd.clear();
         srlcd.print("ready."); 
      }
}

void handleAlarms(){
  if(preAlarm > 0){
    preAlarm--;
    
    if(digitalRead(UP_BTN) && digitalRead(DOWN_BTN)){
      deactTime--;
    }
    
    if(preAlarm==500 || preAlarm==400 || preAlarm==300 || preAlarm == 200 ||
      preAlarm == 100 || preAlarm ==50 || preAlarm == 30 || preAlarm == 10){
      digitalWrite(BEEPER, HIGH);
      delay(100);
      digitalWrite(BEEPER, LOW); 
    }
    
    if(preAlarm==1){
      alarmTime=ALARM_TIME;
    } 
  }
  
  
  // ALARM IS ON !!!
  if(alarmTime > 0){
    alarmTime--;
    srlcd.setCursor(0,0);
    digitalWrite(BEEPER, HIGH);
    digitalWrite(ALARM, HIGH);
    sendMessage("alarm");
    srlcd.setCursor(1,1);
    srlcd.print(" -- ALARM! -- ");
      
    if(digitalRead(UP_BTN) && digitalRead(DOWN_BTN)){
      deactTime--;
    }
  }
  
  // deactbuttons have been pressed long enough
  if(deactTime==0){
      sendMessage("deactivated");
      
      alarmTime=0;
      preAlarm=0; 
      deactTime=DEACT_TIME;
      systemOn = false;
      digitalWrite(BEEPER, LOW); 
      digitalWrite(ALARM, LOW);
      digitalWrite(STATUS_LED, LOW);
   }
   
   // alarm has faded
   if(alarmTime==1){
      sendMessage("alarmPassed");
      digitalWrite(BEEPER, LOW); 
      digitalWrite(ALARM, LOW);
      
   }
}


void setupNetwork(){
 
  if(Ethernet.begin(mac) == 0){
    srlcd.print("no dhcp");
  }else{
      
    delay(1000);
    if (client.connect(serverip,80)) {
      srlcd.print("connected");
      client.println("GET /alarmControl/ctrl.php?msg=PowerOn HTTP/1.0");
      client.println();
      client.stop();
      network = true;
    } else {
      srlcd.clear();
      srlcd.print("connection failed");
    }
  } 
}

void sendMessage(String msg){
  srlcd.clear();
  srlcd.print(msg);

  if (network && client.connect(serverip,80)) {
      client.println("GET /alarmControl/ctrl.php?msg="+msg+" HTTP/1.0");
      client.println();
      client.stop();
      delay(254);
    } else {
      //srlcd.clear();
      //srlcd.print("connection failed");
      srlcd.setCursor(1,1);
      srlcd.print("-");
      network = false;
      srlcd.setCursor(0,0);
    }

}

void checkForMsg(){
     srlcd.setCursor(1,1);
     srlcd.print(".");
     if(client.connect(serverip,80)) {
        network=true;
        client.println("GET /alarmControl/ctrl.php?msg=getControl HTTP/1.0");
        client.println();
        
       int charcount=0;
        String currentline="";
        
        while(client.connected() && charcount < 2000){
          delay(10);
          charcount++; 
          if (client.available()) {
            char inp = client.read();
            currentline += inp;
            
            if(inp == '\n'){
              //srlcd.clear();
              //srlcd.print(currentline);
              delay(10);
              currentline=""; 
            }
            
            if(currentline.startsWith("On ")){
               srlcd.clear();
               srlcd.print(" activated by web");
               systemOn = true;
                digitalWrite(7,HIGH);
               delay(500);
                 digitalWrite(7,LOW);
              
               delay(1500);
               client.stop();
              
            }else if(currentline.startsWith("Off")){
               srlcd.clear();
               srlcd.print(" deactivated by web");
               systemOn = false; 
               delay(1500);
               client.stop();
              
            }else if(currentline.startsWith("Stat")){
               srlcd.clear();
               srlcd.print("status = ");
               delay(100);
               client.stop();
               delay(1000);
               sendMessage("Status:");
               if(systemOn){
                 sendMessage("systemOn"); 
               }else{
                 sendMessage("systemOff"); 
               }
               if(door1Open){
                 sendMessage("door1Open"); 
               }
               if(door2Open){
                 sendMessage("door2Open"); 
               }
               if(window1Open){
                 sendMessage("window1Open");
               }
               if(pir1On == PIR_TIME){
                 sendMessage("pir1Open");
               }     
            } 
            
          }
        
        }
        // always stop client
        client.stop(); 
      }
      srlcd.setCursor(1,1);
      if(network){
        srlcd.print("+");
       }else{
        srlcd.print("-");
       }
}
  


