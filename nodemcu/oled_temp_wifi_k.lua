lighton=0
lcount=4
pin=4
gpio.mode(pin,gpio.OUTPUT)
dhtpin = 7
humi=0
temp=0
tempout=0

wifi.setmode(wifi.STATION)
wifi.sta.config("****","****")

function init_i2c_display()
     i2c.setup(0, 6, 5, i2c.SLOW)
     disp = u8g.ssd1306_128x64_i2c(0x3c)
     --disp = u8g.sh1106_128x64_i2c(0x3c)
     disp:setFontRefHeightExtendedText()
     disp:setDefaultForegroundColor()
     disp:setFontPosTop()
end

function sendStatus()
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", function(conn, payload)success = true print(payload)end)
  conn:on("connection",
   function(conn, payload)
   print("Connected")
   conn:send('GET /projects/senswi/statush.php?s=0&t='..temp..'&h='..humi..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')

end

function readTempout()
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", 
    function(conn, payload)
    success = true 
    print(payload)
    string.gsub(payload,"_(.*)_", function(a) tempout = tonumber(a) end  )
  end)
  conn:on("connection",
   function(conn, payload)
   print("Connected")
   conn:send('GET /projects/senswi/getouttemp.php HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')

end

init_i2c_display()

tmr.alarm(1,2000,1,function()
    if lighton==0 then
        lighton=1
        status,temp,humi,temp_decimial,humi_decimial = dht.read(dhtpin)
        if( status == dht.OK ) then
  
          print(lcount.." Temperature:"..temp..";".."Humidity:"..humi)
          disp:firstPage()
          repeat
            
            disp:setFont(u8g.font_fub14)
            disp:drawStr( 4, 16, "In  : "..temp.."C")
            disp:drawStr( 4, 40, "Out : "..tempout.."C")
            disp:setFont(u8g.font_6x10)
            disp:drawStr( 4, 55, "hum : "..humi.." "..lcount.." ")
            
          until disp:nextPage() == false
          
        elseif( status == dht.ERROR_CHECKSUM ) then
          print( "DHT Checksum error." );
        elseif( status == dht.ERROR_TIMEOUT ) then
          print( "DHT Time out." );
        end
        --gpio.write(pin,gpio.HIGH)
        lcount = lcount -1
          if (lcount == 2) then
            readTempout()
          end
          if (lcount == 0) then
            sendStatus()
            lcount = 180
          end
    else
        lighton=0
         gpio.write(pin,gpio.LOW)
    end
end)
