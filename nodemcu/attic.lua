   sda = 2  -- gpio 0
   scl = 3 -- gpio 2

   require('ds18b20')
   ds18b20.setup(7)
   
   --subPin = 8 -- this is ESP-01 pin GPIO02
   windPin = 6
   tempfront = 0
      
   windCount = 0
   windAvCount = 0
   windAvs = "n"
   windgust = 0
   prevWind = 0
   loopcount = 0  -- loopcount is only for the first 100 to fill the array
   windArray = {}
   for i=1, 180 do
      windArray[i] = 0
   end
   
   ds18b20.read(
    function(ind,rom,res,temp,tdec,par)
        print(ind,string.format("%02X:%02X:%02X:%02X:%02X:%02X:%02X:%02X",string.match(rom,"(%d+):(%d+):(%d+):(%d+):(%d+):(%d+):(%d+):(%d+)")),res,temp,tdec,par)
        print(temp)
    end,{});
    
   st = "nn"
   i2c.setup(0, sda, scl, i2c.SLOW)
   disp = u8g.ssd1306_128x64_i2c(0x3c)
   --disp = u8g.sh1106_128x64_i2c(0x3c)

   disp:setFontRefHeightExtendedText()
   disp:setDefaultForegroundColor()
   disp:setFontPosTop()
   
     
   disp:firstPage()
    repeat
        disp:setFont(u8g.font_fub14)
        disp:drawStr( 4, 20, " init ")
        disp:drawStr( 4, 40, " DISPLAY ")
        disp:setFont(u8g.font_6x10)
        disp:drawStr( 4, 60, " more info ")
      until disp:nextPage() == false
     print("init done")

  local function onWindChange(level, pulse)
    -- print("wind"..pulse)
    if ( pulse ~= prevpulse ) then
      windCount = windCount + 1 
    end
    prevpulse = pulse
    gpio.trig(windPin, level == gpio.HIGH  and "down" or "up")
  end

  gpio.mode(windPin,gpio.INT)
  --gpio.trig(rainPin,"down", debounce(onRainChange))
  gpio.trig(windPin,"down", onWindChange)



  function update() -- every 3 senonds
    if (st == "nn") then
        updateTemp()
    end

    updateWind()
  
    disp:firstPage()
    repeat
        disp:setFont(u8g.font_fub14)
        disp:drawStr( 4, 20, " temp "..st)
        disp:setFont(u8g.font_6x10)
       -- disp:drawStr( 4, 40, " rain "..rainCount.." tot "..rainTot)
        disp:drawStr( 4, 40, " wind "..windCount.."     "..windAvCount)
        disp:drawStr( 4, 60, " av "..windAvs.." gust "..windgust)
      until disp:nextPage() == false

  end

  function totals() -- every 5 minutes
    if ( loopcount < 180 ) then
      loopcount = loopcount + 1
    end
    -- last 100
    print("-- totals "..loopcount)
    local windAv = 0
    windAvCount = windAvCount + 1
    -- divide by 15minuts / 6 seconds
    windArray[windAvCount] = math.floor( windCount / 20 )
    
    for i=1, 180 do
      windAv = windAv + windArray[i] --av
    end
    windAvs = math.floor(windAv / loopcount)
    print ("av "..windAv.."/"..loopcount.."="..windAvs)      
    windCount = 0
    if ( windAvCount == 180 ) then
      windAvCount = 0
    end
    
  end
  

  function updateTemp()
    ds18b20.read(
    function(ind,rom,res,temp,tdec,par)
        print(temp)
        print(tdec)
        --if (t == nil) or (ti == nil) then
        if (temp == nil) then
            print("no temp ")
            st = "nn"
        else
          --print(" Temperature:"..t..";")
          st = string.format("%.2f",temp)  
          print("st"..st)
        end
    end,{});
  end

  function updateWind()
    local ngust = windCount - prevWind
    if (ngust > windgust) then
        windgust = ngust
    end
    prevWind = windCount
  end

  function sendStatus()
    local sendString = 't='..st..'&w='..windAvs..'&g='..windgust 
    conn = nil
    conn = net.createConnection(net.TCP, 0)
    conn:on("receive", function(conn, payload)success = true print(payload)end)
    conn:on("connection",
     function(conn, payload)
     print("Connected")
     conn:send('GET /sens/temp_roof.php?s=0&'..sendString..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
     conn:on("disconnection", function(conn, payload) print('Disconnected') end)
    conn:connect(80,'edsard.nl')
  end


  ltimer = tmr.create()
  -- every 6 seconds
  ltimer:register(6000, 
            tmr.ALARM_AUTO, 
            function (t) 
                update()
            end)
  ltimer:start()

  ttimer = tmr.create()
  -- every 15 minutes
  ttimer:register(15*60*1000, 
            tmr.ALARM_AUTO, 
            function (t) 
                updateTemp()
                
                sendStatus()
                windgust = 0
                totals()
                 
            end)
  ttimer:start()

  mtimer = tmr.create()
  -- every 1 minute
  mtimer:register(60*1000, 
            tmr.ALARM_AUTO, 
            function (t) 
                updateTemp()
                --sendStatus()
                
                totals() 
            end)
  mtimer:start()
  
  
