   sda = 3  -- gpio 0
   scl = 4  -- gpio 2
   tempfront = 0
   tempback = 0
   
   i2c.setup(0, sda, scl, i2c.SLOW)
   disp = u8g.ssd1306_128x64_i2c(0x3c)
   --disp = u8g.sh1106_128x64_i2c(0x3c)

   disp:setFontRefHeightExtendedText()
   disp:setDefaultForegroundColor()
   disp:setFontPosTop()
   
     
   disp:firstPage()
    repeat
        disp:setFont(u8g.font_fub14)
        disp:drawStr( 4, 20, " init ")
        disp:drawStr( 4, 40, " DISPLAY ")
        disp:setFont(u8g.font_6x10)
        disp:drawStr( 4, 60, " more info ")
      until disp:nextPage() == false
     print("init done")

   
function readTempout()
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", 
    function(conn, payload)
    success = true 
    print(payload)
    string.gsub(payload,"_(.*)_", function(a) tempback = tonumber(a) end  )
    string.gsub(payload,"#(.*)#", function(a) tempfront = tonumber(a) end  )
  end)
  conn:on("connection",
   function(conn, payload)
   print("Connected")
   conn:send('GET /sens/getlatest.php HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')
end
   
function printPage()

    disp:firstPage()
    repeat
        disp:setFont(u8g.font_fub14)
        disp:drawStr( 4, 20, " front: "..tempfront)
        disp:drawStr( 4, 40, " back: "..tempback)
        disp:setFont(u8g.font_6x10)
        disp:drawStr( 4, 60, " more info ")
      until disp:nextPage() == false

end

mytimer = tmr.create()
  mytimer:register(5000, 
            tmr.ALARM_AUTO, 
            function (t) 
                 readTempout()
                 printPage()
            end)
  mytimer:start()
   
   


