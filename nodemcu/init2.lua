-- init.sl.lua
  print(" i ")
  dofile("setlight.lua")

  setlight(2,30)

  tstimer = tmr.create()
  tstimer:register(2000, 
            tmr.ALARM_SINGLE, 
            function (t) 
                setlight(2,28) 
            end)
  tstimer:start()
  
  mytimer = tmr.create()
  mytimer:register(4000, 
            tmr.ALARM_SINGLE, 
            function (t) 
                 connectwifi()
            end)
  mytimer:start()

  function connectwifi()
    print(" n ")
    wifi.setmode(wifi.STATION)
    station_cfg={}
    station_cfg.ssid="edland"
    station_cfg.pwd="xxx"
    wifi.sta.config(station_cfg)
    wifi.sta.sethostname("ESP-sleeplight")
    
    dofile("lightweb.lua")
    setlight(3,28)
  end
