   sda = 2  -- gpio 0
   scl = 3 -- gpio 2

   require('ds18b20')
   ds18b20.setup(7)
   
   subPin = 8 -- this is ESP-01 pin GPIO02
   windPin = 6
   rainPin = 1

   tempfront = 0
   rainCount = 0
   prevpulse = 0
   rainTot = 0
   rainArray = {}
   for i=1, 100 do
      rainArray[i] = 0
   end
   rainDayCount = 0
      
   windCount = 0
   windArray = {}
   for i=1, 100 do
      windArray[i] = 0
   end 
   windAvCount = 0
   windAv = 0
   windgust = 0
   prevWind = 0
   
   ds18b20.read(
    function(ind,rom,res,temp,tdec,par)
        print(ind,string.format("%02X:%02X:%02X:%02X:%02X:%02X:%02X:%02X",string.match(rom,"(%d+):(%d+):(%d+):(%d+):(%d+):(%d+):(%d+):(%d+)")),res,temp,tdec,par)
        print(temp)
    end,{});
    
   st = "nn"
   i2c.setup(0, sda, scl, i2c.SLOW)
   disp = u8g.ssd1306_128x64_i2c(0x3c)
   --disp = u8g.sh1106_128x64_i2c(0x3c)

   disp:setFontRefHeightExtendedText()
   disp:setDefaultForegroundColor()
   disp:setFontPosTop()
   
     
   disp:firstPage()
    repeat
        disp:setFont(u8g.font_fub14)
        disp:drawStr( 4, 20, " init ")
        disp:drawStr( 4, 40, " DISPLAY ")
        disp:setFont(u8g.font_6x10)
        disp:drawStr( 4, 60, " more info ")
      until disp:nextPage() == false
     print("init done")


  local function onRainChange(level, pulse)
    print("rain"..pulse)
    if ( pulse ~= prevpulse ) then
      rainCount = rainCount + 1
    end
    prevpulse = pulse
    gpio.trig(rainPin, level == gpio.HIGH  and "down" or "up")
  end

  local function onSubChange(level, pulse)
    print("sub"..pulse)
    
    prevpulse = pulse
    gpio.trig(subPin, level == gpio.HIGH  and "down" or "up")
  end

  local function onWindChange(level, pulse)
    print("wind"..pulse)
    if ( pulse ~= prevpulse ) then
      windCount = windCount + 1 
    end
    prevpulse = pulse
    gpio.trig(windPin, level == gpio.HIGH  and "down" or "up")
  end

  gpio.mode(rainPin,gpio.INT)
  --gpio.trig(rainPin,"down", debounce(onRainChange))
  gpio.trig(rainPin,"down", onRainChange)

  gpio.mode(subPin,gpio.INT)
  --gpio.trig(rainPin,"down", debounce(onRainChange))
  gpio.trig(subPin,"down", onSubChange)

  gpio.mode(windPin,gpio.INT)
  --gpio.trig(rainPin,"down", debounce(onRainChange))
  gpio.trig(windPin,"down", onWindChange)



  function update()
    if (st == "nn") then
        updateTemp()
    end

    updateWind()
  
    disp:firstPage()
    repeat
        disp:setFont(u8g.font_fub14)
        disp:drawStr( 4, 20, " temp "..st)
        disp:setFont(u8g.font_6x10)
        disp:drawStr( 4, 40, " rain "..rainCount.." tot "..rainTot)
        disp:drawStr( 4, 60, " wind "..windCount.." av "..windAv.." g "..windgust)
      until disp:nextPage() == false

  end

  function totals()
    -- last 100
    rainTot = 0
    rainDayCount = rainDayCount + 1
    rainArray[rainDayCount] = rainCount
    for i=1, 100 do
      rainTot = rainArray[i] + rainTot
    end
    rainCount = 0
    if ( rainDayCount == 100 ) then
      rainDayCount = 0
    end

    windAv = 0
    windAvCount = windAvCount + 1
    windArray[windAvCount] = ( windCount / 300 )
    for i=1, 100 do
      windAv = windArray[i] + windAv
    end
    windAv = windAv / 100
    windCount = 0
    if ( windAvCount == 100 ) then
      windAvCount = 0
    end
    
  end
  
  totals()

  function updateTemp()
    ds18b20.read(
    function(ind,rom,res,temp,tdec,par)
        print(temp)
        print(tdec)
        --if (t == nil) or (ti == nil) then
        if (temp == nil) then
            print("no temp ")
            st = "nn"
        else
          --print(" Temperature:"..t..";")
          st = string.format("%.2f",temp)  
          print("st"..st)
        end
    end,{});

    

  end

  function updateWind()
    local ngust = windCount - prevWind
    if (ngust > windgust) then
        windgust = ngust
    end
    prevWind = windCount
  end

  ltimer = tmr.create()
  -- every 3 seconds
  ltimer:register(3000, 
            tmr.ALARM_AUTO, 
            function (t) 
                update()
            end)
  ltimer:start()

  ttimer = tmr.create()
  -- every 15 minutes
  ttimer:register(15*60*1000, 
            tmr.ALARM_AUTO, 
            function (t) 
                totals()
                updateTemp()
            end)
  ttimer:start()
  
  
