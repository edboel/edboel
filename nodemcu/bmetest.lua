i2c.setup(0, 1, 2, i2c.SLOW)
--disp = u8g2.ssd1306_i2c_128x64_noname(0,0x3c)
--uart.setup(0, 9600, 8, uart.PARITY_NONE, uart.STOPBITS_1, 1)
bme280.setup()

P, T = bme280.baro()
print(string.format("QFE=%d.%03d", P/1000, P%1000))

H, T = bme280.humi()

print(bme280.baro())
print(bme280.humi())
P, T = bme280.baro()
print(P)

alt = 10
T, P, H, QNH = bme280.read(alt)
print(" read" )
print(T)
print(" end " )
local Tsgn = (T < 0 and -1 or 1); T = Tsgn*T
print(string.format("T=%s%d.%02d", Tsgn<0 and "-" or "", T/100, T%100))
--print(string.format("humidity=%d.%03d%%", H/1000, H%1000))
bme280.startreadout(0, function ()
  T, P = bme280.read()
  --local Tsgn = (T < 0 and -1 or 1); T = Tsgn*T
  --print(string.format("T=%s%d.%02d", Tsgn<0 and "-" or "", T/100, T%100))
    print(T);
    print("done")

end)
