--function init_i2c_display()
counter=0
pm10=-1
pm25=-1
temp=-100
humi=-100
baro=-100
readOk=false
sentOk=false


i2c.setup(0, 2, 1, i2c.SLOW)
disp = u8g2.ssd1306_i2c_128x64_noname(0,0x3c)
uart.setup(0, 9600, 8, uart.PARITY_NONE, uart.STOPBITS_1, 1)
bme280.setup()

function init_disp()
    disp:setFontRefHeightExtendedText()
    disp:setFontPosTop()
    disp:clearBuffer()
    disp:setFont(u8g2.font_6x10_tf)
    disp:drawStr( 4, 15, "PAUSED")
    disp:sendBuffer()
end


function sendStatus()
  --print("sending ") 
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("connection", function(conn, payload)
      conn:send('GET /sens/airq.php?id='..node.chipid()..'&pm10='..pm10..'&pm25='..pm25..'&t='..temp..'&h='..humi..'&pr='..baro..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')
      disp:drawStr( 4, 15, "request sent ")
      disp:sendBuffer()
      --print("r sent" )
      end)
  conn:on("receive", function(conn, payload)
      disp:drawStr( 4, 30, "request received ")
      disp:sendBuffer()
      --print("r ok" )
      end)
  conn:on("disconnection", function(conn, payload) 
     disp:drawStr( 4, 45, "disconnected ")
     disp:sendBuffer()
     --print("disc" )
     --wifi.sta.disconnect() 
     sentOk = true
     end)
     
  conn:connect(80,'edsard.nl')

end

function readTemp()
    --status, temp, humi, temp_dec, humi_dec = dht.readxx(7)
    --print("DHT Temperature:"..temp..";".."Humidity:"..humi)
    
    --disp:drawStr( 4, 15, "temp "..temp.." hum "..humi)
    --disp:sendBuffer()
    P, T = bme280.baro()
    --print(string.format("QFE=%d.%03d", P/1000, P%1000))
    if(P == nil)then
        baro= -100
    else
        baro = P/1000
    end
    H, T = bme280.humi()
    if(T == nil)then
        temp = -100
        humi = -100
    else
        temp = T/100
        humi = H/1000
    end
    --local Tsgn = (T < 0 and -1 or 1); T = Tsgn*T
    --print(string.format("T=%s%d.%02d", Tsgn<0 and "-" or "", T/100, T%100))
    --print(string.format("humidity=%d.%03d%%", H/1000, H%1000))

end

function startSensorShort()
    uart.write(0, 0x01)
end

function startSensor()
    --print("starting ") 
    uart.write(0,
        0xaa,   -- head
        0xb4,   -- command id
        0x06,   -- data byte 1 (command nr)
        0x01,   -- data byte 2 (set mode)
        0x01,   -- data byte 3 (work)
        0x00,   -- data byte 4
        0x00,   -- data byte 5
        0x00,   -- data byte 6
        0x00,   -- data byte 7
        0x00,   -- data byte 8
        0x00,   -- data byte 9
        0x00,   -- data byte 10
        0x00,   -- data byte 11
        0x00,   -- data byte 12
        0x00,   -- data byte 13
        0xFF,   -- data byte 14 (device id byte 1)
        0xFF,   -- data byte 15 (device id byte 2)
        0x06,   -- checksum
        0xAB    -- tail
        )
     
end


function stopSensor()
    --print("stopping ") 
    --uart.on("data")
    uart.write(0,
        0xaa,   -- head
        0xb4,   -- command id
        0x06,   -- data byte 1 (command nr)
        0x01,   -- data byte 2 (set mode)
        0x00,   -- data byte 3 (sleep)
        0x00,   -- data byte 4
        0x00,   -- data byte 5
        0x00,   -- data byte 6
        0x00,   -- data byte 7
        0x00,   -- data byte 8
        0x00,   -- data byte 9
        0x00,   -- data byte 10
        0x00,   -- data byte 11
        0x00,   -- data byte 12
        0x00,   -- data byte 13
        0xFF,   -- data byte 14 (device id byte 1)
        0xFF,   -- data byte 15 (device id byte 2)
        0x05,   -- checksum
        0xAB    -- tail
        )
     
end

function setSensorSleepTime()
    --print("stopping ") 
    --uart.on("data")
    uart.write(0,
        0xaa,   -- head
        0xb4,   -- command id
        0x08,   -- data byte 1 (command nr)
        0x01,   -- data byte 2 (query or set mode)
        0x10,   -- data byte 3 (sleep time mins)
        0x00,   -- data byte 4
        0x00,   -- data byte 5
        0x00,   -- data byte 6
        0x00,   -- data byte 7
        0x00,   -- data byte 8
        0x00,   -- data byte 9
        0x00,   -- data byte 10
        0x00,   -- data byte 11
        0x00,   -- data byte 12
        0x00,   -- data byte 13
        0xFF,   -- data byte 14 (device id byte 1)
        0xFF,   -- data byte 15 (device id byte 2)
        0x07,   -- checksum
        0xAB    -- tail
        )
     
end

function convertbytes(data,x)
    pm25 = ( data:byte(x+2) + data:byte(x+3) * 256 ) / 10
    pm10 = ( data:byte(x+4) + data:byte(x+5) * 256 ) / 10 
    --print("pm25 "..pm25.." pm10 "..pm10)
    disp:clearBuffer()
    disp:drawStr( 4, 1,  "pm25 " ..data:byte(x+2).." "..data:byte(x+3).."    ")
    disp:drawStr( 4, 15, "pm25 "..pm25)
    disp:drawStr( 4, 30, "pm10 " ..data:byte(x+4).." "..data:byte(x+5).."    ")
    disp:drawStr( 4, 45, "pm10 "..pm10)
    disp:sendBuffer()
end

function readData()
    --when it reads 8 bytes..
    -- byte0 = 170  = AA
    -- byte1 = 192  = C0
    -- byte2 = pm25 
    -- byte3 = pm25
    -- byte4 = pm10
    -- byte5 = pm10
    -- byte6 = CRC1
    -- byte7 = CRC2 

    uart.on("data", 30,
        function(data)
          --print("receive from uart:", data)
          disp:clearBuffer()
          if counter==0 then
            disp:drawStr( 4, 15, "START  ")
            disp:sendBuffer()
          end
          for x=0,22 do
            if data:byte(x)==170 and data:byte(x+1) == 192 then
                convertbytes(data,x)
                readOk=true
                writeToFile()
            end
          end
     end, 0)    
end


function writeToFile()
    --if file.open("readings.txt", "a+") then
    --  ltime = math.floor(tmr.time() / 60)
    --  file.writeline(tmr.time().." p "..pm10.." pm25 "..pm25.." t "..temp.." h "..humi)
    --  file.close()
    --end
end



function checkRead()
    --print("checkread "..counter)
    if(readOk==true or counter > 25)then
        if(temp==-100)then
            readTemp()
        end
        tmr.stop(4)
        --uart.on("data")
        stopSensor()
        sendStatus()
        --stopSensor()
    else
        counter=counter+1
        if(counter==15)then
            uart.setup(0, 9600, 8, uart.PARITY_NONE, uart.STOPBITS_1, 1)
            startSensor()
        end
    end
end

function checkSent()
    if(sentOk==true)then
       --stopSensor()
       wifi.sta.disconnect()
       tmr.stop(5)
    end
end

function doMeasurement()
    startSensor()
    wifi.sta.connect()
    counter=0
    readOk=false
    sentOk=false
    temp=-100
    readTemp()
    --startSensor()
    readData()
    tmr.alarm(  4, 9100, tmr.ALARM_AUTO, checkRead ) -- only checkread will activate sendstatus
    tmr.alarm(  5, 9900, tmr.ALARM_AUTO, checkSent )
    --tmr.alarm(  6, 60*1000, tmr.ALARM_SINGLE, stopSensor )
end

--print("begin airq")
init_disp()
setSensorSleepTime()
doMeasurement()
          
mytimer = tmr.create()
mytimer:register(15*60*1000, tmr.ALARM_AUTO, function (t) 
--mytimer:register(30*1000, tmr.ALARM_SINGLE, function (t) 

    disp:drawStr( 4, 15, "START ")
    disp:sendBuffer()
    doMeasurement()
    end)
    
mytimer:start()

