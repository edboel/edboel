--shutter control v1.1
 
upbutton=5 
downbutton=6

lighton=7
lightoff=0

relaypinup=2
relaypindown=3
blinkpin=4

gpio.mode(upbutton,gpio.INPUT) --gpio.INT)
gpio.mode(downbutton,gpio.INPUT)
gpio.mode(lighton,gpio.INPUT)
gpio.mode(lightoff,gpio.INPUT)

gpio.mode(relaypinup,gpio.OUTPUT)
gpio.write(relaypinup,gpio.HIGH)
gpio.mode(relaypindown,gpio.OUTPUT)
gpio.write(relaypindown,gpio.HIGH)

gpio.mode(blinkpin,gpio.OUTPUT)
gpio.write(blinkpin,gpio.HIGH)

relaystat = 0
shutterdelay=0
maxdelay=60
statusIsSent = 0
prevlighton = 1
prevlightoff = 1
shutterstat = "down";

server = net.createServer(net.TCP)

function sendStatus(event)
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", function(conn, payloadout)
    if (string.find(payloadout, "200 OK")) then
        sendsuccess=1    
    end
    print(payloadout)
  end)
  conn:on("connection",
   function(conn, payload)
   
     conn:send('GET /sens/light_sleeping.php?s=0&e='..event..'&s='..relaystat..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
   
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')

end

function sendLight(event)
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", function(conn, payloadout)
    if (string.find(payloadout, "200 OK")) then
        sendsuccess=1    
    end
    print(payloadout)
  end)
  conn:on("connection",
   function(conn, payload)
       if(event==1)then
         print("send light on")
         conn:send('GET /on HTTP/1.0\r\n\Host: 192.168.188.50\r\n\r\n')
       else
         print("send light off")
         conn:send('GET /off HTTP/1.0\r\n\Host: 192.168.188.50\r\n\r\n')
       end
     end)
   
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'192.168.188.50')

end

function checkShutter()

    relaystat = gpio.read(upbutton)
    if(relaystat == 0) then
        gpio.write(relaypindown,gpio.HIGH)
        gpio.write(blinkpin,gpio.LOW)
        tmr.delay(500000)
        gpio.write(relaypinup,gpio.LOW)
        gpio.write(blinkpin,gpio.HIGH)
        if(statusIsSent == 0) then
              --sendStatus(1)
              statusIsSent=1
        end
        shutterdelay = maxdelay
        print ("shutter up event "..shutterdelay)
        shutterstat = "up";
    end

    relaystat = gpio.read(downbutton)
    if(relaystat == 0) then
        gpio.write(relaypinup,gpio.HIGH)
        gpio.write(blinkpin,gpio.LOW)
        tmr.delay(500000)
        gpio.write(relaypindown,gpio.LOW)
        gpio.write(blinkpin,gpio.HIGH)
        if(statusIsSent == 0) then
              --sendStatus(2)
              statusIsSent=1
        end
        shutterdelay = maxdelay
        print ("shutter down event "..shutterdelay)
        shutterstat = "down";
    end
    
    if(shutterdelay >= 1) then
        shutterdelay = shutterdelay - 1
        print ("shutter delay"..shutterdelay)
        gpio.write(blinkpin,gpio.LOW)
        tmr.delay(10000)
        gpio.write(blinkpin,gpio.HIGH)
        if(shutterdelay ==1) then
          statusIsSent=0
          gpio.write(relaypinup,gpio.HIGH)
          gpio.write(relaypindown,gpio.HIGH)
          
          
        end
    end

end

function checkLight()

    local newlight = gpio.read(lighton)
    if(newlight == 0 and newlight ~= prevlighton)then
        sendLight(1)
        print ("light on event ")
        gpio.write(blinkpin,gpio.LOW)
        tmr.delay(400000)
        gpio.write(blinkpin,gpio.HIGH)
    end
    prevlighton = newlight

    newlight = gpio.read(lightoff)
    if(newlight == 0 and newlight ~= prevlightoff)then
        sendLight(0)
        print ("light off event ")
        gpio.write(blinkpin,gpio.LOW)
        tmr.delay(400000)
        gpio.write(blinkpin,gpio.HIGH)
    end
    prevlightoff = newlight
end

function SendHTML(sck, pos) -- Send LED on/off HTML page
   htmlstring = "<!DOCTYPE html>\r\n"
   htmlstring = htmlstring.."<html>\r\n"
   htmlstring = htmlstring.."<head><style>\r\n"
   htmlstring = htmlstring.."h1 { text-align: center }\r\n"
   htmlstring = htmlstring.."input { width: 100%; \r\n"
   htmlstring = htmlstring.."   height: 150px; }\r\n"
   htmlstring = htmlstring.."</style></head><body>\r\n"
   htmlstring = htmlstring.."<br><br><h1>SHUTTER Control</h1>\r\n"
   htmlstring = htmlstring.."<p>Click for SHUTTER Control, current stat "..shutterstat.."</p>\r\n"
   htmlstring = htmlstring.."<form method=\"get\">\r\n"
  if (pos==1)  then
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER DOWN\" onclick=\"window.location.href='/down'\">\r\n"
  elseif(pos==2)then
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER UP\" onclick=\"window.location.href='/up'\">\r\n"
  else
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER UP\" onclick=\"window.location.href='/up'\"><br>\r\n"
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER DOWN\" onclick=\"window.location.href='/down'\"><br>\r\n"
  end
   htmlstring = htmlstring.."</form><br>\r\n"
   htmlstring = htmlstring..shutterdelay.."</body>\r\n"
   htmlstring = htmlstring.."</html>\r\n"
   sck:send(htmlstring, function(conn) conn:close() end)
end

function receiveData(sck, data)-- process callback on recive data from client
  print("received"..data)
  if string.find(data, "GET /up")  then
   gpio.write(relaypindown,gpio.HIGH)
   SendHTML(sck, 1) 
   gpio.write(blinkpin,gpio.LOW)
    tmr.delay(400000)
    gpio.write(blinkpin,gpio.HIGH)
   gpio.write(relaypinup,gpio.LOW)
   shutterdelay = maxdelay
   shutterstat= "up";
  elseif string.find(data, "GET /down")  then
   gpio.write(relaypinup,gpio.HIGH)
   SendHTML(sck, 2)
    gpio.write(blinkpin,gpio.LOW)
    tmr.delay(400000)
    gpio.write(blinkpin,gpio.HIGH)
   gpio.write(relaypindown,gpio.LOW)
   shutterdelay = maxdelay
   shutterstat= "down";
  elseif string.find(data, "GET /stop")  then
   gpio.write(relaypinup,gpio.HIGH)
   gpio.write(relaypindown,gpio.HIGH)
   SendHTML(sck, 3)
   shutterdelay = 0
  elseif string.find(data, "GET / ") then
   SendHTML(sck, 0) 
  elseif string.find(data, "GET /stat ") then 
      sck:send(shutterstat, function(conn) conn:close() end)
  else
   sck:send("<h2>Not found...!!</h2>")
   sck:on("sent", function(conn) conn:close() end)
  end
end
 
function startServer()
  local ip=wifi.sta.getip()
  if(ip ~= nil)then 
       print("listening on " ..ip)
       server:listen(80, function(conn)-- listen to the port 80
      conn:on("receive", receiveData)
      end)
  else
     print(" no ip ")
     
     tmr.create():alarm(10000,tmr.ALARM_AUTO,function()
        startServer()
        end)
    gpio.write(blinkpin,gpio.LOW)
    tmr.delay(200000)
    gpio.write(blinkpin,gpio.HIGH)
  end
end

--every 0.5 sec
tmr.create():alarm(500,tmr.ALARM_AUTO,function()

    checkShutter()

    checkLight()
    
end)

if server then
  startServer()
end 
 
