gpio.mode(4,gpio.OUTPUT)
gpio.write(4,gpio.HIGH)
tmr.delay(900000)
gpio.write(4,gpio.LOW)
tmr.delay(900000)
gpio.write(4,gpio.HIGH)
tmr.delay(900000)
gpio.write(4,gpio.LOW)
print("begin")
server = net.createServer(net.TCP)

function SendHTML(sck, pos) -- Send LED on/off HTML page
   htmlstring = "<!DOCTYPE html>\r\n"
   htmlstring = htmlstring.."<html>\r\n"
   htmlstring = htmlstring.."<head>\r\n"
   htmlstring = htmlstring.."<title>SHUTTER Control</title>\r\n"
   htmlstring = htmlstring.."</head><body>\r\n"
   htmlstring = htmlstring.."<h1>SHUTTER Control</h1>\r\n"
   htmlstring = htmlstring.."<p>Click for SHUTTER Control</p>\r\n"
   htmlstring = htmlstring.."<form method=\"get\">\r\n"
  if (pos==1)  then
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER UP\" onclick=\"window.location.href='/down'\">\r\n"
  elseif(pos==2)then
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER DOWN\" onclick=\"window.location.href='/up'\">\r\n"
  else
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER UP\" onclick=\"window.location.href='/up'\"><br>\r\n"
   htmlstring = htmlstring.."<input type=\"button\" value=\"SHUTTER DOWN\" onclick=\"window.location.href='/down'\"><br>\r\n"
  end
   htmlstring = htmlstring.."</form>\r\n"
   htmlstring = htmlstring.."</body>\r\n"
   htmlstring = htmlstring.."</html>\r\n"
   sck:send(htmlstring,function(sk)
      sk:close()
    end)
end

function receiveData(sck, data)-- process callback on recive data from client
  print("received"..data)
  if string.find(data, "GET /up")  then
   gpio.write(4,gpio.HIGH)
   SendHTML(sck, 1)
  elseif string.find(data, "GET /down")  then
   gpio.write(4,gpio.LOW)
   SendHTML(sck, 2)
  elseif string.find(data, "GET / ") then
   SendHTML(sck, 0)
  else
   sck:send("<h2>Not found...!!</h2>")
   sck:on("sent", function(conn) conn:close() end)
  end
end 

if server then
  print("listening on " ..wifi.sta.getip())
  server:listen(80, function(conn)-- listen to the port 80
  conn:on("receive", receiveData)
  end)
end