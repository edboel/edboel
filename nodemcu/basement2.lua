scl=1
sda=2

doorpin=3
waterpin=4 --also status led...
lightpin=5

ledpin=6
pumppin=7

doordelay=0
pumpdelay=0

gpio.mode(doorpin,gpio.INPUT) --gpio.INT)
gpio.mode(waterpin,gpio.INPUT)
gpio.mode(lightpin,gpio.INPUT)

gpio.mode(ledpin,gpio.OUTPUT)
gpio.write(ledpin,gpio.LOW)
gpio.mode(pumppin,gpio.OUTPUT)
gpio.write(pumppin,gpio.LOW)

doorstat = gpio.read(doorpin)
waterstat = gpio.read(waterpin)
lightstat = gpio.read(lightpin)

humi=""
temp=""
lcount=5
sendsuccess=0
disp=""

function init_bus() --Set up the u8glib lib
     local sla = 0x3C
     i2c.setup(0, sda, scl, i2c.SLOW)
     bme280.setup()
     --H, T = bme280.humi()
     
     disp = u8g2.ssd1306_i2c_128x64_noname(0,sla)
     disp:setFont(u8g2.font_6x10_tf)
     --disp:firstPage()
        --repeat
         disp:drawStr(10, 30, "welkom")
         disp:sendBuffer()  
       -- until disp:nextPage() == false
     --disp:setFont(u8g2.font_10x20_tf)
     server = net.createServer(net.TCP)
     if server then
        startServer()
    end 
end

function write_OLED() -- Write Display
    local myip=wifi.sta.getip()
    if(myip==nil)then myip="no ip" end
    print("T="..temp.." H="..humi.." C="..lcount)
    print("door:"..doorstat.." water:"..waterstat.." light"..lightstat.."delay:"..doordelay..pumpdelay)
    print("ip:"..myip.." stat:"..sendsuccess)
    --disp:firstPage()
    --repeat
     disp:clearBuffer()
     disp:setFont(u8g2.font_10x20_tf)
     disp:drawStr(1, 20, "T="..temp.." H="..humi)
     disp:setFont(u8g2.font_6x10_tf)
     disp:drawStr(1, 39, "door "..doorstat.." water "..waterstat.." li "..lightstat)
     if(lcount%2==0)then
       disp:drawStr(1, 54, "del:"..doordelay.." "..pumpdelay.." s:"..sendsuccess.." c:"..lcount)
     else
       disp:drawStr(1, 54, "ip:"..myip)
     end
     disp:sendBuffer()  
    --until disp:nextPage() == false
end


function sendStatus(event)
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", function(conn, payloadout)
    if (string.find(payloadout, "200 OK")) then
        sendsuccess=1    
    end
    print(payloadout)
  end)
  conn:on("connection",
   function(conn, payload)
   print("sending_t="..temp..'&h='..humi..'&d='..doorstat..'&w='..waterstat..'&l='..lightstat)
   
     conn:send('GET /sens/temp_basement.php?s=0&t='..temp..'&h='..humi..'&d='..doorstat..'&w='..waterstat..'&l='..lightstat..'&e='..event..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
   
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')

end

function checkDoor()

    newstat = gpio.read(doorpin)
    if(doorstat ~= newstat) then
        print ("door event"..newstat)
        doorstat = newstat
        sendStatus(1)
        --delay to see the light go on
         tmr.delay(600000)
    end 
    if(doorstat == 0) then
        gpio.write(ledpin,gpio.HIGH)
        doordelay = 600
    else
        --grace time
        if(doordelay >= 1) then
            doordelay = doordelay - 1
            print ("door delay"..doordelay)
            if(doordelay ==1) then
              gpio.write(ledpin,gpio.LOW)
            end
        end
    end

end

function checkWater()

    waterstat = gpio.read(waterpin)
    if(waterstat == 0) then
        gpio.write(pumppin,gpio.HIGH)
        if(statusIsSent == 0) then
              sendStatus(1)
              statusIsSent=1
        end
        pumpdelay = 250
        print ("Water event "..pumpdelay)
    else
        if(pumpdelay >= 1) then
            pumpdelay = pumpdelay - 1
            print ("pump delay"..pumpdelay)
            if(pumpdelay ==1) then
              statusIsSent=0
              gpio.write(pumppin,gpio.LOW)
            end
        end
    end

end

function checkLight()
    newstat = gpio.read(lightpin)
    if(lightstat ~= newstat) then
        print ("light event"..newstat)
        lightstat = newstat
        sendStatus(2)
    end
end

function checkTemp()
    bme280.startreadout(0, function ()
        local T, P, H = bme280.read()
        if(T == nil) then
            print("temp read error")
            humi="ee" 
        else
            local Tsgn =  (T < 0 and -1 or 1); T = Tsgn*T
            temp=string.format("%s%d.%02d", Tsgn<0 and "-" or "", T/100, T%100)
--            humi=string.format("%d.%03d%%", H/1000, H%1000)
            humi=string.format("%d", H/1000)
        end
    end)
end

function SendHTML(sck, pos) -- Send LED on/off HTML page
   htmlstring = "<!DOCTYPE html>\r\n"
   htmlstring = htmlstring.."<html>\r\n"
   htmlstring = htmlstring.."<head><style>\r\n"
   htmlstring = htmlstring.."h1 { text-align: center }\r\n"
   htmlstring = htmlstring.."input { width: 100%; \r\n"
   htmlstring = htmlstring.."   height: 150px; }\r\n"
   htmlstring = htmlstring.."</style></head><body>\r\n"
   htmlstring = htmlstring.."<br><br><h1>Basement Control</h1>\r\n"
   htmlstring = htmlstring.."<p>Click for SHUTTER Control, current stat "..pumpdelay.."</p>\r\n"
   htmlstring = htmlstring.."<form method=\"get\">\r\n"
  
   htmlstring = htmlstring.."<input type=\"button\" value=\"PUMP ON\" onclick=\"window.location.href='/pump'\">\r\n"
   htmlstring = htmlstring.."<input type=\"button\" value=\"LIGHT ON\" onclick=\"window.location.href='/light'\">\r\n"

   htmlstring = htmlstring.."</form><br>\r\n"
   htmlstring = htmlstring..doordelay.."</body>\r\n"
   htmlstring = htmlstring.."</html>\r\n"
   sck:send(htmlstring, function(conn) conn:close() end)
end

function receiveData(sck, data)-- process callback on recive data from client
  print("received"..data)
  if string.find(data, "GET /pump")  then
        gpio.write(pumppin,gpio.HIGH)
        pumpdelay = 250
  elseif string.find(data, "GET / ") then
   SendHTML(sck, 0) 
  else
   sck:send("<h2>Not found...!!</h2>")
   sck:on("sent", function(conn) conn:close() end)
  end

end

function startServer()
  local ip=wifi.sta.getip()
  if(ip ~= nil)then 
       print("listening on " ..ip)
       server:listen(80, function(conn)-- listen to the port 80
      conn:on("receive", receiveData)
      end)
  else
     print(" no ip ")
  end
end

--every 0.5 sec
tmr.create():alarm(200,tmr.ALARM_AUTO,function()

    checkDoor()

    checkWater()

    checkLight()
    
end)

--every 5 seconds
tmr.create():alarm(5000,tmr.ALARM_AUTO,function()
    checkTemp()
    write_OLED()
    lcount = lcount -1
    if (lcount == 2) then
    --readTempout()
    end
    if (lcount == 0) then
        sendStatus(0)
        lcount = 180
    end
end)

init_bus()
