MAX7219_REG_NOOP        = 0x00
MAX7219_REG_DECODEMODE  = 0x09
MAX7219_REG_INTENSITY   = 0x0A
MAX7219_REG_SCANLIMIT   = 0x0B
MAX7219_REG_SHUTDOWN    = 0x0C
MAX7219_REG_DISPLAYTEST = 0x0F

TIMEZONE = 2

happy = {0x3C, 0x42, 0xA5, 0x81, 0xA5, 0x99, 0x42, 0x3C};
frown = {0x3C, 0x42, 0xA5, 0x81, 0xBD, 0x81, 0x42, 0x3C};
sad = {0x3C, 0x42, 0xA5, 0x81, 0x99, 0xA5, 0x42, 0x3C};
faces = {happy, frown, sad};

function sendByte(reg, data)
  spi.send(1,reg * 256 + data)
  tmr.delay(50)
end

function displayFace(faceIndex)
  local face = faces[faceIndex];
  -- iterates over all 8 columns and sets their values
  for i=1,8 do
    sendByte(i,face[i]);
  end
end

function setup()
    spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 16, 8)

    sendByte (MAX7219_REG_SHUTDOWN, 1)
    sendByte (MAX7219_REG_SCANLIMIT, 7)
    sendByte (MAX7219_REG_DECODEMODE, 0x00)
    sendByte (MAX7219_REG_DISPLAYTEST, 0)
    sendByte (MAX7219_REG_INTENSITY, 1)
    sendByte (MAX7219_REG_SHUTDOWN, 1)

    -- clear digits...

    spi.send(1,256);
    spi.send(1,512);
    spi.send(1,768);
    spi.send(1,1024);
    spi.send(1,1280);
    spi.send(1,1536);
    spi.send(1,1792);
    spi.send(1,2048);

    wifi.setmode(wifi.STATION);
    wifi.sta.config("****","****") 
    
    spi.send(1, 256 +1 )
    spi.send(1,2048 +1)
    tmr.stop(0);

    tmr.alarm(1,1000, 1, function() if wifi.sta.getip()==nil 
        then print(" Wait to IP address!") 
        else print("New IP address is "..wifi.sta.getip()) 
         tmr.stop(1) 
         spi.send(1, 512 +1 )
         spi.send(1,1792 +1)
         end 
    
    end) 
    
end

-- changes the face every two seconds cycling through the array of faces
function moody(i)
  faceIndex = (i % 3) + 1;
  displayFace(faceIndex);
  tmr.alarm(0, 1000, 0, function()
    moody(faceIndex);
  end);
end

function displayHour(hour)
  if hour > 9 then
    r = math.floor(hour / 10);
    spi.send(1, 1024 + convert7(r) );

    rr = hour - (r*10);
    spi.send(1, 768 + convert7(rr) );
  else
    spi.send(1, 768 + convert7(hour) );
    spi.send(1, 1024)
  end
end

function displayMinute(minute)
  if minute > 9 then
    r = math.floor(minute / 10);
    spi.send(1, 512 + convert7(r) );

    rr = minute - (r*10);
    spi.send(1, 256 + convert7(rr) );
  else
    spi.send(1, 256 + convert7(minute) );
    spi.send(1,512);
  end
  -- spi.send(1,512 + seconds * 2);
end

function displayDot(showDot,hours)
  r = math.floor(hours / 10);
  rr = hours - (r*10);
  c =  convert7(rr); 
  if showDot == true then
    spi.send(1, 768 + c + 128 );
  else
    spi.send(1, 768 + c );
  end

end

function convert7(input)
  x = 128

    if input == 1 then x = 48    end
    if input == 2 then x =  109   end
    if input == 3 then x =  121   end
    if input == 4 then x =  51   end
    if input == 5 then x =  91   end
    if input == 6 then x =  95   end
    if input == 7 then x =  112   end
    if input == 8 then x =  127   end
    if input == 9 then x =  123   end
    if input == 0 then x =  126   end



  return x
end  
  

function readTime()
    conn = nil
    print('begin')
  
    conn=net.createConnection(net.TCP, 0)
    
    conn:on("connection",function(conn, payload)
                conn:send("HEAD / HTTP/1.1\r\n"..
                          "Host: google.com\r\n"..
                          "Accept: */*\r\n"..
                          "User-Agent: Mozilla/4.0 (compatible; esp8266 Lua;)"..
                          "\r\n\r\n")
                end)
               
    conn:on("receive", function(conn, payload)
        print('\nRetrieved in '..((tmr.now()-t)/1000)..' milliseconds.')

        strRes = string.sub(payload,string.find(payload,"Date: ")+6,string.find(payload,"Date: ")+35)
        print('Google says it is '..strRes)
        conn:close()

        hours = string.sub(strRes,18,19)
        mins = string.sub(strRes,21,22)
        secs = string.sub(strRes,24,25)
        print('calcing: '..mins..' and '..hours)
        spi.send(1, 768 +1 )
        spi.send(1,1536 +1)
        calctime(tonumber(hours)+TIMEZONE,tonumber(mins),tonumber(secs))
        
        end)
    t = tmr.now()   
    conn:connect(80,'google.com') 
    --conn:connect(80,'74.125.224.72') 
end



function calctime(hours, minutes, seconds)
  
  displayHour(hours);
  --displaySecond(seconds)
  displayMinute(minutes);
  -- print('minute: '..minutes)
  if (seconds % 2 == 0) then
    displayDot(true,hours)
  else
    displayDot(false,hours)
  end
  
  seconds = seconds + 1;
  if seconds > 59 then
    seconds = 0
    minutes = minutes + 1
  end
  if minutes > 59 then
    minutes = 0
    hours = hours + 1
  end
  if hours > 23 then
    hours = 0
  end

  --sync time every day
  if hours == 5 and minutes == 5 then
    spi.send(1,1536)
    spi.send(1,1792)
    spi.send(1,2048)
    tmr.delay(10000)
    readTime()
  end
  
  tmr.alarm(0, 1000, 0, function()
    calctime(hours, minutes, seconds);
  end);
end

setup()

readTime()

--calctime(21,32);
