--weather.lua 4 dec 2016
rainPin = 5 -- this is ESP-01 pin GPIO02
windPin = 6

rainCount = 0
windCount = 0
hourCount = 0
rh = 0
t = 0

function init()
  wifi.setmode(wifi.STATION)
  wifi.sta.config("xxx","xxx")
  
  gpio.mode(rainPin,gpio.INT,gpio.PULLUP)
  gpio.trig(rainPin,"down", debounce(onRainChange))

  gpio.mode(windPin,gpio.INT,gpio.PULLUP)
  gpio.trig(windPin,"down", debounce(onWindChange))
  
  am2320.init(2,1)
  
  mytimer = tmr.create()
  -- every 15 minutes
  mytimer:register(15*60*1000, 
            tmr.ALARM_AUTO, 
            function (t) 
                hourCount = hourCount + 1
                rh, t = am2320.read()
                local temp = t / 10
                local hum = rh / 10
                local rain = rainCount * 3
                local wind = windCount
                print("up "..tmr.time() .." send "..temp.."c "..hum.."% "..rain.."ml "..wind.."mss")
                if(hourCont == 4) then
                  rainCount = 0
                  windCount = 0
                  hourCount = 0
                  writeStatus(temp,hum,rain,wind) 
                end
                sendStatus(temp,hum,rain,wind) 
                
              
            end)
  mytimer:start()
  print("wheater 1.1 ")
end

function debounce (func)
    local last = 0
    local delay = 10000

    return function (...)
        local now = tmr.now()
        if now - last < delay then return end

        last = now
        return func(...)
    end
end

function onRainChange()
    if gpio.read(rainPin) == 0 then
        rainCount = rainCount + 0.2794
        --print("Rain! "..rainCount)
    end
end

function onWindChange()
    if gpio.read(windPin) == 0 then
        windCount = windCount + 1
        --print("Wind! "..windCount)
    end
end

function sendStatus(temp,hum,rain,wind)
  wifi.sta.connect()

  local nettimer = tmr.create()

  -- oo calling
  nettimer:register(4000, tmr.ALARM_SINGLE,  
     function (t) 
          -- sendStatus()
          if (wifi.sta.getip() == nil) then
            print("no ip")
            wifi.sta.disconnect()

            wifi.sta.connect()
          else
            print("ip "..wifi.sta.getip())
          end
          
          conn = nil
          conn = net.createConnection(net.TCP, 0)
          conn:on("receive", function(conn, payload)success = true print(payload)end)
          conn:on("connection",
           function(conn, payload)
           print("Connected")
           conn:send('GET /sens/temp_roof.php?s=0&t='..temp..'&h='..hum..'&r='..rain..'&w='..wind..' &ip='..wifi.sta.getip()..'  HTTP/1.0\r\n\Host: 192.168.0.105\r\n\r\n')end)
          conn:on("disconnection", function(conn, payload) print('Disconnected') end)
          -- edsard.nl = 192.168.0.105
          conn:connect(80,'192.168.0.105')

            
     
  
     t:unregister() end)
  nettimer:start()

end



srv=net.createServer(net.TCP)
srv:listen(80,function(conn2)
    conn2:on("receive", function(client,request)
        print("received")
        local buf = "";
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then
            _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
        end
        local _GET = {}
        if (vars ~= nil)then
            for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                _GET[k] = v
            end
        end
        if(_GET.stat == "1")then
            print("get data")
            rh, t = am2320.read()
        elseif(_GET.stat == "2")then
           rainCount = rainCount + 0.2794
        elseif(_GET.stat == "3")then
           writeStatus(t,rh,rainCount,windCount) 
        end
        
        local temp = t / 10
        local hum = rh / 10
        buf = buf.."<h1> Weather Web Server</h1>";
        buf = buf.."<p>ip: "..wifi.sta.getip().."<br>uptime"..tmr.time() .." </p>";
        buf = buf.."<p>temp: "..temp.."<br>hum : "..hum.."<br>rain:"..rainCount.."<br>wind:"..windCount.."</p>";
        buf = buf.."<p><a href=\"?stat=1\"><button>stat</button></a>&nbsp;</p>";
        
        client:send(buf);
        client:close();
        collectgarbage();
    end)
end)

function writeStatus(temp,hum,rain,wind)
    --fd = nil
    --fd = file.open("temps.txt", "a+")
    if file.open("temps.txt", "a+") then
      file.write('t'..temp..' h'..hum..' r'..rain..' w'..wind..' up'..tmr.time() )  
      if (wifi.sta.getip() == nil) then
        file.writeline(' noip')
        file.close()
      else
        file.write(' ip'..wifi.sta.getip())
        conn = nil
          conn = net.createConnection(net.TCP, 0)
          conn:on("connection",function(conn, payload)conn:send("HEAD / HTTP/1.1\r\nHost: 216.58.212.196\r\n\r\n")end) 
          
          conn:on("receive", function(conn, payload)
            local _, day, mon, year, hour, min, sec = payload:match("Date: (.-), (.-) (.-) (.-) (.-):(.-):(.-) GMT")
            file.writeline(' tm'..year..mon..day..'-'..hour..':'..min..':'..sec )
            file.close()
            --local unixdate = rtctime.mktime(tonumber(year), tonumber(mon), tonumber(day), tonumber(hour), tonumber(min), tonumber(sec))
            --rtctime.set(unixdate, 0)
            --print("INFO: Time Synchronized (via http). Current timestamp: "..rtctime.get())
          end)
          
          conn:on("disconnection",function(conn, payload)file.close() end)
          -- google.nl = 216.58.212.196
          conn:connect(80,'216.58.212.196')
       
      end
      
    end

end

init()
