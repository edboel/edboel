--function init_i2c_display()
counter=0
pm10=-1
pm25=-1
temp=-100
humi=-100
readOk=false
sentOk=false


i2c.setup(0, 1, 2, i2c.SLOW)
disp = u8g2.ssd1306_i2c_128x64_noname(0,0x3c)
--uart.setup(0, 9600, 8, uart.PARITY_NONE, uart.STOPBITS_1, 1)


function init_disp()
    disp:setFontRefHeightExtendedText()
    disp:setFontPosTop()
    disp:clearBuffer()
    disp:setFont(u8g2.font_6x10_tf)
    disp:drawStr( 4, 15, "PAUSED")
    disp:sendBuffer()
end


function sendStatus()
  print("sending ") 
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("connection", function(conn, payload)
      conn:send('GET /sens/airq.php?pm10='..pm10..'&pm25='..pm25..'&t='..temp..'&h='..humi..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')
      disp:drawStr( 4, 15, "request sent ")
      disp:sendBuffer()
      print("r sent" )
      end)
  conn:on("receive", function(conn, payload)
      disp:drawStr( 4, 30, "request received ")
      disp:sendBuffer()
      print("r ok" )
      end)
  conn:on("disconnection", function(conn, payload) 
     disp:drawStr( 4, 45, "disconnected ")
     disp:sendBuffer()
     print("disc" )
     --wifi.sta.disconnect() 
     sentOk = true
     end)
     
  conn:connect(80,'edsard.nl')

end

function readTemp()
    status, temp, humi, temp_dec, humi_dec = dht.readxx(7)
    print("DHT Temperature:"..temp..";".."Humidity:"..humi)
    
    disp:drawStr( 4, 15, "temp "..temp.." hum "..humi)
    disp:sendBuffer()
end

function startSensor()
    uart.write(0, 0x01)
end

function stopSensor()
    print("stopping ") 
    --uart.on("data")
    uart.write(0,
        0xaa,   -- head
        0xb4,   -- command id
        0x06,   -- data byte 1
        0x01,   -- data byte 2 (set mode)
        0x00,   -- data byte 3 (sleep)
        0x00,   -- data byte 4
        0x00,   -- data byte 5
        0x00,   -- data byte 6
        0x00,   -- data byte 7
        0x00,   -- data byte 8
        0x00,   -- data byte 9
        0x00,   -- data byte 10
        0x00,   -- data byte 11
        0x00,   -- data byte 12
        0x00,   -- data byte 13
        0xFF,   -- data byte 14 (device id byte 1)
        0xFF,   -- data byte 15 (device id byte 2)
        0x05,   -- checksum
        0xAB    -- tail
        )
     if file.open("stops.txt", "a+") then
      ltime = math.floor(tmr.time() / 60)
      file.writeline(tmr.time().."p")
      file.close()
    end
end

function convertbytes(data,x)
    pm25 = ( data:byte(x+2) + data:byte(x+3) * 256 ) / 10
    pm10 = ( data:byte(x+4) + data:byte(x+5) * 256 ) / 10 
    --print("pm25 "..pm25.." pm10 "..pm10)
    disp:clearBuffer()
    disp:drawStr( 4, 1,  "pm25 " ..data:byte(x+2).." "..data:byte(x+3).."    ")
    disp:drawStr( 4, 15, "pm25 "..pm25)
    disp:drawStr( 4, 30, "pm10 " ..data:byte(x+4).." "..data:byte(x+5).."    ")
    disp:drawStr( 4, 45, "pm10 "..pm10)
    disp:sendBuffer()
end

function readData()
    --when it reads 8 bytes..
    -- byte0 = 170  = AA
    -- byte1 = 192  = C0
    -- byte2 = pm25 
    -- byte3 = pm25
    -- byte4 = pm10
    -- byte5 = pm10
    -- byte6 = CRC1
    -- byte7 = CRC2 

    --uart.setup(0, 9600, 8, uart.PARITY_NONE, uart.STOPBITS_1, 1)
    uart.on("data", 30,
        function(data)
          --print("receive from uart:", data)
          disp:clearBuffer()
          if counter==0 then
            disp:drawStr( 4, 15, "START  ")
            disp:sendBuffer()
          end
          for x=0,22 do
            if data:byte(x)==170 and data:byte(x+1) == 192 then
                convertbytes(data,x)
                readOk=true
            end
          end   

     end, 0)    
end

function readDataMock()
    print("starting mock")
    tmr.alarm(  3, 3020, tmr.ALARM_AUTO, setReadOk )
end

function setReadOk()
    print(" data received ")
    pm10=-5
    readOk=true
end

function writeToFile()
    if file.open("readings.txt", "a+") then
      ltime = math.floor(tmr.time() / 60)
      file.writeline(tmr.time().." p "..pm10.." pm25 "..pm25.." t "..temp.." h "..humi)
      file.close()
    end
end

function checkRead()
    print("checkread "..counter)
    if(readOk==true or counter > 90)then
        if(temp==-100)then
            readTemp()
        end
        tmr.stop(4)
        --uart.on("data")
        --stopSensor()
        writeToFile()
        sendStatus()
        stopSensor()
    else
        counter=counter+1
    end
end

function checkSent()
    print("checksent")
    if(sentOk==true)then
        print("sentok")
       stopSensor()
       wifi.sta.disconnect()
       tmr.stop(5)
       tmr.stop(3)
    end
end

function doMeasurement()
    wifi.sta.connect()
    counter=0
    readOk=false
    sendOk=false
    startSensor()
    temp=-100
    readTemp()
    readDataMock()
    tmr.alarm(  4, 1100, tmr.ALARM_AUTO, checkRead )
    tmr.alarm(  5, 4300, tmr.ALARM_AUTO, checkSent )
    tmr.alarm(  6, 60*1000, tmr.ALARM_SINGLE, stopSensor )
end

print("begin airq")
init_disp()
doMeasurement()
          
mytimer = tmr.create()
mytimer:register(3*60*1000, tmr.ALARM_AUTO, function (t) 
--mytimer:register(30*1000, tmr.ALARM_SINGLE, function (t) 

    disp:drawStr( 4, 15, "START ")
    disp:sendBuffer()
    doMeasurement()
    end)
    
mytimer:start()

