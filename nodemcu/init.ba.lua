-- init.ki.lua
curssid = "edland"
currssi = -100

 -- gpio.mode(3, gpio.OUTPUT)
 -- gpio.write(3, gpio.LOW)
  wifi.setmode(wifi.STATION)

   function connectwifi()
    --print(" conn "..curssid)
    --wifi.sta.disconnect()
    
    station_cfg={}
    station_cfg.ssid=curssid
    station_cfg.pwd="xxx"
    wifi.sta.config(station_cfg)
    wifi.sta.sethostname("ESP-basement")
    wifi.sta.connect()
   end

  function listap(t)
    for bssid,v in pairs(t) do
        local ssid, rssi, authmode, channel = string.match(v, "([^,]+),([^,]+),([^,]+),([^,]*)")
        --print(string.format("%32s",ssid).."\t"..bssid.."\t  "..rssi.."\t\t"..authmode.."\t\t\t"..channel)
        if(ssid=='edland')then
            --print ("got edland for "..rssi )
            currssi = rssi
        end
        if(ssid=='edland2')then
            --print ("got edland2 for "..rssi.." "..currssi )
            if(rssi < currssi)then
                --print("edland2 better" )
                curssid="edland2"
            end
        end
    end
  end
  --wifi.sta.getap(1, listap)
  function getAp()
    wifi.sta.getap(1, listap)
  end

  function doScript()
        --print(wifi.sta.getip())
        dofile("basement2.lua")
  end

  --print(" i ")
  --tmr.create():alarm(4000, tmr.ALARM_SINGLE, getAp )
  tmr.create():alarm(5000, tmr.ALARM_SINGLE, connectwifi )
  tmr.create():alarm(8000, tmr.ALARM_SINGLE, doScript )
