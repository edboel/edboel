--
alt=10 -- altitude of the measurement place

print("bme280 test")

res = bme280.init(2, 1)

print("init res = "..res)

P = bme280.baro()
print(string.format("QFE=%d.%03d", P/1000, P%1000))

-- convert measure air pressure to sea level pressure
QNH = bme280.qfe2qnh(P, alt)
print(string.format("QNH=%d.%03d", QNH/1000, QNH%1000))

-- altimeter function - calculate altitude based on current sea level pressure (QNH) and measure pressure
P = bme280.baro()
curAlt = bme280.altitude(P, QNH)
print(string.format("altitude=%d.%02d", curAlt/100, curAlt%100))
--
