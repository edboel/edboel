brightness=0
warmth=0


  --function startServer()
    print("begin lightweb")
    
    -- init webserver
    srv=net.createServer(net.TCP,10)
    srv:listen(80,function(conn)
        conn:on("receive", function(client,payload)
            print("received")
            local buf = "";
            local _, _, method, path, vars = string.find(payload, "([A-Z]+) (.+)?(.+) HTTP");
            
            if(method == nil)then
                _, _, method, path = string.find(payload, "([A-Z]+) (.+) HTTP");
            end
            if(method ~= nil)then
                print("method"..method)
            end
            if(path ~= nil)then
                print("path "..path)
                if(path=="/on")then
                    print("full on")
                    if(brightness>3)then
                        setlight(9,28)
                    else
                        setlight(4,28)
                    end
                end
                if(path=="/off")then
                    print("full off")
                    setlight(0,0)
                end
            end
            local _GET = {}
            if (vars ~= nil)then
                print("vars"..vars)
                for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                    _GET[k] = v
                end
            end
            if(_GET.bright ~= nil and _GET.bright >= "0")then
                print("get data")
                brightness = _GET.bright
                warmth = _GET.warmth
                print (brightness.." w "..warmth)
            end
            
            if(path=="/stat")then
                    sck:send("b"..brightness.." w"..warmth, function(conn) conn:close() end)
            else

                buf = buf.."<!DOCTYPE html><html><head><style>\r\n"
                buf = buf.."input { width: 100%; \r\n"
                buf = buf.."   height: 150px; }\r\n"
                buf = buf.."</style><script>\r\n"
                buf = buf.."function sendval(val){\r\n"
                buf = buf.."window.location.href='/?bright=5&warmth='+val }"
                buf = buf.."</script></head><body>\r\n"
                buf = buf.."<h1>Light bar Web Server</h1>"
                buf = buf.."<h3>brightness: "..brightness.."</h1>"
                buf = buf.."<form method=\"get\">\r\n"
                buf = buf.."<input type=\"button\" value=\"ON\" onclick=\"window.location.href='/on'\"><br><br><br>\r\n"
                buf = buf.."<input type=\"button\" value=\"OFF\" onclick=\"window.location.href='/off'\"><br><br>\r\n"
                buf = buf.."warmth<input type=\"range\" min=\"1\" max=\"31\" class=\"slider\" id=\"w\" name=\"w\" onchange=\"sendval(this.value)\"><br><br>\r\n"
                buf = buf.."</form></body></html>\r\n"
                client:send(buf,function(sk)
                    sk:close()
                end)
            end

            setlight(brightness,warmth)
            
        end)
    end)
   --end



   --startServer()
function setlight(ba,bw)
    a = tonumber(ba)
    w = tonumber(bw)
    if (a < 0) then a=0 end
    if (a > 31) then a=31 end
    if (w < 0) then w=0 end
    if (w > 30) then w = 30 end

    
    if (w < 10) then
        r = (w * 25)
        g = (w * 25)
        b = 250
    else 
        if (w < 20) then
            r = 250 
            g = 250 
            b = 250 - (25 * (w - 10))
        else
            r = 250 
            g = 250 - (25 * (w - 20)) 
            b = 0
        end
    end
        
    leds_set = string.char(a, b, g, r)
    --print ("setting "..a.." "..b.." "..g.." "..r) 
    leds_abgr =  leds_set
    for j=1,120 do 
      leds_abgr = leds_abgr .. leds_set
    end
    apa102.write(2, 1, leds_abgr)
    warmth=w
    brightness=a
   
end

setlight(3,28)
