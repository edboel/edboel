lighton=0
lcount=180
pin=4
gpio.mode(pin,gpio.OUTPUT)
dhtpin = 7

-- setup I2c and connect display
function init_i2c_display()
     i2c.setup(0, 6, 5, i2c.SLOW)
     disp = u8g.ssd1306_128x64_i2c(0x3c)
     --disp = u8g.sh1106_128x64_i2c(0x3c)
     --font_6x10,font_fub14,font_fub17,font_chikita
     
     --disp:setFont(u8g.font_6x10)
     disp:setFontRefHeightExtendedText()
     disp:setDefaultForegroundColor()
     disp:setFontPosTop()
end

init_i2c_display()

tmr.alarm(1,2000,1,function()
    if lighton==0 then
        lighton=1
        status,temp,humi,temp_decimial,humi_decimial = dht.read(dhtpin)
        if( status == dht.OK ) then
  
          -- Float firmware using this example
          print("DHT Temperature:"..temp..";".."Humidity:"..humi)
          disp:firstPage()
          repeat
            -- lines = 25
            --lines = 5
            --disp:drawFrame(0,0,126,62);
            --disp:setFontPosTop()
            --disp:drawStr( 1, 5, "Temp & Humi")
            disp:setFont(u8g.font_6x10)
            disp:drawStr( 4, 0, lcount.." ")
            disp:setFont(u8g.font_fub14)
            disp:drawStr( 4, 20, "Hum  : "..humi.."%")
            disp:drawStr( 4, 40, "Temp : "..temp.."C")
            lcount = lcount -1
          until disp:nextPage() == false
        elseif( status == dht.ERROR_CHECKSUM ) then
          print( "DHT Checksum error." );
        elseif( status == dht.ERROR_TIMEOUT ) then
          print( "DHT Time out." );
        end
        --gpio.write(pin,gpio.HIGH)
    else
        lighton=0
         gpio.write(pin,gpio.LOW)
    end
end)
