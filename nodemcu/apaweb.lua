  wifi.setmode(wifi.STATION)
  --wifi.sta.config("edland","xxx")
  wifi.sta.config("trimm","w@terv@l")
  wifi.sta.connect()
  wcount = 0
  data = nil

  colorRange=1
  function startServer()
    print("begin apaweb")
    -- init webserver
    srv=net.createServer(net.TCP,10)
    srv:listen(80,function(conn2)
        conn2:on("receive", function(client,payload)
            print("received")
            local buf = "";
            local _, _, method, path, vars = string.find(payload, "([A-Z]+) (.+)?(.+) HTTP");
            if(method == nil)then
                _, _, method, path = string.find(payload, "([A-Z]+) (.+) HTTP");
            end
            if method=="POST" then
               print("posted")
               --print(payload)
                local tData = {}
                for sParam, sValue in payload:gmatch "([^=&]+)=([^=&]+)" do
                    --print("got"..sParam.."w "..sValue)
                    tData[sParam:lower()] = sValue
                end
                print("value")
                print(tData.points)
                setbar(tData.points) 
                colorRange=tData.points
                --client:send("200");
                --client:close();
                --collectgarbage();

            else
                local _GET = {}
                if (vars ~= nil)then
                    for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                        _GET[k] = v
                    end
                end
                if(_GET.stat == "1")then
                    print("get data")
                    rh, t = am2320.read()
                elseif(_GET.stat == "2")then
                   rainCount = rainCount + 0.2794
                end
                
                
             end

            buf = buf.."<h1>Light bar Web Server</h1>";
            buf = buf.."<p>ip: "..wifi.sta.getip().."<br>uptime"..tmr.time() .." </p>";
            buf = buf.."<p><a href=\"?stat=1\"><button>stat</button></a>&nbsp;</p>";
            buf = buf.."<p><form action=\"\" method=\"post\" >";
            buf = buf.."<input type=\"hidden\" name=\"test\" value=\"on\">";
            buf = buf.."<input type=\"range\" name=\"points\" min=\"0\" max=\"120\" ";
            buf = buf.."value=\""..colorRange.."\" ";
            buf = buf.."style=\"width:360px;\" onchange=\"this.form.submit()\">";
            buf = buf.."<input type=\"submit\"></form></p>";
            
            client:send(buf);
            client:close();
            collectgarbage();
        end)
    end)
   end
   
  --init display
  print("init display")
   i2c.setup(0, 1, 2, i2c.SLOW)
   disp = u8g.ssd1306_128x64_i2c(0x3c)
   --disp = u8g.sh1106_128x64_i2c(0x3c)
   disp:setFontRefHeightExtendedText()
   disp:setDefaultForegroundColor()
   disp:setFontPosTop()
   disp:firstPage()
   disp:setFont(u8g.font_fub14)
   repeat
      disp:drawStr( 4, 20, " init ")
      disp:drawStr( 4, 40, " apaweb ")
      disp:drawStr( 4, 60, " v0.2 ")
   until disp:nextPage() == false

  lytimer = tmr.create()
  lytimer:register(3500, 
            tmr.ALARM_AUTO, 
            function (t) 
                print(" ip ")
                wcount = wcount + 1
                wifistat = "no ip"
                if (wifi.sta.getip() ~= nil) then
                    wifistat = "ip "..wifi.sta.getip()
                    tmr.stop(lytimer)
                    tmr.unregister(lytimer)
                    startServer()
                    --tmr.delay(50000)
                    setbar(0)
                    fetchData()
                else
                    wifi.sta.disconnect()
                    tmr.delay(50000)
                    wifi.sta.connect()
                    print("searching "..wcount)
                    if(wcount==1) then
                      bardemo()
                      setbar(0)
                    end
                end
                print(wifistat )
                disp:firstPage()
                repeat
                  disp:setFont(u8g.font_6x10)
                  disp:drawStr( 0, 8,  wifistat..wcount)
                until disp:nextPage() == false

                
            end)
  lytimer:start()

  --fetch every 5 minutes
  ftimer = tmr.create()
  ftimer:register(5*60*1000, 
            tmr.ALARM_AUTO, 
            function (t) 
                fetchData()
            end)
  ftimer:start()



  function fetchData()
    if (wifi.sta.getip() ~= nil) then
        print("fetch data")
          http.get("http://213.233.233.189/projects/lightbar/bar.txt", nil, function(code, data)
        if (code < 0) then
          print("HTTP request failed")
        else
          local t = cjson.decode(data)
          --for k,v in pairs(t) do print(k,v) end
          print(t.bar)
          setbarcolor(t.bar,t.color1r,t.color1g,t.color1b,t.color2r,t.color2g,t.color2b)
        end
      end)
    else
      print("no ip for fetch")
      wifi.sta.disconnect()
      tmr.delay(50000)
      wifi.sta.connect()
    end
  end


  function setbar(value)
    leds_abgr = ""
    a = 31
    b = 0
    g = 0
    r = 255
    leds_set = string.char(a, b, g, r) 
    
    for i=0,value do 
      leds_abgr = leds_abgr .. leds_set
    end

    a = 4
    b = 0
    g = 255
    r = 0
    leds_set = string.char(a, b, g, r) 
    for i=value,120 do 
      leds_abgr = leds_abgr .. leds_set
    end

    apa102.write(6, 7, leds_abgr)
    qytimer = tmr.create()
      qytimer:register(500, 
                tmr.ALARM_SINGLE, 
                function (t) 

                    apa102.write(6, 7, leds_abgr)
                end)
      qytimer:start()
  end


  function setbarcolor(value,r,g,b,r2,g2,b2)
    leds_abgr = ""
    a = 31
    leds_set = string.char(a, b, g, r) 
    
    for i=0,value do 
      leds_abgr = leds_abgr .. leds_set
    end

    a = 4
    leds_set = string.char(a, b2, g2, r2) 
    for i=value,120 do 
      leds_abgr = leds_abgr .. leds_set
    end

    apa102.write(6, 7, leds_abgr)
    qytimer = tmr.create()
      qytimer:register(500, 
                tmr.ALARM_SINGLE, 
                function (t) 
                    apa102.write(6, 7, leds_abgr)
                end)
      qytimer:start()
  end

    print ("init done")

    leds_abgr = ""
    a = 4
    b = 0
    g = 255
    r = 0
    leds_set = string.char(a, b, g, r) 
    for i=0,120 do 
      leds_abgr = leds_abgr .. leds_set
    end

    apa102.write(6, 7, leds_abgr)


    function bardemo()
        leds_abgr = ""
        a = 31
        b = 255
        g = 0
        r = 0
        leds_set = string.char(a, b, g, r) 
        
        for i=1,120 do 
        
        apa102.write(6, 7, leds_abgr)
        leds_abgr = leds_abgr .. leds_set
        tmr.delay(1000)
        end
        
        print("4")
    
        leds_abgr = ""
        a = 31
        b = 0
        g = 155
        r = 155
        leds_set = string.char(a, b, g, r) 
        
        for i=1,120 do 
        
        apa102.write(6, 7, leds_abgr)
        leds_abgr = leds_abgr .. leds_set
        tmr.delay(1000)
        end
        
        print("5")
    
        leds_abgr = ""
        a = 31
        b = 255
        g = 255
        r = 255
        leds_set = string.char(a, b, g, r) 
        
        for i=1,120 do 
        
        apa102.write(6, 7, leds_abgr)
        leds_abgr = leds_abgr .. leds_set
        tmr.delay(1000)
        end
        leds_abgr = ""
        print("6")

        leds_abgr = ""
        a = 31
        b = 255
        g = 0
        r = 255
        leds_set = string.char(a, b, g, r) 
        
        for i=1,120 do 
        
        apa102.write(6, 7, leds_abgr)
        leds_abgr = leds_abgr .. leds_set
        tmr.delay(1000)
        end
        leds_abgr = ""
        print("6")

        leds_abgr = ""
        a = 1
        b = 8
        g = 0
        r = 240
        up = 1
        
        for i=1,120 do 
          apa102.write(6, 7, leds_abgr)
          if (a == 0) then
            up = 1
          end
          if (a == 31) then
            up = 0
          end
          if (up ==1) then
            a = a + 1
            b = b + 8
            r = r - 8
          else
            a = a - 1
            b = b - 8
            r = r + 8
          end
          leds_set = string.char(a, b, g, r)
          --print("a"..a.."b"..b)
          leds_abgr = leds_abgr .. leds_set
          tmr.delay(2000)
        end
        leds_abgr = ""
        print("6")
        tmr.delay(50000)
    end
