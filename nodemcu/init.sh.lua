-- init.ki.lua

gpio.mode(8,gpio.INPUT) --gpio.INT)
gpio.mode(7,gpio.INPUT)
gpio.mode(6,gpio.INPUT)
gpio.mode(5,gpio.INPUT)

curssid = "edland"
currssi = -100

  gpio.mode(4, gpio.OUTPUT)
  
  wifi.setmode(wifi.STATION)

   function connectwifi()
    --print(" conn "..curssid)
    --wifi.sta.disconnect()
    gpio.write(4, gpio.LOW)
    station_cfg={}
    station_cfg.ssid=curssid
    station_cfg.pwd="xxx"
    wifi.sta.config(station_cfg)
    wifi.sta.sethostname("ESP-shutter")
    wifi.sta.connect()
   end

  function listap(t)
    for bssid,v in pairs(t) do
        local ssid, rssi, authmode, channel = string.match(v, "([^,]+),([^,]+),([^,]+),([^,]*)")
        --print(string.format("%32s",ssid).."\t"..bssid.."\t  "..rssi.."\t\t"..authmode.."\t\t\t"..channel)
        if(ssid=='edland')then
            --print ("got edland for "..rssi )
            currssi = rssi
        end
            --print ("got edland2 for "..rssi.." "..currssi )
            --if(rssi < currssi)then
                --print("edland2 better" )
            --    curssid="edland2"
    end
  end
  --wifi.sta.getap(1, listap)
  function getAp()
    wifi.sta.getap(1, listap)
  end

  function doScript()
        if(wifi.sta.getip() ~= nil)then
          gpio.write(4, gpio.HIGH)
        end 
        --print(wifi.sta.getip())
        dofile("shutter.lua")
  end
  
  tmr.create():alarm(4000, tmr.ALARM_SINGLE, connectwifi )
  tmr.create():alarm(8000, tmr.ALARM_SINGLE, doScript ) 
