require('ds18b20')
ds18b20.setup(4)
lcount=5
t=0

wifi.setmode(wifi.STATION)
wifi.sta.config("****","****")

t=ds18b20.read()
print(" begin temp :"..t..";")

function init_i2c_display()
     i2c.setup(0, 6, 5, i2c.SLOW)
     disp = u8g.ssd1306_128x64_i2c(0x3c)
     --disp = u8g.sh1106_128x64_i2c(0x3c)
     disp:setFontRefHeightExtendedText()
     disp:setDefaultForegroundColor()
     disp:setFontPosTop()
end

function sendStatus()
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", function(conn, payload)success = true print(payload)end)
  conn:on("connection",
   function(conn, payload)
   print("Connected")
   conn:send('GET /projects/senswi/status_o.php?s=0&t='..t..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')

end

init_i2c_display()

tmr.alarm(1,2000,1,function()
    t=ds18b20.read()
    lcount = lcount -1
    print(" Temperature:"..t..";")
    disp:firstPage()
    repeat
       st = string.format("%.2f",t)    
       disp:setFont(u8g.font_fub14)
       disp:drawStr( 4, 16, "Out : "..st.." c")
       disp:drawStr( 4, 40, "In :  18 c")
       disp:setFont(u8g.font_6x10)
       disp:drawStr( 4, 55, "count : "..string.format("%d",lcount)..".")
            
    until disp:nextPage() == false

    
    if (lcount == 0) then
            sendStatus()
            lcount = 180
          end
end)
