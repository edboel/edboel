print(" i ")
  dofile("setlight.lua")

  setlight(2,30)

  tstimer = tmr.create()
  tstimer:register(2000, 
            tmr.ALARM_SINGLE, 
            function (t) 
                wifi.setmode(wifi.STATION)
                setlight(2,28) 
            end)
  tstimer:start()
  
  mytimer = tmr.create()
  mytimer:register(5000, 
            tmr.ALARM_SINGLE, 
            function (t) 
                 connectwifi()
            end)
  mytimer:start()

  function connectagain()
    if(wifi.sta.getip() ~= nil) then
       print("now listening on " ..wifi.sta.getip())
       setlight(3,28)
    end
    dofile("lightweb.lua")

  end

  function connectwifi()
    print(" n ")
    
    station_cfg={}
    station_cfg.ssid="edland"
    station_cfg.pwd="xxx"
    wifi.sta.config(station_cfg)
    wifi.sta.sethostname("ESP-sleeplight")
    if(wifi.sta.getip() ~= nil) then
       print("listening on " ..wifi.sta.getip())
       setlight(3,28)
       dofile("lightweb.lua")
    else
       setlight(2,26)
       etimer = tmr.create()
       etimer:register(3000, 
            tmr.ALARM_SINGLE, 
            function (t) 
                 connectagain()
            end)
       etimer:start()
    end 
  end

  
