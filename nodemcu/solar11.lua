pin=4
rainPin = 6

current=0
lcount=10
rainCount=0
prevpulse=0
gpio.mode(pin,gpio.OUTPUT)

--wifi.setmode(wifi.STATION)
--wifi.sta.config("edland","xxx")

-- setup I2c and connect display
function init_i2c_display()
     i2c.setup(0, 3, 4, i2c.SLOW)
     disp = u8g.ssd1306_128x64_i2c(0x3c)
     --disp = u8g.sh1106_128x64_i2c(0x3c)
     --font_6x10,font_fub14,font_fub17,font_chikita
     
     --disp:setFont(u8g.font_6x10)
     disp:setFontRefHeightExtendedText()
     disp:setDefaultForegroundColor()
     disp:setFontPosTop()
end

function sendStatus()
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", function(conn, payload)success = true print(payload)end)
  conn:on("connection",
   function(conn, payload)
   print("Connected")
   conn:send('GET /sens/solar1.php?s=0&c='..current..'&r='..rainCount..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')

end

local function onRainChange(level, pulse)
    -- print("wind"..pulse)
    if ( pulse ~= prevpulse ) then
      rainCount = rainCount + 1 
    end
    prevpulse = pulse
    gpio.trig(rainPin, level == gpio.HIGH  and "down" or "up")
end

gpio.mode(rainPin,gpio.INT)
  --gpio.trig(rainPin,"down", debounce(onRainChange))
gpio.trig(rainPin,"down", onRainChange)



init_i2c_display()

tmr.alarm(1,6000,1,function()

          current = adc.read(0)
          -- Float firmware using this example
          print(lcount.." Current:"..current)
          disp:firstPage()
          repeat
            disp:setFont(u8g.font_fub14)
            disp:drawStr( 4, 14, "load : "..current)
            disp:drawStr( 2, 30, "count : "..lcount)
            disp:drawStr( 2, 50, "rain : "..rainCount)
            disp:drawStr( 2, 64, "  --      ")
          until disp:nextPage() == false
          lcount = lcount -1
          if (lcount == 0) then
            disp:firstPage()
          repeat
            disp:setFont(u8g.font_fub14)
            disp:drawStr( 4, 14, " "..current)
            disp:drawStr( 4, 30, " "..lcount)
            disp:drawStr( 4, 50, " "..rainCount)
            disp:drawStr( 2, 64, "  sending    ")
          until disp:nextPage() == false
            sendStatus()
            lcount = 60
            rainCount = 0
          end
        
end)
