lighton=0
lcount=4
pin=4
gpio.mode(pin,gpio.OUTPUT)
dhtpin = 7
humi=0
temp=0

wifi.setmode(wifi.STATION)
wifi.sta.config("****","****")

-- setup I2c and connect display
function init_i2c_display()
     i2c.setup(0, 6, 5, i2c.SLOW)
     disp = u8g.ssd1306_128x64_i2c(0x3c)
     --disp = u8g.sh1106_128x64_i2c(0x3c)
     --font_6x10,font_fub14,font_fub17,font_chikita
     
     --disp:setFont(u8g.font_6x10)
     disp:setFontRefHeightExtendedText()
     disp:setDefaultForegroundColor()
     disp:setFontPosTop()
end

function sendStatus()
  conn = nil
  conn = net.createConnection(net.TCP, 0)
  conn:on("receive", function(conn, payload)success = true print(payload)end)
  conn:on("connection",
   function(conn, payload)
   print("Connected")
   conn:send('GET /sens/temp_sleeping.php?s=0&t='..temp..'&h='..humi..' HTTP/1.0\r\n\Host: edsard.nl\r\n\r\n')end)
  conn:on("disconnection", function(conn, payload) print('Disconnected') end)
  conn:connect(80,'edsard.nl')

end

init_i2c_display()

tmr.alarm(1,2000,1,function()
    if lighton==0 then
        lighton=1
        status,temp,humi,temp_decimial,humi_decimial = dht.read(dhtpin)
        if( status == dht.OK ) then
  
          -- Float firmware using this example
          print(lcount.." Temperature:"..temp..";".."Humidity:"..humi)
          disp:firstPage()
          repeat
            -- lines = 25
            --lines = 5
            --disp:drawFrame(0,0,126,62);
            --disp:setFontPosTop()
            --disp:drawStr( 1, 5, "Temp & Humi")
            
            disp:setFont(u8g.font_fub14)
            disp:drawStr( 4, 16, "Hum  : "..humi.."%")
            disp:drawStr( 4, 40, "Temp : "..temp.."C")
            disp:setFont(u8g.font_6x10)
            disp:drawStr( 4, 55, lcount.." ")
            
          until disp:nextPage() == false
          lcount = lcount -1
          if (lcount == 0) then
            sendStatus()
            lcount = 180
          end
        elseif( status == dht.ERROR_CHECKSUM ) then
          print( "DHT Checksum error." );
        elseif( status == dht.ERROR_TIMEOUT ) then
          print( "DHT Time out." );
        end
        --gpio.write(pin,gpio.HIGH)
    else
        lighton=0
         gpio.write(pin,gpio.LOW)
    end
end)
